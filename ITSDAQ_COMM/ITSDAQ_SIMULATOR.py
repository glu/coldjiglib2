from influxdb import InfluxDBClient
import datetime, time
from modules.GUIlogging import init_logger
from InfluxDBHandler import InfluxDBHandler
import json

from ITK_INFLUX import ITK_INFLUX

logging = init_logger(__name__)
InfluxDBHandler = InfluxDBHandler(None)
setup='BNL'  # eg. BNL

def checkCommand():
    dbClient = InfluxDBHandler.influx_init()
    lastTimeStamp = datetime.datetime.min
    currTimeStamp = lastTimeStamp
    while(True):
        #ResultSet = dbClient.query('SELECT time, Command FROM COMM WHERE Receiver=\'ITSDAQ\' GROUP BY * ORDER BY time DESC LIMIT 1;')
        ResultSet = dbClient.query(f'SELECT "COMMAND", "DATA" FROM "COMM" WHERE ("SETUP" = {setup} AND "RECEIVER" = \'ITSDAQ\' ) GROUP BY * ORDER BY time DESC LIMIT 1')
        for it in ResultSet.get_points("COMM"):
            dict_d= json.loads(it["DATA"])
            currTimeStamp = it["time"]
            currTimeStamp = datetime.datetime.strptime(currTimeStamp, '%Y-%m-%dT%H:%M:%S.%fZ')
            break

        #print(currTimeStamp, lastTimeStamp)
        if currTimeStamp <= lastTimeStamp:
            print("No new command found. Waiting..")
        else:
            lastTimeStamp = currTimeStamp
            #logging.debug(points)
            if "INIT_BURNIN" in str(ResultSet):
                InfluxDBHandler.influx_data_itsdaq(setup=setup, sender="ITSDAQ", command="STATUS", data="xyz", error="fatal")
                print("INIT_BURNIN",dict_d["modules"])
            elif "POWER_UP" in str(ResultSet):
                InfluxDBHandler.influx_data_itsdaq(setup=setup, sender="ITSDAQ", command="STATUS", data="xyz", error="fatal")  
                print("POWER_UP")
            elif "ABORT" in str(ResultSet):
                InfluxDBHandler.influx_data_itsdaq(setup=setup, sender="ITSDAQ", command="STATUS", data="xyz", error="fatal") 
                print("ABORT",dict_d["modules"])
            elif "RUN_HYBRID_BURNIN_TESTS" in str(ResultSet):
                InfluxDBHandler.influx_data_itsdaq(setup=setup, sender="ITSDAQ", command="STATUS", data="xyz", error="fatal")
                print("RUN_HYBRID_BURNIN_TESTS",dict_d["modules"])
            elif "STOP_HYBRID_BURNIN_TESTS" in str(ResultSet):
                InfluxDBHandler.influx_data_itsdaq(setup=setup, sender="ITSDAQ", command="STATUS", data="xyz", error="fatal")
                print("STOP_HYBRID_BURNIN_TESTS")
            elif "STATUS" in str(ResultSet):
                print("ITSDAQ_Returns Status",dict_d["modules"])
            elif "HV_STABILISATION" in str(ResultSet):
                print("RUN HV_STABILISATION",dict_d["modules"])
            elif "RUN_WARM_CONFIRMATION" in str(ResultSet):
                print("ITSDAQ RUN_WARM_CONFIRMATION", dict_d["modules"])
            elif "RUN_WARM_CHARACTERISATION" in str(ResultSet):
                print("ITSDAQ RUN_WARM_CHARACTERISATION", dict_d["modules"])
            elif "RUN_COLD_CONFIRMATION" in str(ResultSet):
                print("ITSDAQ RUN_COLD_CONFIRMATION", dict_d["modules"])
            elif "RUN_COLD_CHARACTERISATION" in str(ResultSet):
                print("ITSDAQ RUN_COLD_CHARACTERISATION",dict_d["modules"])
            elif "INIT_MODULES" in str(ResultSet):
                print("ITSDAQ INIT_MODULES",dict_d["modules"])
                print("***test***",type(dict_d["modules"]))
            else:
                print("No command for processing found / command not understood")

        time.sleep(1)
    
#if __name__ == "__main__":
#    checkCommand()
