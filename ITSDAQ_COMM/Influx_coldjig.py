from influxdb_client import InfluxDBClient, Point
import time
import json
import logging
#from .ITK_INFLUX import ITK_INFLUX

#from modules.GUIlogging import init_logger

logger = logging.getLogger(__name__)

Influx = None # 
#influx_ini_file= "" # Path to ini file

def parse_errorstr(errorstr):
    """
    Check if the `errorstr` returned by an itsdaq command
    is "None" (case insensitive). If yes, convert to
    `None`.
    """
    errorstr if type(errorstr)!=str or errorstr.lower()!='none' else None

class Coldjig_Influx_COMM:

    logger = logging.getLogger(__name__)
    
    def __init__(self, Influx):
        self.Influx = Influx
        self.setup=Influx.influxAttributeDictionary['setup']
        self.bucket=Influx.influxAttributeDictionary['bucket']
        self.sender=Influx.influxAttributeDictionary['setup_type']
        self.comm_measurement=Influx.influxAttributeDictionary['comm_measurement']
        logger.info(f'setup {self.setup}, bucket is {self.bucket}, sender is {self.sender}, comm_measurement is {self.comm_measurement}')

    def convertor(self,modules_list):
        json_d={'modules':modules_list}
        modules_string=json.dumps(json_d)
        return modules_string

    def Init_modules(self, modules):
        #print('*** INIT_Modules ***')
        logger.info(f'*** INIT_Modules ***  {modules}')
        #print(point.to_line_protocol())
        modules=self.convertor(modules)
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","INIT_MODULES").field("DATA", modules).field("ERROR", "NONE")
        logger.info(f'{point.to_line_protocol()}')
        self.Influx.write_comm_point(point)
        logger.debug(f'INIT_Modules {modules}')

    def Init_burnin(self,modules):
        modules=self.convertor(modules)
        #print('*** INIT_BURNIN ***')
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","INIT_BURNIN").field("DATA", modules).field("ERROR", "NONE")
        #print(point.to_line_protocol())
        self.Influx.write_comm_point(point)
        logger.debug(f'INIT_burnin {modules}')

    def RunColdCharacterisation(self,modules):
        modules=self.convertor(modules)
        #print('*** INIT_BURNIN ***')
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","RUN_COLD_CHARACTERISATION").field("DATA", modules).field("ERROR", "NONE")
        #print(point.to_line_protocol())
        self.Influx.write_comm_point(point)
        logger.debug(f' Running Cold Characterisation {modules}')

    def RunColdConfirmation(self,modules):
        modules=self.convertor(modules)
        #print('*** COLD_Confirmation***')
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","RUN_COLD_CONFIRMATION").field("DATA", modules).field("ERROR", "NONE")
        #print(point.to_line_protocol())
        self.Influx.write_comm_point(point)
        logger.debug(f' Running Cold Confirmation {modules}')
        
    def RunWarmCharacterisation(self,modules):
        modules=self.convertor(modules)
        #print('*** Warm Characterisation ***')
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","RUN_WARM_CHARACTERISATION").field("DATA", modules).field("ERROR", "NONE")
        #print(point.to_line_protocol())
        self.Influx.write_comm_point(point)
        logger.debug(f' Running Warm Characterisation {modules}')
    
    def RunWarmConfirmation(self,modules):
        modules=self.convertor(modules)
        #print('*** Warm Confirmation ***')
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","RUN_WARM_CONFIRMATION").field("DATA", modules).field("ERROR", "NONE")
        self.Influx.write_comm_point(point)
        logger.debug(f' Running Warm Confirmation {modules}')

    def RunHybridBurninTests(self,modules):
        modules=self.convertor(modules)
        #print('*** RUN_BURNIN ***')
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","RUN_HYBRID_BURNIN_TESTS").field("DATA", modules).field("ERROR", "NONE")
        #print(point.to_line_protocol())
        self.Influx.write_comm_point(point)
        logger.debug(f' Running Hybrid Burnin Test {modules}')
        
    def RunHVStability(self,modules):
        modules=self.convertor(modules)
        #print('*** RUN_HV_STABILITY ***')
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","HV_STABILISATION").field("DATA", modules).field("ERROR", "NONE")
        #print(point.to_line_protocol())
        self.Influx.write_comm_point(point)
        logger.debug(f' Running HV_Stability {modules}')

    def StatusReport(self,modules):
        modules=self.convertor(modules)
        #print('*** STATUS REPORT***')
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","STATUS").field("DATA", modules).field("ERROR", "")                
        #print(point.to_line_protocol())
        self.Influx.write_comm_point(point)
        logger.debug(f' Requesting status report on modules {modules}')

    def AbortProcess(self,modules):
        modules=self.convertor(modules)
        #print('*** ABORT_PROCESS ***')
        point = Point("COMM").tag("SETUP", self.setup).tag("SENDER",self.sender).tag("RECEIVER","ITSDAQ").field("COMMAND","ABORT").field("DATA", modules).field("ERROR", "")
        #print(point.to_line_protocol())
        self.Influx.write_comm_point(point)
        logger.debug(f' Abort process {modules}')
    
    def get_ITSDAQ_STATUS(self, start_time):
        logger.debug('Get ITSDAQ status')
        query= f'''
        from(bucket: \"{self.bucket}\") 
        |> range(start: time(v: {start_time}), stop: now())
        |> filter(fn: (r) => r["_measurement"] == \"{self.comm_measurement}\")
        |> filter(fn: (r) => r["SENDER"] == \"ITSDAQ\") 
        |> filter(fn: (r) => r["SETUP"] == \"{self.setup}\")  
        |> filter(fn: (r) => r["_field"] == \"COMMAND\" or r["_field"] == \"DATA\" or r["_field"] == \"ERROR\")  
        |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: \"_value\") 
        |> yield(name: \"last\")
        '''
        logger.debug(f'{query}')
        # logger.debug(f'query object is  {self.Influx.query_api.query(query)}')
        tables = self.Influx.query_api.query(query)
        logger.debug(f'tables are {tables}')
        try:
            for record in tables[0].records:
                if record["COMMAND"] == "STATUS":
                    record["ERROR"] = parse_errorstr(record["ERROR"])
                    logger.info(f' Data from ITSDAQ {record["DATA"]}')
                    logger.info(f' ERROR {record["ERROR"]}')
                    return {"DATA": record["DATA"], "ERROR": record["ERROR"]}
                else:
                    return None
        except Exception as e:
            logger.info("WAITING FOR ITSDAQ")
            return None


    def is_command_complete(self, start_time, command):
        '''
        Check ITSDAQ command has completed.  When complete
        ITSDAQ will echo the command with value "Complete"        
        '''

        logger.debug(f'Check {command} has completed')
        query= f'''
        from(bucket: \"{self.bucket}\") 
        |> range(start: time(v: {start_time}), stop: now()) 
        |> filter(fn: (r) => r["_measurement"] == \"{self.comm_measurement}\")   
        |> filter(fn: (r) => r["SENDER"] == \"ITSDAQ\") 
        |> filter(fn: (r) => r["RECEIVER"] == \"COLDJIG\") 
        |> filter(fn: (r) => r["SETUP"] == \"{self.setup}\")  
        |> filter(fn: (r) => r["_field"] == \"COMMAND\" or r["_field"] == \"DATA\" or r["_field"] == \"ERROR\")  
        |> drop(columns: [\"_start\",\"_stop\"])
        |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: \"_value\") 
        |> filter(fn: (r) => r["COMMAND"] == \"{command}\")
        |> yield(name: \"last\")
        '''
        logger.debug(f'{query}')
        tables = self.Influx.query_api.query(query)
        logger.debug(f'tables are {tables}')
        try:
            for record in tables[0].records:
                # if record["COMMAND"] == command:
                record["ERROR"] = parse_errorstr(record["ERROR"])
                logger.info(f' Data from ITSDAQ {record["DATA"]}')
                logger.info(f' ERROR {record["ERROR"]}')
                logger.info(f' TIME {record["_time"]}')
                if record["DATA"] == "Complete" :
                    return {"DATA": record["DATA"], "ERROR": record["ERROR"]}
                else :
                    return None
        except Exception as e:
            logger.info(f'{command} not completed')
            return None


if __name__=="__main__":
    influx_class=Coldjig_Influx_COMM()
    modules=[0,1,3]
    influx_class.Init_modules(modules)
    time.sleep(2)
    #influx_class.Query_DB()
    influx_class.Init_burnin(modules)
    time.sleep(2)
    influx_class.RunColdCharacterisation(modules)
    time.sleep(2)
    influx_class.RunColdConfirmation(modules)
    time.sleep(2)
    influx_class.RunHVStability(modules)
    time.sleep(2)
    influx_class.StatusReport(modules)
    time.sleep(2)
    influx_class.AbortProcess(modules)
    time.sleep(2)
    influx_class.RunHybridBurninTests(modules)
    time.sleep(2)
    influx_class.RunWarmCharacterisation(modules)
    time.sleep(2)
    influx_class.RunWarmConfirmation(modules)
