# Wrapper for ITSDAQ handling for the burn-in crate
from influxdb import InfluxDBClient
from modules import influx_query
import uuid
import random
import time
import threading
import logging
import os
import json        
import pandas as pd # importing pandas as pd
from InfluxDBHandler import InfluxDBHandler
ITSDAQpath=""  #add path to try   e.g. /home/ilona/coldbox_controller_webgui-v0.5.0/RUNITSDAQ.sh
InfluxDBHandler = InfluxDBHandler(None)
from ITK_INFLUX import ITK_INFLUX

influx_ini_file=""   #path to influx_ini_file

class ITSDAQ(threading.Thread) :

    readyToWrite = False

    def __init__(self,name) -> None: 
        """
        Constructor for ITSDAQ
        """
        self.id = None
        self.input  = None
        Influx = ITK_INFLUX(influx_ini_file)
        self.setup=Influx.influxAttributeDictionary['setup']
        self.bucket=Influx.influxAttributeDictionary['bucket']
        self.sender=Influx.influxAttributeDictionary['setup_type']
    

    ## a lot of these will probably just write to influx, then wait for the reply and return it 
    def start(self):
        """
        Start ITSDAQ
        """
        os.popen(f'bash {ITSDAQpath}')
        pass       

    def close(self):
        """
        Close ITSDAQ
        """        
        os.pclose(f'exit {ITSDAQpath}')
        pass

    def initBurnIn(self):
        """
        Initialise burn in and retrieve result
        """
        InfluxDBHandler.influx_data(setup=self.setup, sender=self.sender, command="INIT_BURNIN")
        

    def powerUp(self):
        """
        Power up power supplies
        """
        InfluxDBHandler.influx_data(setup=self.setup, sender=self.sender, command="POWER_UP")     


    def powerDown(self):
        """
        Power down power supplies
        """
        InfluxDBHandler.influx_data(setup=self.setup, sender=self.sender, command="ABORT")     


    def startConfirmationTest(self):
        """
        Start test series for burn-in
        """
        InfluxDBHandler.influx_data(setup=self.setup, sender=self.sender, command="RUN_HYBRID_BURNIN_TESTS") 


    def stopConfirmationTest(self):
        """
        Stop confirmation test series for burn-in
        """
        InfluxDBHandler.influx_data(setup=self.setup, sender=self.sender, command="STOP_HYBRID_BURNIN_TESTS")