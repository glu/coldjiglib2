
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS
import configparser
import time
import os

import logging
logger = logging.getLogger(__name__)

class ITK_INFLUX:
    
    def __init__(self, configFileStr):
        logger.debug(os.path.dirname(__file__))
        self.cp = configparser.ConfigParser()
        try:
            self.cp.read(configFileStr)
            logger.debug(configFileStr)
        except Exception as e:
            logger.error(e)
        self.cpSections = self.cp.sections()
        logger.debug(f'filefound: {configFileStr}')
        if("influx" not in self.cpSections):
            raise Exception("'influx' is not a section in the ini file!")

        self.bucket = self.cp['influx']['bucket']
        self.database = self.cp['influx']['database']
        self.ipAddress = self.cp['influx']['IP']
        self.measurement = self.cp['influx']['measurement']
        self.comm_measurement= self.cp['influx']['comm_measurement']
        self.org = self.cp['influx']['org']
        self.password = self.cp['influx']['password']
        self.port = int(self.cp['influx']['port'])
        self.retention = self.cp['influx']['retention']
        self.token = self.cp['influx']['token']
        self.url = self.cp['influx']['url']
        self.username = self.cp['influx']['username']
        self.version = int(self.cp['influx']['version'])
        self.setup = self.cp['DEFAULT']['Institution_name']
        self.Setup_Type = self.cp['DEFAULT']['Setup_Type']


        if((not self.token) and (not self.username) and (not self.password)):
            raise Exception("You have not provided a [token] or a [username and password] pair")
        elif((self.token and self.username) or (self.token and self.password)):
            raise Exception("You have provided a [token] and a [username or password]\nPlease provide either a [token] or [username and password] pair but not both")
        elif(not self.token):
            if(self.username and self.password):
                self.token = f"{self.username}:{self.password}"
            else:
                raise Exception("You did not provide a [token] but are missing either a [username] or [password]")
        
        logger.warning("You have successfully entered a token or username/password pair, but existence of such is not yet guarenteed")

        if((not self.bucket) and (not self.database) and (not self.retention)):
            raise Exception("You have not provided a [bucket] or a [database and retention] pair")
        if((self.bucket and self.database) or (self.bucket and self.retention)):
            raise Exception("You have provided a [bucket] and a [database or retention]\nPlease provide either a [bucket] or [database and retention] pair but not both")
        
        if(not self.bucket):
            if(self.database and self.retention):
                self.bucket = f"{self.database}/{self.retention}"
            else:
                raise Exception("You did not provide a [bucket] but are missing either a [database] or [retention] policy")
        else :
            self.database = self.bucket

        logger.warning("You have successfully entered a bucket or database/retention pair, but existence of such is not yet guarenteed")

        if((not self.url) and (not self.ipAddress) and (not self.port)):
            raise Exception("You have not provided a [url] or an [IP and port] pair")
        elif((self.url and self.ipAddress) or (self.url and self.port)):
            raise Exception("You have provided a [url] and a [IP or port]\nPlease provide either a [url] or [IP and port] pair but not both")
        elif(not self.url):
            if(self.ipAddress and self.port):
                self.url = f"http://{self.ipAddress}:{self.port}"
            else:
                raise Exception("You did not provide a [url] but are missing either an [IP] or a [port]")
        
        logger.warning("You have successfully entered a url or IP:port pair, but connection to such is not yet guarenteed")
        
        if(not self.measurement):
            raise Exception("You did not enter a measurement!")
        
        
        self.influxAttributeDictionary = {
            'bucket': self.bucket,
            'database': self.database,
            'IP-Address': self.ipAddress,
            'measurement': self.measurement,
            'org': self.org,
            'password': self.password,
            'port': self.port,
            'retention': self.retention,
            'token': self.token,
            'url': self.url,
            'username': self.username,
            'version':self.version,
            'setup': self.setup,
            'setup_type': self.Setup_Type,
            'comm_measurement': self.comm_measurement
        }

        self.client = InfluxDBClient(url = self.url, token = self.token, org = self.org)
        self.write_api = self.client.write_api(write_options = SYNCHRONOUS)
        self.query_api = self.client.query_api()

    def WriteToDatabase(self, data_dict, status):

        # self.write_api.write(self.database, self.org, [{"measurement": self.measurement, "fields": data_dict, "time": time.time_ns()}])

        self.write_api.write( self.database,self.org, 
                              {"measurement": self.measurement,
                              "tags": {"status":status},
                              "fields": data_dict, 
                              "time": time.time_ns()} )


    def returnWriteAttributes(self):
        return({'database': self.database, 'org': self.org, 'table': self.table})


    def printInfluxAttributes(self):
        '''Prints everything in the ini file in a dictionary format for DEBUGGING'''
        for key in self.influxAttributeDictionary.keys():
            print(f"{key}: {self.influxAttributeDictionary[key]}")
    
    def write_comm_point(self,point):
        self.write_api.write(bucket=self.bucket, record= point)
        return True
    
    def get_latest_value(self,field) :
        '''Get the latest recorded value of a data dict key'''

        # Set up DB query
        db_query = f'''
        from(bucket: "{self.bucket}") 
            |> range(start: -1mo) 
            |> filter(fn: (r) => r._measurement == "{self.measurement}")
            |> filter(fn: (r) => r._field == "{field}")
            |> keep(columns: ["_time","_value"])
            |> top(n:1, columns: ["_time"])
        '''
        logger.debug(f'FLUX Query: {db_query}')

        # Run the query 
        tables = self.query_api.query(db_query)

        if len(tables) == 0 :
            logger.debug(f'{field} does not exist in DB, return None')
            return None
        else :
            logger.debug(f'found lastest entry for {field}')
            table = tables[0]
            record = table.records[0]
            logger.debug(f'{field}: {record["_value"]} - Time: {record["_time"].isoformat()}') 
            return record["_value"]


