# Wrapper for InfluxDB Client
from influxdb import InfluxDBClient
from modules import influx_query
import time
import logging 
from modules.GUIlogging import init_logger

logging = init_logger(__name__)

class InfluxDBHandler() :

    readyToWrite = False

    logging = init_logger(__name__)

    def __init__(self,name) -> None: 
        """
        Constructor for InfluxDBHandler
        """
        self.id = None
        self.input  = None
    
    def function_init(self, host, port, user, password, database):
        dbClient = InfluxDBClient(
            host, 
            port, 
            user, 
            password, 
            database) 
        dbClient.create_database('Commands')
        return dbClient

    def influx_init(self):
        dbClient = self.function_init(host = 'localhost', port = 8086, user = 'grafana', password = 'bnlphysics', database = 'Commands')

        return dbClient

    def influxQuery(self, t):
        dbClient = self.influx_init()
        
        while True: 
            points = dbClient.query('select * from COMM;')#.get_points()
            print(points)
            time.sleep(t) #-> timing info
            
    def influx_data(self, setup, sender, command):
        dbClient = self.influx_init()
        
        loginEvents=[{
        "measurement": 'COMM',
        "tags":{"Setup":setup,"Sender":sender,"Receiver":"ITSDAQ"},
        "fields":{"Command":command,"Data":"","Error":"" }},]        
        
        dbClient.write_points(loginEvents) 

    def influx_data_itsdaq(self, setup, sender, command, data, error):
        dbClient = self.influx_init()
        
        loginEvents=[{
        "measurement": 'COMM',
        "tags":{"Setup":setup,"Sender":sender,"Receiver":"HYBRID_BURNIN"},
        "fields":{"Command":command,"Data":data,"Error":error }},]        
        
        dbClient.write_points(loginEvents) 

    def checkCommand(self, text):
        dbClient = self.influx_init()
        ResultSet = dbClient.query('SELECT * FROM COMM GROUP BY * ORDER BY time DESC LIMIT 1;')
        #logging.debug(ResultSet)  
        #print(ResultSet)
        if text in str(ResultSet):
            logging.debug(text + "" + "found.")
            #time.sleep(1)
        else:
            logging.debug(text + "" +  "not found.")
            print("Hello")

'''influx_test = InfluxDBHandler(None)
#_dbClient=influx_test.influx_init()
influx_test.checkCommand(text="INIT_BURNIN")'''


