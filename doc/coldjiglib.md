# Coldjiglib API 

Access to current hardware values are exclusively though INFLUX DB and not through coldjiglib 
_(this could change)_

All coldjiglib functions should be non-blocking and return immediately.   Functions that call processes that block should launch those in a thread with additional status functions to report progress. 
---
## ColdJigLib Settings 
- `coldJigLib.hardware_ini_file` : full path to Hardware INI file
- `coldJigLib.influx_ini_file` : full path to INFLUX INI file
- `coldJigLib.thermal_cycle_module` : name of thermal cycle module 
- `coldJigLib.RATE` : Set how often to run core_loop [default is 1s.  Do not adjust unless absolutely necessary]
- `coldJigLib.gas_flow_min` : Set minimum N2/dry-air flow. Flow less than this will fire interlock
---
## ColdJig Status
Communicates what the ColdJig is doing.
Written to data_dict (INFLUX Field) and INFLUX Tag

The defined status messages are:
- `IDLE` : Default state. Doing nothing
- `TC_START` : Thermal Cycling routine started
- `PRE_TC_CHARACTERISATION` : Module cold turn-on and first cold testing
- `TC_INITAL_COOLDOWN` : First step in ColdJig Thermal Cycling routine - cooling ColdJig
- `TC_WARMUP_<N>` : Fast warm up of thermal cycle <N> (1 < N < 1000)
- `TC_COOLDOWN_<N>` : Fast cool down of themal cycle <N> (1 < N < 1000)
- `TEST_START` : Called at start of ITSDAQ module testing
- `TEST_END` : Called at end of ITSDAQ module testing
- `POST_TC_WARMUP` : After thermal cycling complete, warm up for POST_TC and HV stabilisation
- `POST_TC_CHARACTERISATION` : Final module warm testing
- `HV_STABILISATION` : Set when running HV stabilisation test
- `TC_FINAL_WARMUP` : Last step in ColdJig thermal cyling routine. final warm up to room temperature
- `TC_END` : Thermal cycling routine has finished

---
## Message passing

ColdJigLib uses the pypubsub to pass important messages to other code (eg: ColdJig GUI).  These should be uses in instances where you want to bring attention to something that occurred or to take action (eg: Interlock fired, lower N2 flow now, etc). 

The defined messages topics are:
- 'warning' : eg: "humidity increasing", "N2 flow low"
- 'error' : eg: "No response from chiller", "can't connect to INFLUX"
- 'alert' : "Turn down N2 flow", "Safe to open lid", "module QC completed"
- 'danger' : "condensation on module", "interlock fired","Peltier overheating"

---
`start() -> bool`

- Connects and initialises hardware 
- starts core loop 
- shouldn't block
- launch thread for any long processes 
- return false if there's a problem 

----
`shutdown() -> bool`

- immediately shut down all jobs, threads, tasks 
- stop core loop
- Gracefully disengage hardware 
- return false if there's a problem 

---
`start_thermal_cycle(modules: list[int],
                     tc_warm_temp: float,
                     tc_cool_temp: float,
                     tc_warm_up_temp: float,
                     ncycle: int) :
 ) -> bool`

- start the module QC thermal cycle process for modules in list 
- parameters:
  - `modules` : list of modules on chucks to be tested 
    - Example: modules on chucks 1,3,5 would expect list = [1,3,5]
  - `tc_warm_temp` : high module temperature during thermal cycle (units: centigrade)
  - `tc_cool_temp` : low module temperature during thermal cycle (units: centigrade)
  - `tc_warm_up_temp` : final module temperature after compeltion of thermal cycle (units: centigrade)
  - `ncycle` : number of thermal cycles 
- launch thread to run thermal cycle sequence 
- return false for any errors 

---
`stop_thermal_cycle() -> bool`

- immediately stop thermal cycle
- turns off Peltiers if running  
- return false for any errors

---
`ColdJigLib.status -> string`
- returns Coldig's current status as a string 

----
