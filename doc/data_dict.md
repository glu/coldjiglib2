# data_dict keys

Date: 15-SEP-2021

This document list the keys defined, their meaning, and which Coldjigs use them

| key                     | Meaning                             | Data Type & Units               |UK_China_Barrel | US_Barrel | EndCap |
|-------------------------|-------------------------------------|---------------------------------|----------------|-----------|--------|
| RUN_NUMBER              | Thermal Cycle Run Number            | int                             | X              | X         | X      |
| CYCLE                   | Thermal Cycle number [1-10; 0≡IDLE] | int                             | X              | X         | X      |
| status                  | ColdJig status                      | string                          | X              | X         | X      |
| chiller.set_temperature | Set Chiller Temperature             | float [℃]                       | X              | X         | X      |
| chiller.set_state       | Turn Chiller On / Off               | string                          |                | X         |        |
| chiller.read_state      | Read Chiller log (On/Off)           | string                          |                | X         |        |
| chiller.set_pump        | Set Chiller pump speed              | int (units:chiller dependent)   | X              |           |        |
| chiller.temperature     | Read chiller (coolant) temperature  | float [℃]                       | X              | X         |        |
| chiller.pump            | Read chiller pump speed             | int (units:chiller dependent)   | X              |           |        |
| thermometer.C1          | Read chuck 1 temperature            | float [℃]                       | X              | X         | X      |
| thermometer.C2          | Read chuck 2 temperature            | float [℃]                       | X              | X         | X      |
| thermometer.C3          | Read chuck 3 temperature            | float [℃]                       | X              | X         | X      |
| thermometer.C4          | Read chuck 4 temperature            | float [℃]                       | X              | X         | X      |
| thermometer.C5          | Read chuck 5 temperature            | float [℃]                       | X              |           |        |
| thermometer.CB1         | Read chuck 1 cold-block temperature | float [℃]                       | X              |           |        |
| thermometer.CB2         | Read chuck 2 cold-block temperature | float [℃]                       | X              |           |        |
| thermometer.CB3         | Read chuck 3 cold-block temperature | float [℃]                       | X              |           |        |
| thermometer.CB4         | Read chuck 4 cold-block temperature | float [℃]                       | X              |           |        |
| thermometer.CB5         | Read chuck 5 cold-block temperature | float [℃]                       | X              |           |        |
| thermometer.COOL_IN     | Read Coolant In temperature         | float [℃]                       | X              |           |        |
| thermometer.COOL_OUT    | Read Coolant Out temperature        | float [℃]                       | X              |           |        |
| thermometer.AIR1        | Read AIR1 temperature               | float [℃]                       | X              | X         |        |
| thermometer.AIR2        | Read AIR2 temperature               | float [℃]                       | X              | X         |        |
| thermometer.LAB         | Read LAB temperature                | float [℃]                       | X              |           |        |
| thermometer.SPARE       | no thermocouple connected, empty    | float [℃]                       | X              |           |        |
| peltier.set_volt.1      | Set Peltier 1 Voltage               | float [V]                       | X              |           | X      |
| peltier.set_volt.2      | Set Peltier 2 Voltage               | float [V]                       | X              |           | X      |
| peltier.set_volt.3      | Set Peltier 3 Voltage               | float [V]                       | X              |           | X      | 
| peltier.set_volt.4      | Set Peltier 4 Voltage               | float [V]                       | X              |           | X      | 
| peltier.set_volt.5      | Set Peltier 5 Voltage               | float [V]                       | X              |           |        | 
| peltier.set_current.1   | Set Peltier 1 Current               | float [A]                       | X              |           | X      | 
| peltier.set_current.2   | Set Peltier 2 Current               | float [A]                       | X              |           | X      | 
| peltier.set_current.3   | Set Peltier 3 Current               | float [A]                       | X              |           | X      | 
| peltier.set_current.4   | Set Peltier 4 Current               | float [A]                       | X              |           | X      | 
| peltier.set_current.5   | Set Peltier 5 Current               | float [A]                       | X              |           |        | 
| peltier.volt.1          | Read Peltier 1 Voltage              | float [V]                       | X              |           | X      | 
| peltier.volt.2          | Read Peltier 2 Voltage              | float [V]                       | X              |           | X      |
| peltier.volt.3          | Read Peltier 3 Voltage              | float [V]                       | X              |           | X      |
| peltier.volt.4          | Read Peltier 4 Voltage              | float [V]                       | X              |           | X      |
| peltier.volt.5          | Read Peltier 5 Voltage              | float [V]                       | X              |           |        |
| peltier.current.1       | Read Peltier 1 Current              | float [A]                       | X              |           | X      |
| peltier.current.2       | Read Peltier 2 Current              | float [A]                       | X              |           | X      |
| peltier.current.3       | Read Peltier 3 Current              | float [A]                       | X              |           | X      |
| peltier.current.4       | Read Peltier 4 Current              | float [A]                       | X              |           | X      |
| peltier.current.5       | Read Peltier 5 Current              | float [A]                       | X              |           |        |
| peltier.set_mode        | Set all Peltiers' mode (HEAT/COOL)  | String (HEAT/COOL)              | X              |           |        |
| gas_flow                | Measure of Dry-Air / N_2 flow       | float [LPM/SCFM]                | X              | X         | X      |
| coolant_flow            | Measure of coolant flow             | float [LPM]                     | X              |           |        |
| RH.1                    | Humditiy sensor 1 reading           | float [%]                       | X              |           | X      |
| RH.2                    | Humidity sensor 2 reading           | float [%]                       | X              |           | X      |
| RH.3                    | Humidity sensor 3 reading           | float [%]                       |                |           | X      |
| RH.4                    | Humidity sensor 4 reading           | float [%]                       |                |           | X      |
| RH.5                    | Humidity sensor 5 reading           | float [%]                       |                |           | X      |
| DP.1                    | Dew Point Sensor 1 reading          | float [℃]                       | X              | X         | X      |
| DP.2                    | Dew Point Sensor 2 reading          | float [℃]                       | X              | X         | X      |
| Lid.1                   | Read Lid Switch 1 state             | String (OPEN/CLOSED)            | X              | X         | X      |
| Lid.2                   | Read Lid Switch 2 state             | String (OPEN/CLOSED)            | X              | X         | X      |
| ADC.0.set_DOUT0         | Set DAQC2Plate 0 Digital Output 0   | Boolean                         | X              |           |        |
| ADC.0.DIN0              | Read DAQC2Plate 0 Digital Input 0   | Boolean                         | X              |           |        |
| ADC.0.DIN1              | Read DAQC2Plate 0 Digital Input 1   | Boolean                         | X              |           |        |
| ADC.0.AIN0              | Read DAQC2Plate 0 Analog Input 0    | float [V]                       |                |           |        |
| ADC.0.AIN1              | Read DAQC2Plate 0 Analog Input 1    | float [V]                       |                |           |        |
| ADC.0.AIN2              | Read DAQC2Plate 0 Analog Input 2    | float [V]                       |                |           |        |
| ADC.0.AIN3              | Read DAQC2Plate 0 Analog Input 3    | float [V]                       |                |           |        |
| ADC.0.AIN4              | Read DAQC2Plate 0 Analog Input 4    | float [V]                       |                |           |        |
| ADC.0.AIN5              | Read DAQC2Plate 0 Analog Input 5    | float [V]                       |                |           |        |
| ADC.0.AIN6              | Read DAQC2Plate 0 Analog Input 6    | float [V]                       |                |           |        |
| ADC.0.AIN7              | Read DAQC2Plate 0 Analog Input 7    | float [V]                       |                |           |        |
| ADC.0.FREQ              | Read DAQC2Plate 0 Frequency         | float [Hz]                      | X              |           |        |
| AMAC_hybrid_X_0         | (Avg) Hybrid X Temperature Chuck 0  | float [℃]                       | X              | X         |        |
| AMAC_hybrid_X_1         | (Avg) Hybrid X Temperature Chuck 1  | float [℃]                       | X              | X         |        |
| AMAC_hybrid_X_2         | (Avg) Hybrid X Temperature Chuck 2  | float [℃]                       | X              | X         |        |
| AMAC_hybrid_X_3         | (Avg) Hybrid X Temperature Chuck 3  | float [℃]                       | X              | X         |        |
| AMAC_hybrid_X_4         | (Avg) Hybrid X Temperature Chuck 4  | float [℃]                       | X              |           |        |
| AMAC_hybrid_Y_0         | (Avg) Hybrid Y Temperature Chuck 0  | float [℃]                       | X              | X         |        |
| AMAC_hybrid_Y_1         | (Avg) Hybrid Y Temperature Chuck 1  | float [℃]                       | X              | X         |        |
| AMAC_hybrid_Y_2         | (Avg) Hybrid Y Temperature Chuck 2  | float [℃]                       | X              | X         |        |
| AMAC_hybrid_Y_3         | (Avg) Hybrid Y Temperature Chuck 3  | float [℃]                       | X              | X         |        |
| AMAC_hybrid_Y_4         | (Avg) Hybrid Y Temperature Chuck 4  | float [℃]                       | X              |           |        |
| LV.set_state_1          | Turn LV1     On / Off               | string                          |                | X         |        |
| LV.set_state_2          | Turn LV2     On / Off               | string                          |                | X         |        |
| RH.1                    | SHT85 RH sensor 1 reading           | float [%]                       |                | X         |        |
| RH.2                    | SHT85 RH sensor 2 reading           | float [%]                       |                | X         |        |
| thermometer.sht1        | SHT85  T sensor 1 reading           | float [℃]                       |                | X         |        |
| thermometer.sht2        | SHT85  T sensor 2 reading           | float [℃]                       |                | X         |        |
| HV.set_state_0          | Turn HV channel 0    On / Off       | string                          |                | X         |        |
| HV.set_state_1          | Turn HV channel 1    On / Off       | string                          |                | X         |        |
| HV.set_state_2          | Turn HV channel 2    On / Off       | string                          |                | X         |        |
| HV.set_state_3          | Turn HV channel 3    On / Off       | string                          |                | X         |        |
| HV.set_voltage_0        | Set  HV channel 0    voltage        | float [V]                       |                | X         |        |
| HV.set_voltage_1        | Set  HV channel 1    voltage        | float [V]                       |                | X         |        |
| HV.set_voltage_2        | Set  HV channel 2    voltage        | float [V]                       |                | X         |        |
| HV.set_voltage_3        | Set  HV channel 3    voltage        | float [V]                       |                | X         |        |
| HV.voltage_0            | Read HV channel 0    voltage        | float [V]                       |                | X         |        |
| HV.voltage_1            | Read HV channel 1    voltage        | float [V]                       |                | X         |        |
| HV.voltage_2            | Read HV channel 2    voltage        | float [V]                       |                | X         |        |
| HV.voltage_3            | Read HV channel 3    voltage        | float [V]                       |                | X         |        |
| HV.current_0            | Read HV channel 0    current        | float [uA]                      |                | X         |        |
| HV.current_1            | Read HV channel 1    current        | float [uA]                      |                | X         |        |
| HV.current_2            | Read HV channel 2    current        | float [uA]                      |                | X         |        |
| HV.current_3            | Read HV channel 3    current        | float [uA]                      |                | X         |        |
| LV.set_voltage_0        | Set  LV channel 0    voltage        | float [V]                       |                | X         |        |
| LV.set_voltage_1        | Set  LV channel 1    voltage        | float [V]                       |                | X         |        |
| LV.set_voltage_2        | Set  LV channel 2    voltage        | float [V]                       |                | X         |        |
| LV.set_voltage_3        | Set  LV channel 3    voltage        | float [V]                       |                | X         |        |
| LV.voltage_0            | Read LV channel 0    voltage        | float [V]                       |                | X         |        |
| LV.voltage_1            | Read LV channel 1    voltage        | float [V]                       |                | X         |        |
| LV.voltage_2            | Read LV channel 2    voltage        | float [V]                       |                | X         |        |
| LV.voltage_3            | Read LV channel 3    voltage        | float [V]                       |                | X         |        |
| LV.current_0            | Read LV channel 0    current        | float [mA]                      |                | X         |        |
| LV.current_1            | Read LV channel 1    current        | float [mA]                      |                | X         |        |
| LV.current_2            | Read LV channel 2    current        | float [mA]                      |                | X         |        |
| LV.current_3            | Read LV channel 3    current        | float [mA]                      |                | X         |        |



