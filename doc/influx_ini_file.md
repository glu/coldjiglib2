# INFLUX INI Settings

This INI file allows one to setup communication to INFLUX V1.8 or V2.x

- `bucket` : INFLUX bucket setting.  Leave empty for V1.8 
- `database` : INFLUX database name
- `IP` : INFLUX IP address
- `measurement` : INFLUX measurement name
- `org` : INFLUX orgnisation name. Set to "-" for V1.8
- `password` : INFLUX password.  Set to "  " if not set 
- `port` : INFLUX port (default: 8086)
- `retention` : INFLUX retention policy.  Set to " " if not set
- `token` : INFLUX API token. Leave empty for V1.8
- `url` : 
- `username` : INFLUX username to access database.  Set to " " if not set
- `version` " INFLUX version.  1 for 1.x and 2 for 2.x
