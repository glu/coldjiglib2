##instek is null modem (not pin to pin, need a specialized cable for serial)

import serial
import time

class Instek_LV:

    def __init__(self,location="/dev/ttyUSB0",baudrate=9600,outtime=5):
        try:
            self.device=serial.Serial(location, baudrate, timeout=outtime)
        except serial.SerialException:
            print("can't open the instek, is it connected on "+location+" ?")

    #def __del__(self):
    #    try:
    #        self.device.close()
    #    except AttributeError:
    #        pass
        
    def GetType(self):
        query="*IDN?\n"
        try:
            self.device.write(query.encode())
            idn=self.device.readline()
            #print(idn)
            return idn
        except serial.SerialTimeoutException:
            print("instek query from GetType() failed")

    def TurnOn(self):
        query="OUTP:STAT 1\n"
        self.device.write(query.encode())

    def TurnOff(self):
        query="OUTP:STAT 0\n"
        self.device.write(query.encode())
        self.device.flush()

    def SetCurrent(self,value=0,Channel=1):
        query=":CHAN"+str(int(Channel))+":CURR "+str(value)+"\n"
        #print(query)
        self.device.write(query.encode())
        return self.device.readline()

    def SetVoltage(self,value=0,Channel=1):
        query=":CHAN"+str(int(Channel))+":VOLT "+str(value)+"\n"
        #print(query)
        self.device.write(query.encode())
        #volts=self.device.readline()
        #return volts

    def GetCurrent(self,Channel=1):
        query=":CHAN"+str(int(Channel))+":CURR ?\n"
        self.device.write(query.encode())
        current=self.device.readline()
        #print(current)
        return float(current.encode())

    def GetVoltage(self,Channel=1):
        query=":CHAN"+str(int(Channel))+":VOLT ?\n"
        self.device.write(query.encode())
        voltage=self.device.readline()
        return float(voltage.encode())

    def GetActualCurrent(self,Channel=1):
        query=":CHAN"+str(int(Channel))+":MEAS:CURR ? \n"
        self.device.write(query.encode())
        current=self.device.readline()
        try:
            return float(current)
        except ValueError:
            pass

    def GetActualVoltage(self,Channel=1):
        query=":CHAN"+str(int(Channel))+":MEAS:VOLT ? \n"
        self.device.write(query.encode())
        volts=self.device.readline()
        try:
            return float(volts)
        except ValueError:
            pass

    def TurnOnCurrentProtection(self,Channel=1):
        query=":CHAN"+str(int(Channel))+":PROT:CURR 1 \n"
        self.device.write(query.encode())

    def TurnOffCurrentProtection(self,Channel=1):
        query=":CHAN"+str(int(Channel))+":PROT:CURR 0 \n"
        self.device.write(query.encode())

    def IsCurrentProtectionOn(self,Channel=1):
        query=":CHAN"+str(int(Channel))+":PROTection:CURRent ? \n"
        self.device.write(query.encode())
        print(self.device.readline())

    def SetVoltageProtection(self,value=0,Channel=1):
        query=":CHAN"+str(int(Channel))+":PROT:VOLT " +str(value)+"\n"
        self.device.write(query.encode())

    def GetVoltageProtection(self,Channel=1):
        query=":CHAN"+str(int(Channel))+":PROT:VOLT ?\n" 
        self.device.write(query.encode())
        return self.device.readline()

if __name__=="__main__":
    instk=Instek_LV()
    time.sleep(2)
    instk.GetType()
    time.sleep(2)
    instk.TurnOn()
    time.sleep(3)
    instk.SetVoltage(11.0)
    time.sleep(3)
    print(instk.GetActualVoltage(2))
    print(instk.GetActualVoltage(1))
    print(instk.GetActualCurrent(1))
    print(instk.GetActualCurrent(2))
    time.sleep(3)
    instk.TurnOff()