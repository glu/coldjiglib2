from abc import ABC, abstractmethod

class LV(ABC):
    
    @abstractmethod
    def set_Voltage(self,value,channel) :
        pass

    @abstractmethod
    def get_Voltage(self,channel) :
        pass

    @abstractmethod
    def set_Current(self,value,channel) :
        pass

    @abstractmethod
    def get_Current(self,channel) :
        pass
 