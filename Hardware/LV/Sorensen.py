from .. import Hardware 
import serial
import time 
import logging
from . import Sorensen_lib
logger = logging.getLogger(__name__)

class Instek(Hardware.Hardware) :
    def __init__(self,name) :
        super().__init__(name)
        self.__location = None
        self.__name = name
        self.__outtime = 5
        self.__state = 'OFF'
        self.__voltage = [0.0,0.0]
        self.__current = [0.0,0.0]
        self.__ovp = [11.5,11.5]
    
    def initialise(self,parameters) :
        """
        Initialise hardware
        """
        self.__location = parameters['Location']
        self.__baud= parameters['Baud']
        self.id = parameters['ID']
        try:
            self.__LV=Sorensen_Lib.Sorensen_LV(location=self.__location,baudrate=self.__baud,outtime=self.__outtime)
            #self.__LV.TurnOn()
        except :
            pass


    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        for i in range (0, 2):
            data_dict[f'LV.set_voltge_{self.id}_{i}'] = 11.0
            data_dict[f'LV.set_current_{self.id}_{i}'] = 0.0
            data_dict[f'LV.set_state_{self.id}'] = 'OFF'
            data_dict[f'LV.ovp_{self.id}_{i}'] = self.__ovp[i]
            data_dict[f'LV.current_{self.id}_{i}'] = self.__current[i]
            data_dict[f'LV.voltage_{self.id}_{i}'] = self.__voltage[i]
        
    def Read(self, data_dict) :
        """
        Read out all values and update data_dict
        """
        #print(f"reading LV_{self.id}")
        for i in range (0, 2):
            data_dict[f'LV.current_{self.id}_{i}'] =self.__LV.GetActualCurrent(i+1)
            data_dict[f'LV.voltage_{self.id}_{i}'] =self.__LV.GetSetVoltage(i+1)
            #data_dict[f'LV.ocp_{self.id}_{i}'] = None
            #data_dict[f'LV.set_state_{self.id}'] = self.__state 
        #print(f"done read LV_{self.id}")

    def Set(self, data_dict) :
        """
        Read data_dict for new chiller settings & apply them
        """
        if data_dict[f'LV.set_state_{self.id}'] =='ON':  #and self.__state=="OFF":
            self.__LV.TurnOn(1)
            self.__LV.TurnOn(2)
            self.__state = 'ON'
        elif data_dict[f'LV.set_state_{self.id}']=='OFF': #and self.__state=="ON":
            self.__LV.TurnOff(1)
            self.__LV.TurnOff(2)
            self.__state = 'OFF'

        for i in range(0,2):
            self.__LV.SetVoltage(data_dict[f'LV.set_voltge_{self.id}_{i}'],i+1)
            #self.device.set_Current(data_dict[f'LV.set_current_{self.id}__{i}'],i)

    def Shutdown(self) :
        """
        Called to release resources
    
        """
        self.__LV.TurnOff(1)
        self.__LV.TurnOff(2)
        self.__state='OFF'
        logging.info(f'Shutting down {self.name}')
        # CALL ANY METHOD TO RELEASE RESOURCES IF NEEDED
    
    def set_Voltage(self,value,channel) :
        self.__LV.SetVoltage(value,channel)

    def get_Voltage(self,channel) :
        self.__LV.GetVoltage(channel)

    def set_Current(self,value,channel) :
        self.__LV.SetCurrent(value,channel)

    def get_Current(self,channel) :
        self.__LV.GetCurrent(channel)
 