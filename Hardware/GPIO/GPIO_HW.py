from .. import Hardware
import logging
logger = logging.getLogger(__name__)

import RPi.GPIO as GPIO

class GPIO_HW(Hardware.Hardware):

    def __init__(self, name):
        '''
        Constuctor for GPIO
        '''
        super().__init__(name)

        self._GOUTn = []

    def initialise(self, parameters):
        '''
        Initialize Hardware
        '''
        GPIO.setmode(GPIO.BCM)

        param = lambda x : list(map(int,x.split(',')))

        if 'GOUT' in parameters:
            self._GOUTn = param(parameters['GOUT'])

        for pin in self._GOUTn:
            GPIO.setup(pin, GPIO.OUT)

    def add_data_dict_keys(self, data_dict):
        '''
        Add keys to the data dictionary
        '''
        for pin in self._GOUTn:
            data_dict[f'GPIO.set_GOUT{pin}'] = 0

    def Read(self, data_dict):
        '''
        No values to Read
        '''
        pass

    def Set(self, data_dict):
        '''
        Set the values
        '''
        for pin in self._GOUTn:
            self._GOUT(pin, data_dict[f'GPIO.set_GOUT{pin}'])

    def Shutdown(self):
        '''
        Called to release resources
        Nothing to do - empty function
        '''
        pass

    def Cleanup(self):
        GPIO.cleanup()

    def _GOUT(self, pin, mode):
        try:
            if mode == 1:
                GPIO.output(pin, GPIO.HIGH)
            elif mode == 0:
                GPIO.output(pin, GPIO.LOW)
        except:
            logger.debug('Failed to change the GPIO pins', exc_info = True)
