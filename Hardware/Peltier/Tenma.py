# Wrapper for the TENMA power supply

from .. import Hardware 
import logging
import time
import tenma

class Tenma(Hardware.Hardware) :

    def __init__(self,name) :
        """
        Constructor for Tenma class
        """
        super().__init__(name)
        self.UsbPort = None
        self.Chuck = None
        self.__tenma = None

        # Dummy variables for holding TENMA voltage and current
        self.__volt = 0.0
        self.__current = 0.0

    def initialise(self,parameters) :
        """
        Initialise hardware using values defined in 
        parameters dictionary
        """
        self.UsbPort = parameters['UsbPort']
        self.Chuck = parameters['Chuck']

        # Create an instance of Tenma PSU and open connection and 
        # and check serial number can be readout 
        logging.debug(f'Opening {self.name} on {self.UsbPort}')
        self.__tenma = tenma.Tenma(self.UsbPort)
        try :
            self.__tenma.open()    
            logging.debug(f'{self.name} serial number: {self.__tenma.ID}')
        except :
            logging.critical(f'Failed to open serial connection to {self.name}')
            raise

        # Turn output off
        self.__tenma.output = "OFF"
        # Set I/V to 0 
        self.__tenma.vset = 0.0
        self.__tenma.iset = 0.0
        # Turn on output    
        self.__tenma.output = "ON"
    
    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        
        # Read out tenma to initialise data_dict keys
        volt = self.__tenma.volt
        vset = self.__tenma.vset

        current = self.__tenma.current
        iset = self.__tenma.iset

        data_dict[f'peltier.volt.{self.Chuck}'] = volt
        data_dict[f'peltier.set_volt.{self.Chuck}'] = vset

        data_dict[f'peltier.current.{self.Chuck}'] = current
        data_dict[f'peltier.set_current.{self.Chuck}'] = iset

        logging.debug(f'VSet/Iset: {vset}/{iset}  V/I: {volt}/{current}')

    
    def Read(self, data_dict) :
        """
        Reading out all TENMA voltage and current
        """
        volt = self.__tenma.volt
        current = self.__tenma.current

        data_dict[f'peltier.volt.{self.Chuck}'] = volt
        data_dict[f'peltier.current.{self.Chuck}'] = current

        logging.debug(f'V/I: {volt}/{current}')


    def Set(self, data_dict) :
        """
        Set the TENMA voltage and current
        """
        self.__tenma.vset = data_dict[f'peltier.set_volt.{self.Chuck}']        
        #time.sleep(0.5)
        self.__tenma.iset = data_dict[f'peltier.set_current.{self.Chuck}']

        vset = data_dict[f'peltier.set_volt.{self.Chuck}']        
        iset = data_dict[f'peltier.set_current.{self.Chuck}']
        logging.debug(f'VSet/Iset: {vset}/{iset}')


    def Shutdown(self) :
        """
        Called to release resources
        """
        logging.info(f"Shutdown {self.name} -- setting V/I to 0V/0A")        
        self.__tenma.vset = 0.0
        self.__tenma.iset = 0.0

        while (i := self.__tenma.current) > 0.01 :
            logging.info(f'{self.name} I={i}: Waiting for current to reach ~0A')
            time.sleep(0.001)
        self.__tenma.output = "OFF"

        self.__tenma.close()


