# Dummy 4-channel HV module for code testing
# Simulate HV connected to 1-ohm resistor

class HVTest :

    def __init__(self,location,name="TEST_LV") :
        self.__location = location
        self.__name = name
        self.__state = "OFF"
        self.__voltage = 0.0
        self.__current = 0.0
        self.__ovp = 10.0
        self.__ovc = 1.0
        self.__vrange = 100.0
        
    def TurnOn(self) :
        self.__state = "ON"

    def TurnOff(self) :
        self.__state = "OFF"

    def SetVoltageLevel(self,value) :
        self.__voltage = value
        self.__current= value

    def SetVoltageRange(self,v_range) :
        self.__vrange = v_range

    def GetVoltage(self) :
        return self.__voltage

    def SetCurrentLevel(self,value) :
        self.__current = value
        self.__voltage = value

    def GetCurrent(self) :
        return self.__current

    def SetVoltageProtection(self,value) :
        self.__ovp = value

    def SetCurrentCompliance(self,value) :
        self.__ovc = value

    def ToRear(self) :
        pass

    
