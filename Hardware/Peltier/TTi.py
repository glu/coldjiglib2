# Wrapper for TTi power supply
from logging import debug
from .. import Hardware 
import logging

# TTi Driver
# IMPORT TTi DRIVER HERE 


class TTi(Hardware.Hardware) :

    def __init__(self,name) :
        """
        Constructor for TTi class
        """
        super().__init__(name)
        self.UsbPort = None
        self.Chuck = None

        # CREATE YOUR TTi OBJECT
        # tti = ....


    def initialise(self,parameters) :
        """
        Initialise hardware using values defined in 
        parameters dictionary
        """
        self.UsbPort = parameters['UsbPort']
        self.Chuck = parameters['Chuck']

    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict[f'peltier.volt.{self.Chuck}'] = None
        data_dict[f'peltier.set_volt.{self.Chuck}'] = None

        data_dict[f'peltier.current.{self.Chuck}'] = None
        data_dict[f'peltier.set_current.{self.Chuck}'] = None
    
    def Read(self, data_dict) :
        """
        Reading out all TTi voltage and current
        """
        data_dict[f'peltier.volt.{self.Chuck}'] = self.voltage()
        data_dict[f'peltier.current.{self.Chuck}'] = self.current()

    def Set(self, data_dict) :
        """
        Set the TTi voltage and current
        """
        self.set_voltage(data_dict[f'peltier.set_volt.{self.Chuck}'])
        self.set_current(data_dict[f'peltier.set_current.{self.Chuck}'])


    def Shutdown(self) :
        """
        Called to release resources
        """
        logging.info(f"Shutdown {self.name}")
        # CALL FUNCTION TO RELEASE CONNECTION IF NEEDED

    
    def set_voltage(self,volt) :
        # CALL TTi DRIVER SET VOLTAGE HERE
        # tti.set_voltage(volt) 
        logging.debug(f'TTI Set Voltage: {volt}')

    def voltage(self) :
        # CALL TTi DRIVER GET VOLTAGE HERE
        # return tti.voltage()
        logging.debug(f'TTI Get Voltage called')

    def set_current(self,current) :
        # CALL TTi DRIVER SET CURRENT HERE
        logging.debug(f'TTi Set CurrentL {current}')

    def current(self) :
        # CALL TTi DRIVER GET CURRENT HERE
        logging.debug(f'TTi Get Current called')

    def OVC(self) :
        # CALL TTi DRIVER OVER CURRENT SET HERE
        logging.debug(f'TTi OVER CURRENT SET CALLED')

    def OVV(self) :
        # CALL TTi DRIVER OVER VOLTAGE SET HERE
       logging,debug(f'TTi OVER VOLTAGE SET CALLED')

    def alarm(self) :
        # CALL TTi DRIVER GET ALARMS HERE
        logging.debug(f'TTi GET ALARMS CALLED')


