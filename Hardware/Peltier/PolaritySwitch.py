from .. import Hardware
import logging
logger = logging.getLogger(__name__)

class PolaritySwitch(Hardware.Hardware) :

    def __init__(self,name) -> None:
        '''
        Constructor for Peltier-Polarity Switch class
        '''
        super().__init__(name)
        self.mode_val = {'HEAT' : 1, 'COOL' : 0}
        self.Output_list = None

    def initialise(self,parameters) :
        '''
        Initialise hardware (Polarity Switch)
        '''
        self.Output_list = [parameters[key] for key in parameters.keys() if 'output' in key.lower()]

    def add_data_dict_keys(self,data_dict) :
        '''
        Add keys to the data dictionary
        '''
        data_dict['peltier.set_mode']  = 'COOL'

    def Read(self, data_dict) :
        '''
        No values to read - empty function
        '''
        pass

    def Set(self, data_dict):
        '''
        Set the Peltier mode by setting the output state
        '''
        try :
            for output in self.Output_list:
                data_dict[output] = self.mode_val[data_dict['peltier.set_mode']]
        except KeyError as e:
            logger.warning(f'failed to set peltier mode {e}')

    def Shutdown(self) :
        '''
        Called to release resources
        Nothing to do - empty function
        '''
        pass
