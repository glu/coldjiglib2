# Wrapper for setting Peltier into 
# heating or cooling mode

from .. import Hardware 
import logging

class Relay(Hardware.Hardware) :

    def __init__(self,name) -> None:
        """
        Constructor for Peltier-Relay class
        """
        super().__init__(name)
        self.output_val = {'HEAT' : 1, 'COOL' : 0}
        self.output = None 
        
    def initialise(self,parameters) :
        """
        Initialise hardware using values defined in
        parameters dictionary
        """
        self.output = parameters['Output']

    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict['peltier.set_mode']  = 'COOL' 

    def Read(self, data_dict) :
        """
        No values to read - empty function
        """
        pass

    def Set(self, data_dict) :
        """
        Set the peltier mode by setting the output state
        """
        try : 
            data_dict[self.output] = \
              self.output_val[data_dict['peltier.set_mode']]        
        except KeyError as e:
            logging.warning(f'failed to set peltier mode {e}')


    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass 

    
