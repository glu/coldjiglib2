from .. import Hardware
import logging
logger = logging.getLogger(__name__)

# import your Power Supply

from .TTiCPX400 import TTiCPX400

class TTiDP(Hardware.Hardware):

    def __init__(self, name):
        '''
        Constuctor for TTi
        '''

        super().__init__(name)

        # Set variables to None
        self._UsbPort  = None
        self._Chuck    = None
        self._Channel  = None
        self._tti      = None

    def initialise(self, parameters):
        '''
        Initialize Hardware
        '''
        # Set values from configuration
        self._UsbPort  = parameters['UsbPort']
        self._Chuck    = parameters['Chuck']
        self._Channel  = parameters['Channel']

        # Initialize the power supply
        try:
            self._tti = TTiCPX400(self._UsbPort)
            logger.info(f'Initialized TTi. Port:{self._UsbPort}')
        except serial.SerialException:
            logger.error(f'Failed to initialize TTi at port:{self._UsbPort}', exc_info = True)

        # Turn on the power supply
        try:
            self._tti.enable_output(self._Channel)
            logger.info(f'TTi enabled channel, channel:{self._Channel}')
        except:
            logger.error(f'Failed to enable channel:{self._Channel}', exc_info = True)

    def add_data_dict_keys(self, data_dict):
        '''
        Add keys to the data dictionary
        '''
        
        logger.info('Adding data dictionary keys for TTiCPX400DP.')

        try:
            # Set current/voltage to zero to start
            data_dict[f'peltier.set_volt.{self._Chuck}']    = 0.0
            data_dict[f'peltier.set_current.{self._Chuck}'] = 0.0
            # Get actual values for current/voltage
            data_dict[f'peltier.volt.{self._Chuck}']    = self._tti.get_actual_voltage(self._Channel)
            data_dict[f'peltier.current.{self._Chuck}'] = self._tti.get_actual_current(self._Channel)
        except:
            logger.error(f'Failed to read current and voltage while setting dictionary keys', exc_info = True)


    def Read(self, data_dict):
        '''
        Read voltage and current and update data_dict
        '''
        
        try:
            data_dict[f'peltier.volt.{self._Chuck}']    = self._tti.get_actual_voltage(self._Channel)
            data_dict[f'peltier.current.{self._Chuck}'] = self._tti.get_actual_current(self._Channel)
        except:
            logger.error('Failed to read voltage, current, and update data_dict', exc_info = True)

    def Set(self, data_dict):
        '''
        Set the TTiCPX400 voltage and current
        '''

        # Set voltage
        try:
            self._tti.set_voltage(self._Channel, data_dict[f'peltier.set_volt.{self._Chuck}'])
        except:
            logger.error('Failed to set voltage', exc_info = True)

        # Set current
        try:
            self._tti.set_current(self._Channel, data_dict[f'peltier.set_current.{self._Chuck}'])
        except:
            logger.error('Failed to set current', exc_info = True)

    def Shutdown(self):
        '''
        Close connection for TTiCPX400DP
        '''
        logger.info('Closing connection for TTiCPX400DP')

        # Disable output
        try:
            self._tti.disable_output(self._Channel)
        except:
            logger.error('Failed to shut down TTiCPX400DP.', exc_info = True)
