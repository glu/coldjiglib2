from .. import Hardware
import logging
logger = logging.getLogger(__name__)

# import your Power Supply

from .BKPrecision_1902 import BKPrecision_1902

class BK_Precision(Hardware.Hardware):

    def __init__(self, name):
        '''
        Constructor for BK
        '''
        super().__init__(name)

        # Set variables to None
        self.UsbPort  = None
        self.Chuck    = None
        self._bk      = None

     def initialise(self, parameters):
        '''
        Initialize Hardware
        '''
        # Set values from configuration
        self.UsbPort  = parameters['UsbPort']
        self.Chuck    = parameters['Chuck']

        # Initialize the power supply
        try:
            self._bk = BKPrecision_1902(self.UsbPort)
            logger.debug(f'Initialized BK. Port:{self.UsbPort}')
        except serial.SerialException:
            logger.error(f'Failed to initialize BK at port:{self.UsbPort}', exc_info = True)

     def add_data_dict_keys(self, data_dict):
        '''
        Add keys to the data dictionary
        '''
        logger.info('Adding data dictionary keys for Peltier Power supply.')

        try:
            # Set current/voltage to zero to start
            data_dict[f'peltier.set_volt.{self.Chuck}']    = 0.0
            data_dict[f'peltier.set_current.{self.Chuck}'] = 0.0
            # Get actual values for current/voltage
            data_dict[f'peltier.volt.{self.Chuck}']    = self._bk.get_actual_voltage()
            data_dict[f'peltier.current.{self.Chuck}'] = self._bk.get_actual_current()
        except:
            logger.error(f'Failed to read current and voltage while setting dictionary keys', exc_info = True)

    def Read(self, data_dict):
        '''
        Read voltage and current and update data_dict
        '''
        try:
            data_dict[f'peltier.volt.{self.Chuck}']    = self._bk.get_actual_voltage()
            data_dict[f'peltier.current.{self.Chuck}'] = self._bk.get_actual_current()
       except:
            logger.error('Failed to read voltage, current, and update data_dict', exc_info = True)

   def Set(self, data_dict):
        '''
        Set the BK Precision voltage and current
        '''
        # Set voltage
        try:
            self._bk.set_voltage(data_dict[f'peltier.set_volt.{self.Chuck}'])
        except:
            logger.error('Failed to set voltage', exc_info = True)

        # Set current
        try:
            self._bk.set_current(data_dict[f'peltier.set_current.{self.Chuck}'])
        except:
            logger.error('Failed to set current', exc_info = True)

        # enabling the channel output
        try:
            self._bk.enable_output()
        except:
            logger.error(f'Failed to enable output', exc_info = True)
        
            
    def Shutdown(self):
        '''
        Close connection for BK
        '''
        logger.info('Closing connection for BK')

        # Disable output
        try:
            self._bk.disable_output()
        except:
            logger.error(f'Failed to shut down BK.', exc_info = True)
