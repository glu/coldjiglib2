from .. import Hardware 
import logging
import time

from .HVTest import HVTest

class PSU_Simulator(Hardware.Hardware) :

    def __init__(self,name) :
        """
        Constructr of Simulator Peltier PSU object
        """
        super().__init__(name)
        self.chuck = None 
        self.__psu = None 

    def initialise(self, parameters):
        """
        Initialise hardware
        """
        self.chuck = parameters['Chuck']
        self.__psu = HVTest(parameters['UsbPort'])
        self.__psu.TurnOn()

    def add_data_dict_keys(self, data_dict):
        """
        Add keys to the data dictionary
        """
        data_dict[f'peltier.volt.{self.Chuck}'] = None
        data_dict[f'peltier.set_volt.{self.Chuck}'] = None

        data_dict[f'peltier.current.{self.Chuck}'] = None
        data_dict[f'peltier.set_current.{self.Chuck}'] = None


    def Read(self, data_dict):
        """
        Read out values and update data_dict
        """
        data_dict[f'peltier.volt.{self.Chuck}'] = self.__volt()
        data_dict[f'peltier.current.{self.Chuck}'] = self.__current()


    def Set(self, data_dict):
        """
        Read data_dict for new Peltier PSU settings and apply them
        """
        self.__set_volt(data_dict[f'peltier.set_volt.{self.Chuck}'])
        self.__set_current(data_dict[f'peltier.set_current.{self.Chuck}'])


    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass
    

    def __volt(self) :
        volt = self.__psu.GetVoltage()
        logging.debug(f'Chuck {self.chuck} Peltier PSU: {volt} V')      
        return volt

    def __set_volt(self,value) :
        logging.debug(f'Chuck {self.chuck} Peltier PSU: Set to {value} V')
        self.__psu.SetVoltageLevel(value)

    def __current(self) :
        current = self.__psu.GetCurrent()
        logging.debug(f'Chuck {self.chuck} Peltier PSU: {current} A')
        return current

    def __set_current(self,value) :
        logging.debug(f'Chuck {self.chuck} Peltier PSU: Set to {value} A')
        self.__psu.SetCurrentLevel(value)


