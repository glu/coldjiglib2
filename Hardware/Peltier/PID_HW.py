# PID controller 
import logging
logger = logging.getLogger(__name__)

class PID_Controller() :

    def __init__(self,pid_params,currentLimit) -> None:
        """
        PID Controller Constructor
        - pid_params: Dictionary of PID parameters
            {'PID_MODE_1': {'ki':ki,'kp':kp}, 'PID_MODE_2': {'ki':ki,'kp':kp}, ... }
        - currentLimit: maximum current of Peltier power supply

        NB: IDLE mode is a special mode:  all PID parameters are reset to zero 
        and output current is not updated
        """

        self.pid_params = pid_params
        self.currentLimit = currentLimit

        self._mode = "IDLE"     # PID Mode

        # Tempertature values
        self.temperatureDifference = 0.0    # Temperature Difference
        self.targetTemperature = 0.0        # Target temperature

        # PID Parameters
        self.kp = 0.0   # PID k_p constant
        self.ki = 0.0   # PID k_i constant

        # PID components
        self.P = 0.0    # PID P values (kp * dT)
        self.I = 0.0    # PID I vlaues (ki * integral)
        self.integral = 0.0 # Integral value

        # Output currents 
        self.proposed_current = 0.0 # PID proposed current = P + I
        self.actual_current = 0.0   # Acutal current applied
        self.saturation = False     # PID current limited

    # PID Mode getter
    @property
    def mode(self) :
        return self._mode

    # PID Mode setter
    @mode.setter
    def mode(self,value) :
        # If the mode has changed, update the PID parameters
        if self.mode != value :            
            logger.debug(f"Mode updated => old mode:{self.mode}  new mode:{value}")

            # For IDLE, reset all parameters
            if value == "IDLE" :
                self.kp = 0.0
                self.ki = 0.0
                self.P  = 0.0
                self.I  = 0.0

                self.temperatureDifference = 0.0   
                self.targetTemperature = 0.0       

                self.proposed_current = 0.0 
                self.actual_current = 0.0   
                self.saturation = False     

            # .. else load new PID parameter
            else :
                params = self.pid_params[value]
                self.ki = params['ki']
                self.kp = params['kp']


            self.integral = 0.0   # Reset integral            
            logger.debug(f'New PID params loaded and integral reset [ki:{self.ki},kp:{self.kp},integral{self.integral}]')

        self._mode = value # update mode
        logger.debug(f'Mode updated')    
        


    def setCurrent(self,currentTemperature):
        """
        Calculate the new Peltier current based on the current temperatuee
        returns new current to set Peltier
        """

        # skip for IDLE mode & return None
        if self.mode == "IDLE" :
            return None
        
        # Calculate the temperature difference to the current target temperature
        self.temperatureDifference =  currentTemperature - self.targetTemperature

        # Proportional
        # Define the proportional term as the temperature difference times the factor kp
        self.P = self.temperatureDifference * self.kp

        # Integral
        self.I = self.integral * self.ki

        # We create a proposed current as the sum of the Proportional and Integral parameters
        self.proposed_current = self.P + self.I

        # Create a set current of 0
        self.actual_current = 0.0
        
        if self.proposed_current > self.currentLimit:
            # If the proposed current is larger than the maximum allowed current, we set the
            # set current to the maximum
            self.actual_current = self.currentLimit
            self.saturation = True
        elif self.proposed_current < 0.0:
            # If the proposed current is less than 0.0 we set it to 0.0
            self.actual_current = 0.0
            self.saturation = True
        else:
            # If the proposed current is not larger than the maximum, or less than 0.0
            # We set the current to the proposed current
            self.actual_current = self.proposed_current
            self.saturation = False

        if not self.saturation :
            # If the PID is not saturated, the current temperature difference is appended to the integral list
            # and the Integral parameter is defined as the sum of that list times the factor ki.
            # This gives the PID knowledge of the history of the system, and helps it reach an equilibrium between
            # current and cooling needed
            self.integral += self.temperatureDifference

        logger.debug(f'T:{currentTemperature} dT:{self.temperatureDifference} P:{self.P} I:{self.I} \
                    Proposed:{self.proposed_current} Actual:{self.actual_current} Saturation:{self.saturation}')

        return self.actual_current
