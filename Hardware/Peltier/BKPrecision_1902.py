import serial
import time
import decimal
import logging
logger = logging.getLogger(__name__)

class BKPrecision_1902():

    def __init__(self, port, timeout = 3):
        '''
        Constructor of the BK precision object
        '''
        # Start serial connection
        try:
            self.ser  = serial.Serial(port, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout=timeout)
        except serial.SerialException:
            logger.error('Failed to establish serial connection.', exc_info = True)

    def _send_command(self, command):
        '''
        Send command to power supply
        '''
        self.ser.write(command.encode())
        return self.ser.read_until(expected=b'\r')

    def _receive(self, command):
        reply = []
        notDone = True
        self.ser.write(command.encode())
        while(notDone):
            r = self.ser.read_until(expected=b'\r')
            r.decode()
            if(len(r) == 0):
                notDone = False
            else:
                reply.append(r)
        return reply

    def enable_output(self):
        '''
        Enable output
        '''
        status = 0
        self._send_command("SOUT%01d\r"%status)

    def disable_output(self):
        '''
        Disable output
        '''
        status = 1
        self._send_command("SOUT%01d\r"%status)

    def set_voltage(self, voltage):
        '''
        Set voltage
        '''
        data = self._get_data()
        max_volt = float(data['max_volt'])
        _v = int(voltage*10)
        _vmax = int(max_volt*10)
        if voltage <= max_volt:
            self._send_command("VOLT%03d\r"%_v)
        else:
            self._send_command("VOLT%03d\r"%_vmax)

    def set_current(self, current):
        '''
        Set current
        '''
        data = self._get_data()
        max_curr = float(data['max_curr'])
        _c = int(current*10)
        _cmax = int(max_curr*10)
        if current <= max_curr:
            self._send_command("CURR%03d\r"%_c)
        else:
            self._send_command("CURR%03d\r"%_cmax)
        
    def _get_data(self):
        data = {}
        reply = self._receive("GETD\r")
        data['volt'] = int(reply[0][0:4])/100.
        data['curr'] = int(reply[0][4:8])/100.

        # Maximum Value of Voltage and Current
        reply = self._receive("GMAX\r")
        data['max_volt'] = int(reply[0][0:3])/10.
        data['max_curr'] = int(reply[0][3:6])/10.
        
        return data

    def set_VLimit(self, _V):
        _V = int(_V*10)
        self._send_command("SOVP%03d\r"%_V)

    def get_actual_voltage(self):
        data = self._get_data()
        return (data['volt'])

    def get_actual_current(self):
        data = self._get_data()
        return (data['curr'])



