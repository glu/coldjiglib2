# Wrapper for a generic analog Humidity sensor

from .. import Hardware 
from . import PID_HW

import logging
logger = logging.getLogger(__name__)


from json import loads as json_loads

class PID(Hardware.Hardware) :

    def __init__(self,name) -> None:
        """
        Constructor
        """
        super().__init__(name)
        self.name                   =   name
        self.currentTemperature     =   0.0

    def initialise(self,parameters) :
        """
        Initialise parameters
        """
        currentLimit                =   float(parameters['CurrentLimit'])
        pid_parameters              =   json_loads(parameters['CycleParameters'])
        self.pid                    =   PID_HW.PID_Controller(pid_parameters,currentLimit)

        self.chuck                  =   parameters['Chuck']
        self.thermometer            =   parameters['Input_Temp']
        self.peltier_control        =   parameters['Output_control']
        

    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict[f'PID.Chuck{self.chuck}.set_mode']                = "IDLE" 
        data_dict[f'PID.Chuck{self.chuck}.mode']                    = "IDLE"
        data_dict[f'PID.Chuck{self.chuck}.CurrentLimit']            =   0.0
        data_dict[f'PID.Chuck{self.chuck}.ProposedCurrent']         =   0.0
        data_dict[f'PID.Chuck{self.chuck}.ActualCurrent']           =   0.0
        data_dict[f'PID.Chuck{self.chuck}.targetTemperature']       =   0.0
        data_dict[f'PID.Chuck{self.chuck}.TemperatureDifference']   =   0.0
        data_dict[f'PID.Chuck{self.chuck}.P']                       =   0.0
        data_dict[f'PID.Chuck{self.chuck}.I']                       =   0.0
        data_dict[f'PID.Chuck{self.chuck}.Integral']                =   0.0
        data_dict[f'PID.Chuck{self.chuck}.kp']                      =   0.0
        data_dict[f'PID.Chuck{self.chuck}.ki']                      =   0.0
        data_dict[f'PID.Chuck{self.chuck}.saturation']              =   False


    def Read(self, data_dict) :
        """
        Read latest values
        """
        # Get the latest PID mode, current temperature, and target temperature
        self.pid.mode = data_dict[f'PID.Chuck{self.chuck}.set_mode']
        data_dict[f'PID.Chuck{self.chuck}.mode'] = self.pid.mode
        self.currentTemperature = data_dict[self.thermometer]
        self.pid.targetTemperature = data_dict[f'PID.Chuck{self.chuck}.targetTemperature']


    def Set(self, data_dict) :
        """
        Do the PID work
        """        

        # Calculate new peltier current
        new_peltier_current = self.pid.setCurrent(self.currentTemperature)

        # Skip setting current if PID mode is IDLE
        if self.pid.mode == "IDLE" :
            logger.debug(f"PID IDLE - skip setting {self.peltier_control}")
        else :
            data_dict[self.peltier_control] = new_peltier_current

        data_dict[f'PID.Chuck{self.chuck}.ProposedCurrent']         =   self.pid.proposed_current
        data_dict[f'PID.Chuck{self.chuck}.ActualCurrent']           =   new_peltier_current
        data_dict[f'PID.Chuck{self.chuck}.TemperatureDifference']   =   self.pid.temperatureDifference
        data_dict[f'PID.Chuck{self.chuck}.P']                       =   self.pid.P
        data_dict[f'PID.Chuck{self.chuck}.I']                       =   self.pid.I
        data_dict[f'PID.Chuck{self.chuck}.Integral']                =   self.pid.integral
        data_dict[f'PID.Chuck{self.chuck}.kp']                      =   self.pid.kp
        data_dict[f'PID.Chuck{self.chuck}.ki']                      =   self.pid.ki
        data_dict[f'PID.Chuck{self.chuck}.saturation']              =   self.pid.saturation


    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass 
