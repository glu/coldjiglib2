import serial

class TTiCPX400():

    def __init__(self, port, timeout=6):
        self.timeout = timeout
        self.ser = serial.Serial(port, timeout=self.timeout, write_timeout=timeout)

    def _send_command(self, command):
        command = command + '\n'
        self.ser.write(command.encode())

    def _receive(self):
        ret = self.ser.read_until()
        return ret.decode()

    def enable_output(self, channel):
        query = 'OP{} 1'.format(channel)
        self._send_command(query)

    def disable_output(self, channel):
        query = 'OP{} 0'.format(channel)
        self._send_command(query)

    def get_voltage(self, channel):
        query = 'V{}?'.format(channel)
        self._send_command(query)
        reply = self._receive()
        return float(reply[3:])

    def get_actual_voltage(self, channel):
        query = 'V{}O?'.format(channel)
        self._send_command(query)
        reply = self._receive()
        return float(reply[:-3])

    def get_overvoltage_protection(self, channel):
        query = 'OVP{}?'.format(channel)
        self._send_command(query)
        reply = self._receive()
        return float(reply)

    def set_voltage(self, channel, voltage):
        query = 'V{c} {v}'.format(c=channel, v=voltage)
        self._send_command(query)

    def set_overvoltage_protection(self, channel, voltage):
        query = 'OVP{c} {v}'.format(c=channel, v=voltage)

    def get_current(self, channel):
        query = 'I{}?'.format(channel)
        self._send_command(query)
        reply = self._receive()
        return float(reply[3:])

    def get_actual_current(self, channel):
        query = 'I{}O?'.format(channel)
        self._send_command(query)
        reply = self._receive()
        return float(reply[:-3])

    def get_overcurrent_protection(self, channel):
        query = 'OCP{}?'.format(channel)
        self._send_command(query)
        reply = self._receive()
        return float(reply)

    def set_current(self, channel, current):
        query = 'I{c} {v}'.format(c=channel, v=current)
        self._send_command(query)

    def set_overcurrent_protection(self, channel, current):
        query = 'OCP{c} {v}'.format(c=channel, v=current)
