# Wrapper for the PiPlates DAQC2Plates 

from .. import Hardware 
import logging

import piplates.DAQC2plate as DAQC2

class DAQC2Plate(Hardware.Hardware) :

    def __init__(self,name) :
        """
        Constructor for DAQC2Plate class
        """
        super().__init__(name)
        self.address = None 
        self.__DIN_CH = []
        self.__DOUT_CH = []
        self.__AIN_CH = []
        self.__FREQ = False


    def initialise(self,parameters) :
        """
        Initialise the DAQC2Plate using the values 
        defined in parameters
        """
        self.address = int(parameters['Address'])

        param2ch = lambda x : list(map(int,x.split(',')))

        if 'DIN' in parameters :
            self.__DIN_CH = param2ch(parameters['DIN'])
   
        if 'DOUT' in parameters :
            self.__DOUT_CH = param2ch(parameters['DOUT'])
    
        if 'AIN' in parameters :
            self.__AIN_CH = param2ch(parameters['AIN'])

        if 'FREQ' in parameters :
            self.__FREQ = True

    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        # Add all the Digital Outputs, Inputs, Analog Input
        for i in self.__DIN_CH :
            data_dict[f'ADC.{self.address}.DIN{i}'] = DAQC2.getDINbit(self.address,i)

        for i in self.__DOUT_CH :
            DAQC2.clrDOUTbit(0,i)
            data_dict[f'ADC.{self.address}.set_DOUT{i}'] = 0

        for i in self.__AIN_CH :
            data_dict[f'ADC.{self.address}.AIN{i}'] = DAQC2.getADC(self.address,i)

        if self.__FREQ :
            data_dict[f'ADC.{self.address}.FREQ'] = DAQC2.getFREQ(self.address)


    def Read(self, data_dict) :
        """
        Read out all analog & digital inputs and store values
        in data dictionary 
        """
        for i in self.__DIN_CH :
            data_dict[f'ADC.{self.address}.DIN{i}'] = self.__Read_DIN(i)
        
        for i in self.__AIN_CH :
            data_dict[f'ADC.{self.address}.AIN{i}'] = self.__Read_AIN(i)

        if f'ADC.{self.address}.FREQ' in data_dict.keys() :
            data_dict[f'ADC.{self.address}.FREQ'] = self.__Read_FREQ() 

    def Set(self, data_dict) :
        """
        Set the digital output values
        """
        for i in self.__DOUT_CH :
            self.__DOUT(i, data_dict[f'ADC.{self.address}.set_DOUT{i}'] )

    def Shutdown(self) :
        """
        Called to release resources
        """
        logging.info(f'Shutting down {self.name} (DAQC2Plate {self.address})')
        DAQC2.CLOSE()

        # - CALL RELEASE RESOURCES FUNCTIONS IF REQUIRED


    def __Read_DIN(self,ch) :
        """
        Function to read of digital input 
        """
        return DAQC2.getDINbit(self.address,ch)

    def __Read_AIN(self,ch) :
        """
        Function to read Analog input 
        """
        return DAQC2.getADC(self.address,ch)

    def __DOUT(self,ch,val) :
        """
        Function to set digital output
        """
        logging.debug(f'Setting digital output {ch} to {val}')
        if val == 0 :
            DAQC2.clrDOUTbit(self.address,ch)
        if val == 1 : 
            DAQC2.setDOUTbit(self.address,ch)

    def __Read_FREQ(self) :
        """
        Function to return frequency input
        """
        return DAQC2.getFREQ(self.address)

 