from .. import Hardware
import serial
import time
from . import Caen_Lib2

import logging
logger = logging.getLogger(__name__)


class Caen(Hardware.Hardware):
    def __init__(self,name):

        super().__init__(name)
        self.__location = "/dev/HV_Caen"
        self.__outtime=2
        self.__baudrate=9600
        self.__state=['OFF','OFF','OFF','OFF']

    def initialise(self,parameters) :
        """
        Initialise hardware
        """
        self.__location = parameters['Location']
        self.__baudrate=parameters['Buad']
        self.__HV=Caen_Lib2.Caen(self.__location, self.__baudrate, self.__outtime)


    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        for i in range (0, 4):
            data_dict[f'HV.set_voltage_{i}'] = 0.0
            data_dict[f'HV.set_current_{i}'] = None
            data_dict[f'HV.set_state_{i}'] = 'OFF'
            data_dict[f'HV.state_{i}'] = self.__state[i]
            data_dict[f'HV.ovp_{i}'] = 500
            data_dict[f'HV.current_{i}'] = 0.0
            data_dict[f'HV.voltage_{i}'] = 0.0

    def Read(self, data_dict) :
        """
        Read out all values and update data_dict
        """
        print("read HV")
        for i in range (0, 4):
            data_dict[f'HV.current_{i}'] = self.__HV.GetCurrent(i)
            data_dict[f'HV.voltage_{i}'] = self.__HV.GetVoltage(i)
            #data_dict[f'HV.ovp_{i}'] = 500
            data_dict[f'HV.state_{i}']=self.__state[i]

    def Set(self, data_dict) :
        """
        Read data_dict for new chiller settings & apply them
        """
        for i in range(0, 4):
            self.__HV.SetVoltage(float(data_dict[f'HV.set_voltage_{i}']),i)
            if data_dict[f'HV.set_state_{i}']=='ON':# and self.__state[i]=="OFF":
                self.__HV.TurnOn(i)
                self.__state[i]='ON'
            elif data_dict[f'HV.set_state_{i}']=='OFF':# and self.__state[i]=="ON":
                self.__HV.TurnOff(i)
                self.__state[i]='OFF'
        

    def Shutdown(self) :
        """
        Called to release resources
    
        """
        for i in range(0,4):
                self.__HV.TurnOff(i)
                self.__state[i]='OFF'
        logging.info(f'Shutting down {self.name}')
        # CALL ANY METHOD TO RELEASE RESOURCES IF NEEDED

    def set_Voltage(self,value,channel) :
        pass

    def get_Voltage(self,channel) :
        pass

    def set_Current(self,value,channel) :
        pass

    def GetCurrent(self,channel) :
        pass