import serial
import time

class Caen_HV:
    
    def __init__(self,location="/dev/HV_Caen",baudrate=9600,outtime=2):
        try:      
            self.device=serial.Serial(location, baudrate, timeout=outtime)
          #  self.device = serial.Serial()
          #  self.device.baudrate = 9600
          #  self.device.port = "COM{}".format(9)
          #  self.device.timeout = 5
          #  self.device.open() 
          #  self.device.bytesize=8
        except serial.SerialException:
            print("can't open the Caen, is it connected on the port you entered?")
    
    def __del__(self):
        try:
            self.device.close()
        except AttributeError:
            pass

    def TurnOn(self,channel):
        query="{}{}{}".format('$CMD:SET,CH:',int(channel),',PAR:ON\r\n')
        try:
            self.device.write(query.encode('utf-8'))
            return True
        except serial.SerialTimeoutException:
            print("channel %d turn On failed" %channel)
            return False

    def TurnOn(self,channel):
        query="{}{}{}".format('$CMD:SET,CH:',int(channel),',PAR:ON\r\n')
        try:
            self.device.write(query.encode('utf-8'))
            return True
        except serial.SerialTimeoutException:
            print("channel %d turn On failed" %channel)
            return False

    def TurnOff(self, channel):
        query="{}{}{}".format('$CMD:SET,CH:',int(channel),',PAR:OFF\r\n')
        try:
            self.device.write(query.encode())
            return True
        except serial.SerialTimeoutException:
            print("channel %d turn Off failed" %channel)
            return False 
    
    def SetVoltage(self,voltage,channel):
        query="{}{}{}{}\r\n".format('$CMD:SET,CH:',channel,',PAR:VSET,VAL:',voltage)
        try:
            self.device.write(query.encode())
            return True
        except serial.SerialTimeoutException:
            print("caen channel %d voltage set failed" %channel)
    
    
    def SetCurrent(self,current,channel):
        query="{}{}{}{}\r\n".format('$CMD:SET,CH:',channel,',PAR:ISET,VAL:',current)        
        try:
            self.device.write(query.encode())
            return True
        except serial.SerialTimeoutException:
            print("Caen query from Set current failed")
            return False
           
    def GetVoltage(self,channel):
        try:
            query="{}{}{}".format('$CMD:MON,CH:',channel,',PAR:VMON\r\n')
            self.device.write(query.encode('ascii'))
            data= self.device.readline().decode()
            #print(data)
            try:
                return float(data[12:19])
            except:
                return None
        except serial.SerialTimeoutException:
            print("Device Read failed")
            return None
        
    def GetCurrent(self,channel):
        try:
            query="{}{}{}".format('$CMD:MON,CH:',channel,',PAR:IMON\r\n')
            self.device.write(query.encode('ascii'))
            data= self.device.readline().decode()
            #print(data)
            try:
                return float(data[12:19])
            except:
                return None 
        except serial.SerialTimeoutException:
            print("Device Read failed")
            return None


if __name__=="__main__":
    HV=Caen_HV()
    time.sleep(2)
    #HV.GetType()
    #time.sleep(2)
    HV.TurnOn(1)
    HV.TurnOn(2)
    HV.TurnOn(3)
    HV.TurnOn(4)
    time.sleep(3)
    HV.SetVoltage(1,1)
    time.sleep(3)
    print(HV.GetVoltage(2))
    print(HV.GetVoltage(1))
    print(HV.GetCurrent(1))
    print(HV.GetCurrent(2))
    time.sleep(3)
    HV.TurnOff(1)
    HV.TurnOff(0)
    HV.TurnOff(2)
    HV.TurnOff(3)
