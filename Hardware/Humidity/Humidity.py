# Wrapper for a generic analog Humidity sensor

from .. import Hardware 
import logging

class Humidity(Hardware.Hardware) :

    def __init__(self,name) -> None:
        """
        Constructor for the IDT FS2012 class
        """
        super().__init__(name)
        self.input = None
        self.id = None


    def initialise(self,parameters) :
        """
        Initialise hardware
        """
        self.input = parameters['Input']
        self.id = parameters['ID']
    
    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict[f'RH.{self.id}'] = None

    def Read(self, data_dict) :
        """
        Read the input voltage and convert to SLPM
        """
        try : 
            data_dict[f'RH.{self.id}'] = self.v2h(data_dict[self.input])
        except :
            logging.debug(f'failed to get ADC value {self.input}')

    def Set(self, data_dict) :
        """
        Nothing to set
        """
        pass

    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass 

    def v2h(self,v) :
        """
        Voltage to humidity calibration function
        - a dummy function for now where 0 to 5V maps to 0 to 100% RH
        """
        return (v/5.0)
