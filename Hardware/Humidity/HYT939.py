# Wrapper for IST HYT939 humidity sensor

from .. import Hardware 
import logging

import hyt939

# Set up logger 
logger = logging.getLogger(__name__)

class HYT939(Hardware.Hardware) :

    def __init__(self,name) -> None:
        """
        Constructor for the HYT939 class
        """
        super().__init__(name)
        self.id = None    
        self.hyt = None


    def initialise(self,parameters) :
        """
        Initialise hardware
        """
        self.id = parameters['ID']
        self.hyt = hyt939.HYT939( int(parameters['BUS']),
                                  int(parameters['ADDR'],16) )



    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """        
        # Do an initial read to set humidity and temperature values
        self.hyt.read()        
        data_dict[f'RH.{self.id}'] = self.hyt.humidity
        data_dict[f'RH.{self.id}.temperature'] = self.hyt.temperature


    def Read(self, data_dict) :
        """
        Read the HYT939 humidity sensor
        """
        try : 
            self.hyt.read()
            data_dict[f'RH.{self.id}'] = self.hyt.humidity
            data_dict[f'RH.{self.id}.temperature'] = self.hyt.temperature
        except :
            logger.error(f'failed to read RH{self.input}', exc_info=True)


    def Set(self, data_dict) :
        """
        Nothing to set
        """
        pass


    def Shutdown(self) :
        """
        Called to release resources
        """
        self.hyt.close()

