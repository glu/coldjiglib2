import time
import smbus
from .. import Hardware 
import logging

class SHT85(Hardware.Hardware) :

    def __init__(self,name) -> None:

        super().__init__(name)



    def initialise(self,parameters) :
        """
        Initialise hardware
        """
        self.__id =parameters['ID']
        self.__address=0x44
        self.__bus= smbus.SMBus(int(parameters['bus']))
        #print(self.get_humidity())
        #print(self.get_temperature())
    
    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict[f'RH.{self.__id}'] = 0.0
        data_dict[f'thermometer.sht{self.__id}'] = 0.0

    def Read(self, data_dict) :
        """
        Read the input voltage and convert to SLPM
        """
        try : 
            data_dict[f'RH.{self.__id}'] = self.get_humidity()
            data_dict[f'thermometer.sht{self.__id}'] = self.get_temperature()
            #print(self.get_humidity())
            #print(self.get_temperature())
        except :
            logging.debug(f'failed to connect to SHT is it connected at {self.__bus}')

    def Set(self, data_dict) :
        """
        Nothing to set
        """
        pass

    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass 



    def get_data(self):
        #Write the read sensor command
        self.__bus.write_byte_data(self.__address, 0x24, 0x00)
        time.sleep(0.1) #This is so the sensor has tme to preform the mesurement and write its registers before you read it

        # Read data back, 8 bytes, temperature MSB first then lsb, Then skip the checksum bit then humidity MSB the lsb.
        data0 = self.__bus.read_i2c_block_data(self.__address, 0x00, 8)

        t_val = (data0[0]<<8) + data0[1] #convert the data

        h_val = (data0[3] <<8) + data0[4]     # Convert the data
        T = ((175.72 * t_val) / 65536.0 ) - 45 #do the maths from datasheet
        H = ((100 * h_val) / 65536.0 )
        
        return {"temperature":T,"humidity":H}

    def get_temperature(self):
        out= self.get_data()
        return out["temperature"]

    def get_humidity(self):
        outt= self.get_data()
        return outt["humidity"]
    
