# Wrapper for testing ColdJigLib

from .. import Hardware
import logging

class Test(Hardware.Hardware) :

    def __init__(self,name) :
        """
        Constructor for Test
        """
        super().__init__(name)

        # Set up variables to store internal states
        # -- Chiller
        self.chiller_temp = 20.0
        self.chiller_pump = 5
                
        # -- thermometry
        self.VC1 = 20.0
        self.VC2 = 20.0
        self.VC3 = 20.0
        self.VC4 = 20.0
        self.VC5 = 20.0

        self.CB1 = 20.0
        self.CB2 = 20.0
        self.CB3 = 20.0
        self.CB4 = 20.0
        self.CB5 = 20.0

        self.AIR1 = 20.0
        self.AIR2 = 20.0

        # -- N2 Flow
        self.N2 = 0.0

        # -- Dew Point
        self.DP1 = 5.0
        self.DP2 = 5.0

        # -- Lid
        self.Lid1 = 'CLOSED'
        self.Lid2 = 'CLOSED'

        # -- RH
        self.RH1 = 40.0
        self.RH2 = 40.0

        # Peltier
        self.peltier1_V = 30.0
        self.peltier1_A = 2.5

        self.peltier2_V = 30.0
        self.peltier2_A = 2.5

        self.peltier3_V = 30.0
        self.peltier3_A = 2.5

        self.peltier4_V = 30.0
        self.peltier4_A = 2.5

        self.peltier5_V = 30.0
        self.peltier5_A = 2.5

    def initialise(self, parameters):
        """
        Initialise H/W - empty for now
        """
        pass

    def add_data_dict_keys(self, data_dict):
        """
        Add keys to data dictionary
        """
        data_dict['chiller.set_temperature'] = self.chiller_temp
        data_dict['chiller.set_pump'] = self.chiller_pump
        data_dict['chiller.temperature'] = self.chiller_temp
        data_dict['chiller.pump'] = self.chiller_pump

        data_dict['thermometer.VC1'] = self.VC1
        data_dict['thermometer.VC2'] = self.VC2
        data_dict['thermometer.VC3'] = self.VC3
        data_dict['thermometer.VC4'] = self.VC4
        data_dict['thermometer.VC5'] = self.VC5

        data_dict['thermometer.CB1'] = self.CB1
        data_dict['thermometer.CB2'] = self.CB2
        data_dict['thermometer.CB3'] = self.CB3
        data_dict['thermometer.CB4'] = self.CB4
        data_dict['thermometer.CB5'] = self.CB5

        data_dict['thermometer.AIR1'] = self.AIR1
        data_dict['thermometer.AIR2'] = self.AIR2

        data_dict['DP.1'] = self.DP1
        data_dict['DP.2'] = self.DP2

        data_dict['gas_flow'] = self.N2

        data_dict['RH.1'] = self.RH1
        data_dict['RH.2'] = self.RH2

        data_dict['Lid.1'] = self.Lid1
        data_dict['Lid.2'] = self.Lid2

        data_dict['peltier.volt.1'] = self.peltier1_V
        data_dict['peltier.set_volt.1'] = self.peltier1_V
        data_dict['peltier.current.1'] = self.peltier1_A
        data_dict['peltier.set_current.1'] = self.peltier1_A

        data_dict['peltier.volt.2'] = self.peltier2_V
        data_dict['peltier.set_volt.2'] = self.peltier2_V
        data_dict['peltier.current.2'] = self.peltier2_A
        data_dict['peltier.set_current.2'] = self.peltier2_A

        data_dict['peltier.volt.3'] = self.peltier3_V
        data_dict['peltier.set_volt.3'] = self.peltier3_V
        data_dict['peltier.current.3'] = self.peltier3_A
        data_dict['peltier.set_current.3'] = self.peltier3_A

        data_dict['peltier.volt.4'] = self.peltier4_V
        data_dict['peltier.set_volt.4'] = self.peltier4_V
        data_dict['peltier.current.4'] = self.peltier4_A
        data_dict['peltier.set_current.4'] = self.peltier4_A

        data_dict['peltier.volt.5'] = self.peltier5_V
        data_dict['peltier.set_volt.5'] = self.peltier5_V
        data_dict['peltier.current.5'] = self.peltier5_A
        data_dict['peltier.set_current.5'] = self.peltier5_A


    def Read(self, data_dict):
        """
        Read out the data -- write to data_dict
        """
        data_dict['chiller.temperature'] = self.chiller_temp
        data_dict['chiller.pump'] = self.chiller_pump

        data_dict['thermometer.VC1'] = self.VC1
        data_dict['thermometer.VC2'] = self.VC2
        data_dict['thermometer.VC3'] = self.VC3
        data_dict['thermometer.VC4'] = self.VC4
        data_dict['thermometer.VC5'] = self.VC5

        data_dict['thermometer.CB1'] = self.CB1
        data_dict['thermometer.CB2'] = self.CB2
        data_dict['thermometer.CB3'] = self.CB3
        data_dict['thermometer.CB4'] = self.CB4
        data_dict['thermometer.CB5'] = self.CB5

        data_dict['thermometer.AIR1'] = self.AIR1
        data_dict['thermometer.AIR2'] = self.AIR2

        data_dict['DP.1'] = self.DP1
        data_dict['DP.2'] = self.DP2

        data_dict['gas_flow'] = self.N2

        data_dict['RH.1'] = self.RH1
        data_dict['RH.2'] = self.RH2

        data_dict['Lid.1'] = self.Lid1
        data_dict['Lid.2'] = self.Lid2

        data_dict['peltier.volt.1'] = self.peltier1_V
        data_dict['peltier.current.1'] = self.peltier1_A
        data_dict['peltier.volt.2'] = self.peltier2_V
        data_dict['peltier.current.2'] = self.peltier2_A
        data_dict['peltier.volt.3'] = self.peltier3_V
        data_dict['peltier.current.3'] = self.peltier3_A
        data_dict['peltier.volt.4'] = self.peltier4_V
        data_dict['peltier.current.4'] = self.peltier4_A
        data_dict['peltier.volt.5'] = self.peltier5_V
        data_dict['peltier.current.5'] = self.peltier5_A


    def Set(self, data_dict):
        """
        Set values -- read from data_dict
        """
        
        self.chiller_temp = data_dict['chiller.set_temperature']
        self.chiller_pump = data_dict['chiller.set_pump']

        self.peltier1_V = data_dict['peltier.set_volt.1'] 
        self.peltier1_A = data_dict['peltier.set_current.1']
        self.peltier2_V = data_dict['peltier.set_volt.2'] 
        self.peltier2_A = data_dict['peltier.set_current.2']
        self.peltier3_V = data_dict['peltier.set_volt.3'] 
        self.peltier3_A = data_dict['peltier.set_current.3']
        self.peltier4_V = data_dict['peltier.set_volt.4'] 
        self.peltier4_A = data_dict['peltier.set_current.4']
        self.peltier5_V = data_dict['peltier.set_volt.5'] 
        self.peltier5_A = data_dict['peltier.set_current.5']


    def Shutdown(self):
        """
        Shutdown H/W - empty for now
        """
        pass

