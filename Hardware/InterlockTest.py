# Dummy interlock for testing 

class InterlockTest:
    def __init__(self,location="/dev/interlock",baudrate=115200):
        self.__location = location
        self.__baudrate = baudrate
        
    def get_data(self):
        return "-35,-30,-25,-20,-15,-10,-5,0,5,10,15,20"

