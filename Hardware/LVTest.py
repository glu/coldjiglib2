# Dummy 4-channel LV module for code testing
# Simulate LV connected to 1-ohm resistor

class LVTest :

    def __init__(self,location,name="TEST_LV") :
        self.__location = location
        self.__name = name
        self.__state = "OFF"
        self.__voltage = [0.0,0.0,0.0,0.0]
        self.__current = [0.0,0.0,0.0,0.0]
        self.__ovp = [10.0,10.0,10.0,10.0]

        
    def TurnOn(self) :
        self.__state = "ON"

    def TurnOff(self) :
        self.__state = "OFF"

    def SetVoltage(self,value,channel) :
        self.__voltage[channel] = value
        self.__current[channel] = value

    def GetVoltage(self,channel) :
        return self.__voltage[channel]

    def SetCurrent(self,value,channel) :
        self.__current[channel] = value
        self.__voltage[channel] = value

    def GetCurrent(self,channel) :
        return self.__current[channel]

    def GetActualCurrent(self,channel) :
        return self.GetCurrent(channel) 
    
    def SetVoltageProtection(self,value,channel) :
        self.__ovp[channel] = value

        
