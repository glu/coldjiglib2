# Water/Ice vapour pressure calculations, using the Hyland/Wexler
# numerical approximations
#
import math
import numpy as np

import logging
logger = logging.getLogger(__name__)

# centigrade to kelvin conversion
c2k = lambda x : x + 273.15

# kelvin to centigrade conversion
k2c = lambda x : x - 273.15



#  Wexler, A. (1976). "Vapor pressure formulation for water in range 0
# to 100C. A revision". Journal of Research of the National Bureau of
# Standards Section A. 80A (5-6): 775-785. doi:10.6028/jres.080a.071
#
# Wexler, A. (1977). "Vapor pressure formulation for ice". Journal of
# Research of the National Bureau of Standards Section A. 81A (1):
# 5-20. doi:10.6028/jres.081a.003.
#
# Formulas taken from Dortmund databank
# http://ddbonline.ddbst.com/OnlinePropertyEstimation/HylandWexlerPsatOfVapourOverLiquidWaterCGI.exe
#



# Liquid saturated water vapour pressure 
# Hyland R.W., Wexler A., "Formulations for the thermodynamic
# properties of the saturated phases of H2O from 173.15 K to 473.15
# K",ASHRAE Trans., 89(2A), 500-519, 1983; Equation 17 
#
# Equation
# ln(P_water) = \Sum^3_{i=-1} (g_i T^i) + g_4 ln(T)
#
# T in Kelvin
# P is Pa
#
# Hyland+Wexler Vapor Over Liquid Water.png
# Parameters
#
# g-1	=	-5.800221e+03
# g0	=	1.391499e+00
# g1	=	-4.864024e-02
# g2	=	4.176477e-05
# g3	=	-1.445209e-08
# g4	=	6.545967e+00
#
# Parameter validity:
# 273.15 K <= T <= 473.15K
#
# Psat is obtained in [Pa]



def water_vp(T) :
    
    if (T < 273.15) or (T > 473.15) :
        logger.error(f'water_vp: T={T}k --> out of range')
        return -1 

    gm1	=	-5.800221e+03
    g0	=	1.391499e+00
    g1	=	-4.864024e-02
    g2	=	4.176477e-05
    g3	=	-1.445209e-08
    g4	=	6.545967e+00
 
    ln_ws =  (gm1/T) + g0 + (g1*T) + (g2*T*T) + (g3*T*T*T) + (g4*math.log(T))

    ws = math.exp(ln_ws)

    return ws

     

# Saturation Pressure of Water Vapor Over Ice

# Paper

# Hyland R.W., Wexler A., "Formulations for the thermodynamic properties of the saturated phases of H2O from 173.15 K to 473.15 K",ASHRAE Trans., 89(2A), 500-519, 1983; Equation 18 

# Equation
# ln (P_water) = \Sum_{i=0}^5 (m_i T^{i-1}) + m_6 ln(T)

# Hyland+Wexler Vapor Over Ice.png
# Parameters

# m0	=	-5.674536e+03
# m1	=	6.392525e+00
# m2	=	-9.677843e-03
# m3	=	6.221570e-07
# m4	=	2.074783e-09
# m5	=	-9.484024e-13
# m6	=	4.163502e+00

# Parameter validity:
# 173.16 K <= T <= 273.16K
#
# Psat is obtained in [Pa]

def ice_vp(T) :
    
    if (T < 173.15) or (T > 273.16) :
        logger.error(f'ice_vp: T={T}k --> out of range')
        return -1

    m0	=	-5.674536e+03
    m1	=	6.392525e+00
    m2	=	-9.677843e-03
    m3	=	6.221570e-07
    m4	=	2.074783e-09
    m5	=	-9.484024e-13
    m6	=	4.163502e+00
    
    ln_ws =  (m0/T) + m1 + (m2*T) + (m3*T*T) + (m4*T*T*T) \
      + (m5*T*T*T*T) + (m6*math.log(T))

    ws = math.exp(ln_ws)

    return ws

def max_RH(temp,dew_point) :

    vp_dp = water_vp(dew_point) if dew_point > 273.15 else ice_vp(dew_point)
    vp_temp = water_vp(temp) if temp > 273.15 else ice_vp(temp)

    rh = vp_dp / vp_temp

    return rh

# Create a LUT to map vapour pressure to air temperature
air_temp = np.arange(-99.9,200.1,0.1)
air_vp = np.array([ice_vp(c2k(T)) if T < 0.0  else water_vp(c2k(T)) for T in air_temp])

def DP(temp,RH) :

    # Calculate the water vapour pressure at temperature T
    vp = water_vp(temp) if temp >273.15 else ice_vp(temp)

    # Detetmine saturation vapour pressure at Dew Point
    vp_dp = vp * RH

    # Use pre-calculated LUT to find nearest air temperature for 
    # given vapur pressure
    idx = (np.abs(air_vp - vp_dp)).argmin()
    dew_point = air_temp[idx]
    
    return dew_point
    
def temp(DP,RH) :

    # Calculate vapour pressure at Dew Point
    vp_dp = water_vp(DP) if DP > 273.15 else ice_vp(DP)

    # Calculate the air vapour pressure
    vp = vp_dp / RH

    # Use pre-calculated LUT to find nearest air temperature for 
    # given vapur pressure
    idx = (np.abs(air_vp - vp)).argmin()
    gas_temp = air_temp[idx]

    return gas_temp
