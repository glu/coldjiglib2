# Wrapper for the Dew Point calculation
# using RH and Air Temperature

from .. import Hardware 
from . import DewPoint_HW

import logging
logger = logging.getLogger(__name__)

class DewPoint(Hardware.Hardware) :

    def __init__(self,name) -> None:
        """
        Constructor for DewPoint calculator
        """
        super().__init__(name)
        self.input_air_temp = None
        self.input_rh = None
        self.id = None

    def initialise(self,parameters) :
        """
        Initialise Dew Point calculator 
        """
        self.input_air_temp = parameters['Input_Air_Temp']
        self.input_rh = parameters['Input_RH']
        self.id = parameters['ID']

    def add_data_dict_keys(self,data_dict) :
        """
        Add data keys
        """
        data_dict[f'DP.{self.id}'] = None        
        self.Read(data_dict)  # Initialise Dew Point starting value

    def Read(self, data_dict) :
        """
        Read the Air-Temp and RH values to convert to Dew Point
        """
        try : 
            air_temp = data_dict[self.input_air_temp]
            rh = data_dict[self.input_rh]
            data_dict[f'DP.{self.id}'] = self.DP_calc(air_temp,rh)
        except :
            logger.debug(f'Failed to get Air Temp or RH')


    def Set(self, data_dict) :
        """
        No values to set. Empty function
        """
        pass


    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass


    def DP_calc(self,air_temp,rh) :
        """
        Function to calculate Dew Point
        """
        # Convert air_temp to Kelvin
        air_temp_kelvin = DewPoint_HW.c2k(air_temp)
        logger.debug(f'Air Temperature: {air_temp} C / {air_temp_kelvin} k, RH: {rh}%')

        dp = DewPoint_HW.DP(air_temp_kelvin,(rh/100.0))
        logger.debug(f'Dew Point={dp} C')

        return dp