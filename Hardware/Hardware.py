from abc import ABC, abstractmethod
import warnings

import logging
logger = logging.getLogger(__name__)

class Hardware(ABC) :

    def __init__(self,name="UNKNOWN_HW") -> None:
        self.name = name
        super().__init__()

        self.inputs = []
        self.outputs = []
        self.read_keys = []
        self.set_keys = []

    @abstractmethod
    def initialise(self,parameters) :
        """
        Abstract method to initialise hardware
        Hardware settings are defined in the parameters 
        dictionary
        """
        pass

    @abstractmethod
    def add_data_dict_keys(self,data_dict) :
        """
        Abstract method to add keys to the data dictionary
        """
        pass 

    @abstractmethod
    def Read(self, data_dict) :
        """
        Abstract method for reading out all values out of the hardware
        and written to the data_dict
        """
        pass

    @abstractmethod
    def Set(self, data_dict) :
        """
        Abstract method that is the counterpart to Read - it sets 
        hardware values through values stored in the data_dict
        """
        pass

    @abstractmethod
    def Shutdown(self) :
        """
        Called to release resources
        """
        pass

    def initialise_hw(self,parameters) :
        self.__fill_dependencies(parameters)
        self.initialise(parameters)

    def __fill_dependencies(self,parameters) :
        """
        Function fills in input/output dependencies
        Assumes input dependencies have "INPUT" prefix
        and output dependencies have "OUTPUT" prefix
        """
        for key in parameters :
            #  Fill in inputs list
            if key.upper().startswith('INPUT') :
                self.inputs.append(parameters[key]) 

            #  Fill in outputs list
            if key.upper().startswith('OUTPUT') :
                self.outputs.append(parameters[key])

    def add_data_dict_keys_hw(self,data_dict) :
        logger.debug("Add_Data_DICT_KEYS_HW")

        # Get current keys
        keys_initial_size = len(data_dict)
        logger.debug(f'INITIAL KEYS SIZE {keys_initial_size}')

        # Add new keys
        self.add_data_dict_keys(data_dict)
    
        # Find the new keys
        keys_final = data_dict.keys()
        logger.debug(f'FINAL KEYS: {keys_final}')
        new_keys = list(keys_final)[keys_initial_size:]
        logger.debug(f'NEW KEYS {new_keys}')

        # split keys by Read and Set 
        for key_i in new_keys : 
            if 'set_' in key_i :
                self.set_keys.append(key_i)
            else :
                self.read_keys.append(key_i)

