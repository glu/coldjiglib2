from abc import ABC, abstractmethod

class Chiller(ABC) :

    @abstractmethod
    def set_temperature(self,temperature) :
        pass

    @abstractmethod
    def temperature(self) :
        pass

    @abstractmethod
    def set_flow(self,flow) :
        pass

    @abstractmethod
    def flow(self) :
        pass

    @abstractmethod
    def alarm(self) :
        pass 
