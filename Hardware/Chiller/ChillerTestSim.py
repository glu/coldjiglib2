# Dummy chiller module for code testing
# Simulate chiller that changes temperature 1 deg / sec

import time

from enum import Enum 

class Mode(Enum) :
    HEAT = 1.0
    COOL = -1.0

class ChillerTest :

    def __init__(self,location,name="TestChiller") :
        self.__location = location
        self.__name = name
        self.__temperature = 20.0
        self.__set_temperature = 20.0
        self.__mode = Mode.HEAT
        self.__state = "ON"
        self.__start = 0.0

    def TurnOn(self) :
        self.__state = "ON"

    def TurnOff(self) :
        self.__state = "OFF"

    def SetTemperature(self,value) :

        # Return immediately if it's same set point
        if value == self.__set_temperature : 
            return 

        self.__set_temperature = value

        if self.__set_temperature > self.__temperature :
            self.__mode = Mode.HEAT
        else :
            self.__mode = Mode.COOL
    
        self.__start = time.monotonic()

    def GetTemperature(self) :

        dTime = time.monotonic() - self.__start

        dTemp = dTime * self.__mode.value

        temp = self.__temperature + dTemp

        if (self.__mode is Mode.HEAT) :
            if (temp < self.__set_temperature) :
                self.__temperature = temp
            else :
                self.__temperature = self.__set_temperature

        if (self.__mode is Mode.COOL) :
            if (temp > self.__set_temperature) :
                self.__temperature = temp
            else :
                self.__temperature = self.__set_temperature

        self.__start = time.monotonic()
        return self.__temperature 
