import serial
from serial import *
import time


class SPS_Chiller:
    def __init__(self,location="/dev/ttyUSB0",timeout=1):
        self.chiller=serial.Serial(location,9600,SEVENBITS,parity=PARITY_NONE,stopbits=STOPBITS_ONE,timeout=1)
        
    def SetRampRate(self,rate):
        query="RR="+str(rate)+"\n\r"
        self.chiller.write(query.encode())
        #print(self.chiller.readline())
        
    def SetTemperature(self,temp):
        query="SP="+str(temp)+"\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
        
    def TurnOn(self):
        query="START\n\r"
        self.chiller.write(query.encode())
        #print(self.chiller.readline())
    
    def TurnOff(self):
        query="STOP\n\r"
        self.chiller.write(query.encode())
        #print(self.chiller.readline())

    def Gettemperature(self):
        query="PTLOC?\n\r"
        self.chiller.write(query.encode())
        data=self.chiller.readline()
        #print(data)
        data=data.decode()
        #print(data)
        pass
        #try:
        #    return float(data[data.index("=")+1:data.index("!")])
        #except ValueError:
        #    pass



    def GetTemperature(self):
        self.Gettemperature()
        #time.sleep(0.5)
        query="PTLOC?\n\r"
        self.chiller.write(query.encode())
        data=self.chiller.readline()
        #print(data)
        data=data.decode()
        #print(data)
        try:
            return float(data[data.index("=")+1:data.index("!")])
        except ValueError:
            pass

    def GetRampRate(self):
        query="RR?\n\r"
        self.chiller.write(query.encode())
        data=self.chiller.readline()
        #print(data)
        return float(data[data.index("=")+1:data.index("!")])


if __name__=="__main__":
    chiller=SPS_Chiller(location="/dev/ttyUSB2")
    chiller.TurnOff()
    while(1):
        print(chiller.GetTemperature())

    #time.sleep(20)
    #chiller.TurnOff()