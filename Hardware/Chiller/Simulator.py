from .. import Hardware 
import logging

from .ChillerTest import ChillerTest

class Simulator(Hardware.Hardware) :

    def __init__(self,name) :
        """
        Constructor of Simulator chiller object
        """
        super().__init__(name)
        self.__pump = None
        self.__chiller = None 
    

    def initialise(self,parameters) :
        """
        Initialise hardware
        """
        self.__chiller = ChillerTest(parameters['UsbPort'])
        self.__chiller.TurnOn()


    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict['chiller.set_temperature'] = 20.0
        data_dict['chiller.set_pump'] = 5
        data_dict['chiller.temperature'] = None
        data_dict['chiller.pump'] = None

        
    def Read(self, data_dict) :
        """
        Read out all values and update data_dict
        """
        data_dict['chiller.temperature'] = self.temperature()
        data_dict['chiller.pump'] = self.pump()
        

    def Set(self, data_dict) :
        """
        Read data_dict for new chiller settings & apply them
         """
        self.set_temperature(data_dict['chiller.set_temperature'])
        self.set_pump(data_dict['chiller.set_pump'])


    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass 
    

    def set_temperature(self,temperature) :
        logging.debug(f"ChillerSim - set tempetature {temperature} ")
        self.__chiller.SetTemperature(temperature)

    def temperature(self) :
        temp_now = self.__chiller.GetTemperature()
        logging.debug(f"ChillerSim - temperature is {temp_now}")
        return temp_now
   
    def set_pump(self,pump) :
        logging.debug(f'ChillerSim - set flow to {self.__pump}')
        self.__pump = pump
   
    def pump(self) :
        logging.debug(f'ChillerSim - flow is {self.__pump}')
        return self.__pump
   
    def alarm(self) :
        logging.debug("ChillerSim -- No alarm")
        return False

