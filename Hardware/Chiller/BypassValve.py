from .. import Hardware
import logging
logger = logging.getLogger(__name__)

class BypassValve(Hardware.Hardware):

    def __init__(self, name):
        '''
        Constuctor for ByPass Valve
        '''

        super().__init__(name)

        self.mode_val ={'ON' : 1, 'OFF' : 0}
        self.Output = None

    def initialise(self, parameters):
        '''
        Initialize Hardware using values defined in parameters dictionary
        '''

        self.Output = parameters['Output']

    def add_data_dict_keys(self,data_dict) :
        '''
        Add keys to the data dictionary
        '''
        
        data_dict['bypassvalve.set_mode']  = 'OFF'
        

    def Read(self, data_dict) :
        '''
        No Values to Read - empty function
        '''
        pass

    def Set(self, data_dict):
        '''
        Set the bypassvalve mode by setting the output state
        '''
        try :
            data_dict[self.Output] = self.mode_val[data_dict['bypassvalve.set_mode']]
        except KeyError as e:
            logger.warning(f'failed to set bypassvalve mode {e}')

    def Shutdown(self) :
        '''
        Called to release resources
        Nothing to do - empty function
        '''
        pass
