import serial

class JulaboDyneoDD1000F:

  def __init__(self, port, timeout = 2):
    '''
    Constructor of the JulaboDyneoDD1000F chiller object
    '''
    try:
      # Start serial connection
      self._ser = serial.Serial(port, timeout = timeout, write_timeout = timeout)
    except:
      logging.error('Failed to establish serial connection.', exc_info = True)

    # get temperature limits
    self._temp_min = self.get_lower_temp_limit()
    self._temp_max = self.get_upper_temp_limit()
  
  def start_chiller(self):
    '''
    Start chiller
    '''
    
    command = 'out_mode_051'
    self._send_command(command)
  
  def stop_chiller(self):
    '''
    Stop chiller
      Stops and closes serial connection
    '''
    
    command = 'out_mode_050'
    self._send_command(command)
    self._ser.close()

  def _send_command(self, command):
    '''
    Sends command over serial connection
      Adds \r\n termination characters
    '''
    
    command += '\r\n'
    self._ser.write(command.encode())
  
  def _receive(self):
    '''
    Receive over serial connection and decode
    '''
    
    ret = self._ser.read_until()
    
    return ret.decode()
  
  def get_lower_temp_limit(self):
    '''
    Get chiller's minimum temperature limit (can be set by user)
    and return float
    '''
    
    command = 'in_sp_04'
    self._send_command(command)
    t = float(self._receive())
    
    return t
  
  def get_upper_temp_limit(self):
    '''
    Get chiller's maximum temperature limit (can be set by user)
    and return float
    '''
    
    command = 'in_sp_03'
    self._send_command(command)
    t = float(self._receive())
    
    return t
  
  def get_target_temperature(self):
    '''
    Get target temperature of chiller
    '''

    command = 'in_sp_00'
    self._send_command(command)
    t = float(self._receive())
    
    return t
  
  def set_target_temperature(self, temp):
    '''
    Set target temperature of chiller
      Check first that it isn't outside the chiller's range.
      If outside range, temperature is not set.
    '''

    if ((temp < self._temp_min) or (temp > self._temp_max)):
      raise ValueError(
          f'Given Temperature {temp} is out of the allowed range of {self._temp_min}, {self._temp_max}'
          )
    else:
      command = f'out_sp_00{temp}'
      self._send_command(command)
  
  def get_external_temperature(self):
    '''
    Get external temperture (from PT100 sensor) from chiller and return float
    '''
    command = 'in_pv_02'
    self._send_command(command)
    t = float(self._receive())
    
    return t

  def get_internal_temperature(self):
    '''
    Get internal temperature from chiller and return float
    '''

    self._send_command('in_pv_00')
    t = float(self._receive())
    return t
  
  # This function is currently not utilized. 
  def __del__(self):
    self.stop_chiller()
  
