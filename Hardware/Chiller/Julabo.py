from .. import Hardware
import logging

# from .YourJulabo import YourJulabo 
# need to change if you use a different Julabo model,
# which should have all the same functions for this wrapper to work
from .JulaboDyneoDD1000F import JulaboDyneoDD1000F as chiller

class Julabo(Hardware.Hardware):
  
  def __init__(self, name):
    '''
    Constructor of Julabo chiller object
    '''
    super().__init__(name)
    
    # Set variables to None
    self._usb_port    = None
    self._julabo      = None

  def initialise(self, parameters):
    '''
    Initialize Hardware
    '''
    
    try:
      # USB port
      self._usb_port = parameters['UsbPort']
      # Initialize your model of chiller
      self._julabo = chiller(self._usb_port)
    except:
      logging.error('Failed to initialize chiller', exc_info = True)

    # Start Chiller
    self._julabo.start_chiller()
    
    # Logging
    logging.info(f'Julabo chiller initialized. Port: {self._usb_port}')
    
  def add_data_dict_keys(self, data_dict):
    '''
    Add keys to the data dictionary
    '''

    logging.info('Setting data dictionary keys for chiller.')

    # Initialize values 
    # Choose set_temperature to be close to room temperature
    # Use actual temperatures to initialize (makes for a nicer plot)
    data_dict['chiller.set_temperature'] = 20.0 
    try:
      data_dict['chiller.temperature_internal'] = self._julabo.get_internal_temperature()
      data_dict['chiller.temperature_external'] = self._julabo.get_external_temperature()
    except:
      logging.error('Failed to read chiller when setting dictionary keys', exc_info = True)

  def Read(self, data_dict):
    '''
    Read temperatures from chiller
    '''

    logging.debug(f'Reading chiller temperatures and updating data_dict')

    try:
      data_dict['chiller.temperature_internal'] = self._julabo.get_internal_temperature()
      data_dict['chiller.temperature_external'] = self._julabo.get_external_temperature()
    except:
      logging.error('Failed to read chiller', exc_info = True)

  def Set(self, data_dict):
    '''
    Read data_dict for new chiller settings and apply them
    '''
    logging.debug(f'Chiller temperature set to {data_dict["chiller.set_temperature"]}.') 
    
    try:
      # Set chiller temperature
      self._julabo.set_target_temperature(data_dict['chiller.set_temperature'])
    except:
      logging.error('Failed to set chiller temperature.', exc_info = True)

  def Shutdown(self):
    '''
    Close connection
      Stops the chiller and closes the serial connection
    '''
    logging.info('Shutting down chiller')
    
    try:
      # Turn off and shut down connection
      self._julabo.stop_chiller()
    except:
      logging.error('Failed to shut down chiller.', exc_info = True)
