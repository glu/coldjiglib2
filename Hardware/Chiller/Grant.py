from .. import Hardware 
import logging

#import TXF200
import grant.TXF200 as TXF200

class Grant(Hardware.Hardware) :

    def __init__(self,name) :
        """
        Constructor of Grant chiller object
        """
        super().__init__(name)
        self.__usb_port = None  # Chiller USB Port   
        self.__temperature = None
        self.__pump = None

    def initialise(self,parameters) :
        """
        Initialise hardware
        """
        self.__usb_port = parameters['UsbPort']

        # Open connection
        self.__grant = TXF200.TXF200(self.__usb_port)
        self.__grant.open()

        # Check ID can be read
        id = self.__grant.ID
        logging.info(f'Grant Chiller ID: {id}')


    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        temp_data = self.__grant.get_temp_info()
        data_dict['chiller.set_temperature'] = temp_data['set_temp']
        data_dict['chiller.set_pump'] = self.pump()
        data_dict['chiller.temperature'] = temp_data['coolant_temp']
        data_dict['chiller.pump'] = data_dict['chiller.set_pump']

        
    def Read(self, data_dict) :
        """
        Read out all values and update data_dict
        """
        data_dict['chiller.temperature'] = self.temperature()
        data_dict['chiller.pump'] = self.pump()



    def Set(self, data_dict) :
        """
        Read data_dict for new chiller settings & apply them
         """
        self.set_temperature(data_dict['chiller.set_temperature'])
        self.set_pump(data_dict['chiller.set_pump'])


    def Shutdown(self) :
        """
        Called to release resources
        """
        logging.info(f'Shutting down {self.name}')
        self.__grant.close()


    def set_temperature(self,temperature) :
        logging.debug(f"GRANT - set tempetature {temperature} ")
        self.__temperature = temperature
        self.__grant.set_isothermal(temperature)

    def temperature(self) :
        temp_data = self.__grant.get_temp_info()
        self.__temperature = temp_data['coolant_temp']
        logging.debug(f"GRANT - temperature is {self.__temperature}")
        return self.__temperature
   
    def set_pump(self,pump) :
        logging.debug(f'GRANT - set flow to {self.__pump}')
        self.__pump = pump
        alarm = self.__grant.get_alarm()
        self.__grant.set_alarm(alarm['High'],
                               alarm['Low'],
                               alarm['Hold Off'],
                               alarm['Countdown'],
                               self.__pump)

   
    def pump(self) :
        status,fault = self.__grant.get_status()
        self.__pump = status['Pump Rate']
        logging.debug(f'GRANT - flow is {self.__pump}')
        return self.__pump
   
    def alarm(self) :
        status,fault = self.__grant.get_status()

        alarm_low  = status['Low Alarm']
        alarm_high = status['High Alarm']
        
        # Check if any faults triggered
        fault_found = False
        for flag in fault.values() :
            if flag :
                fault_found = True
                break
        
        # Check for any alarm
        if (alarm_low | alarm_high | fault_found) :
            logging.critical(f'Chiller Alarm/Fault: Low-Alarm:{alarm_low} High-Alarm:{alarm_high} Fault:{fault}')
            return True
        else :        
            logging.debug("GRANT -- No alarm")
            return False

