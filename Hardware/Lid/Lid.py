# Wrapper for the ColdJig Lid

from .. import Hardware 
import logging

# Dictionary state to translate digital state
lid_state = {0: 'CLOSED', 1: 'OPEN' }  # Strings need to be enclosed in " " for INFLUX DB upload

class Lid(Hardware.Hardware) :

    def __init__(self,name) -> None: 
        """
        Constructor for a ColdJig Lid Switch
        """
        super().__init__(name)
        self.id = None
        self.input  = None

    def initialise(self,parameters) :
        """
        Set the lid parameters
        """
        self.id = parameters['ID']
        self.input = parameters['Input']

    def add_data_dict_keys(self,data_dict) :
        """
        Add data dicts for the lid
        """
        data_dict[f'Lid.{self.id}'] = None

    def Read(self, data_dict) :
        """
        Read the digital input state and convert to lid state
        """
        try : 
            data_dict[f'Lid.{self.id}'] = lid_state[data_dict[self.input]]
        except :
            logging.debug(f"Failed to read {self.input}")


    def Set(self, data_dict) :
        """
        Noting to set - empty function for now
        """
        pass

    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass 
