# Temperature simulator to test code

from .. import Hardware 
import logging

from .PeltierModel import Chuck

class Simulator(Hardware.Hardware) :

    def __init__(self,name) -> None:
        """
        Constructor for the tempertature simulator
        """
        super().__init__(name)
        self.chuck1 = Chuck(20.0,20.0,0.0,False)
        self.chuck2 = Chuck(20.0,20.0,0.0,False)
        self.chuck3 = Chuck(20.0,20.0,0.0,False)
        self.chuck4 = Chuck(20.0,20.0,0.0,False)
        self.chuck5 = Chuck(20.0,20.0,0.0,False)


    def initialise(self,parameters) :
        """
        Initialise simulator
        Simulator settings are defined in the parameters 
        dictionary
        """
        pass

    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict[f'thermometer.VC1'] = None
        data_dict[f'thermometer.VC2'] = None
        data_dict[f'thermometer.VC3'] = None
        data_dict[f'thermometer.VC4'] = None
        data_dict[f'thermometer.VC5'] = None

        data_dict[f'thermometer.CB1'] = None
        data_dict[f'thermometer.CB2'] = None
        data_dict[f'thermometer.CB3'] = None
        data_dict[f'thermometer.CB4'] = None
        data_dict[f'thermometer.CB5'] = None


    def Read(self, data_dict) :
        """
        Return simulator temperature values and write to data_dict
        """
        # Update the Peltier current
        self.chuck1.peltier_I = data_dict['peltier.current.1']
        self.chuck2.peltier_I = data_dict['peltier.current.2']
        self.chuck3.peltier_I = data_dict['peltier.current.3']  
        self.chuck4.peltier_I = data_dict['peltier.current.4']
        self.chuck5.peltier_I = data_dict['peltier.current.5']

        # # Update the Peltier Mode
        self.chuck1.mode = data_dict['peltier.set_mode']
        self.chuck2.mode = data_dict['peltier.set_mode']
        self.chuck3.mode = data_dict['peltier.set_mode']
        self.chuck4.mode = data_dict['peltier.set_mode']
        self.chuck5.mode = data_dict['peltier.set_mode']

        # Update the chuck temperatures
        self.chuck1.update(data_dict['chiller.temperature'])
        self.chuck2.update(data_dict['chiller.temperature'])
        self.chuck3.update(data_dict['chiller.temperature'])
        self.chuck4.update(data_dict['chiller.temperature'])
        self.chuck5.update(data_dict['chiller.temperature'])

        # Write the new temperatures to the data_dict
        data_dict['thermometer.VC1'] = self.chuck1.top
        data_dict['thermometer.CB1'] = self.chuck1.bottom

        data_dict['thermometer.VC2'] = self.chuck2.top
        data_dict['thermometer.CB2'] = self.chuck2.bottom

        data_dict['thermometer.VC3'] = self.chuck3.top
        data_dict['thermometer.CB3'] = self.chuck3.bottom

        data_dict['thermometer.VC4'] = self.chuck4.top
        data_dict['thermometer.CB4'] = self.chuck4.bottom

        data_dict['thermometer.VC5'] = self.chuck5.top
        data_dict['thermometer.CB5'] = self.chuck5.bottom


    def Set(self, data_dict) :
        """
        No values to set - empty function
        """
        pass


    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass
