from .. import Hardware
from NTC_read import NTC_read


class NTC(Hardware.Hardware) :

    def __init__(self,name) :
        """
        Constructor for the TC08 class
        """
        super().__init__(name)
        self.__serial = None   # TC08 Serial Number
        self.all_channels = ()


    def initialise(self,parameters) :
        """
        Initialise hardware using settings defined the parameters 
        dictionary
        """
        self.__serial = parameters['Serial']




    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        for ch in self.all_channels :
            data_dict[f'thermometer.{ch.name}'] = None


    def Read(self, data_dict) :
        """
        Reading out all values and write to the data_dict
        """
        self.read_all_data(data_dict)

        
    def Set(self, data_dict) :
        """
        Settings only applied during initialisation
        """
        pass
        
    def Shutdown(self) :
        """
        Called to release resources
        """
        logging.info(f"Shutdown {self.name}")
        # CALL FUNCTIONS TO RELEASE/SHUTDOWN TC08 UNIT
        
    def read_all_data(self,data_dict) :
        for ch in self.all_channels :
            pass
#            data_dict[f'thermometer.{ch.name}'] = NTC_read({ch.name})