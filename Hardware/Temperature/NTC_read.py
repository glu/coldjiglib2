import math
import piplates.DAQCplate as DAQ
from .. import Hardware

class NTC_read(Hardware.Hardware):
        def __init__(self,name):
                super().__init__(name)

        def initialise(self,parameters) :
                """
                Initialise hardware using settings defined the parameters 
                dictionary
                """
                self.__input_pin = parameters['A_IN_Pin']
                self.__R0 = parameters['Resistance']
                self.__b = parameters['B']
                self.__id = parameters['ID']
                self.__Rfx =parameters['Rfx']
                #print(self.Temperature())


        def add_data_dict_keys(self,data_dict) :
                """
                Add keys to the data dictionary
                """
                data_dict[f'thermometer.{self.__id}'] = 0.0


        def Read(self, data_dict) :
                """
                Reading out all values and write to the data_dict
                """
                data_dict[f'thermometer.{self.__id}'] = self.Temperature()
                #print(self.Temperature())

                
        def Set(self, data_dict) :
                """
                Settings only applied during initialisation
                """
                pass
                
        def Shutdown(self) :
                """
                Called to release resources
                """
                #logging.info(f"Shutdown {self.name}")
                # CALL FUNCTIONS TO RELEASE/SHUTDOWN TC08 UNIT
                
		
        def Temperature(self):
                #print("input pin ",self.__input_pin)
                deltav=DAQ.getADC(0,7)
                #print(deltav)
                Vin=DAQ.getADC(0,int(self.__input_pin))
                #print(Vin)
                To=298.0  #To of NTC
                Ri=int(self.__R0)*math.exp(-int(self.__b)/To)
                Rn=(Vin*int(self.__Rfx))/((deltav*2)-Vin)
                T=int(self.__b)/(math.log(Rn/Ri))-273
                return T

        def get_data(self):
                return {"temperature":self.Temperature()}
