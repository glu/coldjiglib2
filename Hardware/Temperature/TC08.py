# Wrapper for the PicoTech TC08 thermometry logger
import logging
logger = logging.getLogger(__name__)

from .. import Hardware 
import tc08
import time

from collections import namedtuple
channel = namedtuple('channel', 'type name')

class TC08(Hardware.Hardware) :

    def __init__(self,name) :
        """
        Constructor for the TC08 class
        """
        super().__init__(name)
        self.__serial = None   # TC08 Serial Number


    def initialise(self,parameters) :
        """
        Initialise hardware using settings defined the parameters 
        dictionary
        """
        self.__serial = (parameters['Serial']).encode('ascii')

        self.ch1 = self.parse_parameter(parameters['CH1'])
        self.ch2 = self.parse_parameter(parameters['CH2'])
        self.ch3 = self.parse_parameter(parameters['CH3'])
        self.ch4 = self.parse_parameter(parameters['CH4'])
        self.ch5 = self.parse_parameter(parameters['CH5'])
        self.ch6 = self.parse_parameter(parameters['CH6'])
        self.ch7 = self.parse_parameter(parameters['CH7'])
        self.ch8 = self.parse_parameter(parameters['CH8'])

        all_channels = [self.ch1, self.ch2, self.ch3, self.ch4, self.ch5,
                        self.ch6, self.ch7, self.ch8]         
        
        # Create TC08 object 
        self.__tc08 = tc08.TC08(self.__serial)

        # Open unit
        self.__tc08.open_unit()

        # Set mains frequency
        self.__tc08.mains_frequency = int(parameters['Mains'])

        # Set units to centigrade
        self.__tc08.units = "centigrade"

        # Initialise all channels & create list of active channels
        self.active_channels = []
        for i,ch in enumerate(all_channels) :
            self.__tc08.set_channel(i+1, ch.type)
            if ch.type != "DISABLE" :
                self.active_channels.append( (i+1,ch) )

        logger.debug(f'Active Channels: {self.active_channels}')

        # Get minimium sampling interval
        logger.debug(f'Minimum sampling interval (ms): {self.__tc08.minimum_interval}')

        # Start Streaming at minimum interval
        self.__tc08.start_streaming(self.__tc08.minimum_interval)

        # Wait minimum sampling interval to have at least 1 reading in buffer
        time.sleep(self.__tc08.minimum_interval * 0.001)


    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        self.read_all_data(data_dict)


    def Read(self, data_dict) :
        """
        Reading out all values and write to the data_dict
        """
        self.read_all_data(data_dict)

        
    def Set(self, data_dict) :
        """
        Settings only applied during initialisation
        """
        pass
        

    def Shutdown(self) :
        """
        Called to release resources
        """
        logger.info(f"Shutdown {self.name}")
        self.__tc08.close_unit()
        

    def parse_parameter(self, param_string) :

        # split the param string
        ch_type,ch_name = param_string.split(',')
        return channel(ch_type, ch_name)


    def read_all_data(self,data_dict) :
        """
        Read out all thermocouples
        """

        # Read out all channels : Save most recent value in data_dict
        for i,ch in self.active_channels :
            logger.debug(f'Reading {ch.name} (channel {i})')
            data = self.wait_for_data(i)
            data_dict[f'thermometer.{ch.name}'] = (data[-1]).data


    def wait_for_data(self,channel) :
        """
        Wait for data streamed data to be available 
        """

        # Poll every 10ms for data to arrive
        while (data := self.__tc08.read_streamed_channel(channel)) is None :
            logger.debug(f'Waiting for data on channel {channel}')
            time.sleep(0.01) # Sleep 10ms

        logger.debug(f'Data available on channel {channel} : {data}')
        return data
