# Not a real model...just a hack

# efficiency factors based on Peltier dT
def f_neg_dt_peltier(x) :
    if x < -15.0 :
        return 0.0
    elif x > 0.0 :
        return -1.0
    else :    
        return (-x/15.0) - 1.0
    

def f_pos_dt_peltier(x) :
    if x < 0.0 :
        return 1.0
    elif x > 60.0 :
        return 0.0
    else :
        return (-x/60.0) + 1.0

# efficiency factors based on Peltier current
f_I_peltier = lambda x : (x/3.0)

def peltier_dt( dT, I, pmode) :
    f_dt_peltier = f_pos_dt_peltier if pmode else  f_neg_dt_peltier 
    top_dT = f_dt_peltier(dT) * f_I_peltier(I) * 5.0

    return top_dT


# Put in equations to describe peltier bottom temperautre
# in contact with heat exchange and Chiller


class Chuck :
    def __init__(self,top_temp, bottom_temp,peltier_I, pmode) :
        self.top = top_temp
        self.bottom = bottom_temp
        self.mode = pmode
        self.peltier_I = peltier_I

    def update(self,chiller_temp) :

        # Check values are not None
        if chiller_temp is None :
            return

        if self.top is None : 
            return 
        
        if self.bottom is None :
            return 

        if self.peltier_I is None :
            return

        if self.mode is None : 
            return


        top_dT = peltier_dt(self.top - self.bottom, self.peltier_I, self.mode)
        top_dT2 = (self.bottom - self.top) * 0.001
        bottom_dT = (chiller_temp - self.bottom) * 0.01

        print(f'bottom DT: {bottom_dT}  top_DT: {top_dT} top_DT2: {top_dT2}')

        self.top += (top_dT + top_dT2)
        self.bottom += bottom_dT

    



def vc_test() :

    top = -20.0
    bottom = -20.0

    # Test cooling
    pmode = False

    I = 3.0
    
    for i in range(100) :
        peltier_dT = top - bottom
        print(f'i={i} bottom={bottom} top={top}')        
        top += peltier_dt(peltier_dT, I, pmode)
        print(f'new Top={top}')


    print("********************")
        
    top = -20.0
    bottom = -20.0

    # Test heating
    pmode = True

    I = 3.0
    
    for i in range(400) :
        peltier_dT = top - bottom
        print(f'bottom={bottom} top={top}')        
        top += peltier_dt(peltier_dT, I, pmode)
        print(f'new Top={top}')
    
    

if __name__ == '__main__' :
    #vc_test()

    chuck1 = Chuck(20.0,20.0,0.0,False)

    chiller_temp = -20.0

    chuck1.peltier_I = 3

    for i in range(1000) :
        print(f'Idx={i} : Peltier: Bottom={chuck1.bottom} Top={chuck1.top}')
        chuck1.update(chiller_temp)
        print(f'UPDATE: Peltier: Bottom={chuck1.bottom} Top={chuck1.top}')
        print('******************************')

    
    chuck1.mode = True
    for i in range(200) :
        print(f'Idx={i} : Peltier: Bottom={chuck1.bottom} Top={chuck1.top}')
        chuck1.update(chiller_temp)
        print(f'UPDATE: Peltier: Bottom={chuck1.bottom} Top={chuck1.top}')
        print('******************************')
    
