# Dummy Module Temp readout
# Takes current chiller temperature as input and then modifies
# it. Modelling module warm-up/cool-down as an RC curve, with time
# constant set at 1 minute (60 seconds) 

from DCS.Sensors.Sensor import Sensor
import time
import math

class ModuleTempTest(Sensor) :

    def __init__(self,chiller) :
        Sensor.__init__(self,"ModuleNTCTest")
        self.__chiller = chiller
        self.__tau = 1.0
        self.__old_temp = self.__chiller.GetTemperature()
        self.__temp = self.__old_temp
        self.__time = time.monotonic()

    def temperature(self) :
        # Attempt to simulate a time constant between chiller temp and
        # module temp
        time_now = time.monotonic()
        
        dTemp = self.__chiller.GetTemperature() - self.__temp

        # Module already at chiller temperautre
        if dTemp == 0.0 :
            return self.__temp

        # ..got here... there's a temperature difference between
        # chiller and module

        # Get time since temperature change        
        dTime = time_now - self.__time

        temp_now = self.__temp + \
          (dTemp * (1.5 - math.exp(-dTime/self.__tau)))

        self.__time = time_now
        self.__old_temp = self.__temp
        self.__temp = temp_now
        
        return self.__temp

    def get_data(self) :
        return {'channel_2':self.temperature(),
                'channel_3':self.temperature(),
                'channel_4':self.temperature(),
                'channel_5':self.temperature()
                }

    
    
