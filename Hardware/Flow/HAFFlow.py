import time
import decimal
from decimal import*
from smbus import SMBus
from .. import Hardware 
import logging

logger = logging.getLogger(__name__)

class HAFFlow(Hardware.Hardware):

    def __init__(self,name) -> None:
        self.__address=None
        self.__bus=None
        super().__init__(name)

    

    def initialise(self,parameters) :
        """
        Initialise hardware using settings defined in 
        the parameters dictionary
        """
        self.__bus=parameters['bus']
        self.__input = None
        self.__address = parameters['address']
        self.__sensor=SMBus(int(self.__bus))
        self.__flowrange=parameters['flowRange']
        #print(self.read())

    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict['gas_flow'] = None
    

    def Read(self, data_dict) :

        try : 
            data_dict['gas_flow'] = self.read()
            #print(self.read())
        except :
            logging.debug(f'failed to connect to the I2C bus {self.__bus}')
    
    def Set(self, data_dict) :
        pass
        """
        No parameter to set during running
        - empty function
        """

    def Shutdown(self) :
        pass
        """
        Called to release resources
        Nothing to do - empty function
        """
        
    def read(self):
        #print("Ciao!")
        data=self.__sensor.read_i2c_block_data(int(self.__address,16), 0x00, 2)
        #print("Ciao2")
        #print(data)
        #print(data[0])
        #print(data[1])
        time.sleep(0.1)
        #print("ciao2")
        #data2=self.sensor.read_byte(int(self.address))
        #print(data2)
        
        
        output = data[0] << 8 | data[1]
        #print(output)
        #print(self.flowrange)
        #flow1 = ((200*output)/16384)
        #print(flow1)
        flow2= float(self.__flowrange) * (((float(output)/float(16384)) - 0.1)/0.8) 
        #print(flow2)
        conversionFactor = ( (273.15/293.15)*(14.696/14.504) )
        #print(conversionFactor)
        flow= (flow2*conversionFactor)/28.3168;
        return flow
    
    def __del__(self):
        #self.sensor.close()
        pass