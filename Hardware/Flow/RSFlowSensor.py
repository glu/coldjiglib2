# Wrapper for the RS Flow Sensor
# RS 257-149

from .. import Hardware 
import logging

logger = logging.getLogger(__name__)

class RSFlowSensor(Hardware.Hardware) :

    def __init__(self,name) -> None:
        """
        Constructor for RSFlowSensor class
        """
        super().__init__(name)
        self.input = None
        self.__flow_m = ((30-1.5) / 600)
        self.__flow_c = 1.5
        logger.debug(f'RS Flow Senor: m={self.__flow_m} c={self.__flow_c}')


    def initialise(self,parameters) :
        """
        Initialise hardware using settings defined in 
        the parameters dictionary
        """
        self.input = parameters['Input']
        logger.debug(f'Input: {self.input}')

    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict['coolant_flow'] = None
        self.Read(data_dict)   # Do a read to get an initial value

    def Read(self, data_dict) :
        """
        Convert the frequency recorded in input and convert to 
        flow in SLPM
        """
        try : 
            data_dict['coolant_flow'] = \
                (self.__flow_m * data_dict[self.input]) + self.__flow_c
            logger.debug(f'{self.input}={data_dict[self.input]} Hz coolant flow={data_dict["coolant_flow"]} LPM')
        except :
            logger.debug(f'failed to get ADC value {self.input}')


    def Set(self, data_dict) :
        """
        No parameter to set during running
        - empty function
        """
        pass


    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass 
