# Wrapper for the IDT FD2012 Gas Flow Sensor

from .. import Hardware 
import logging

import fs2012

# Set up logger
logger = logging.getLogger(__name__)

class FS2012(Hardware.Hardware) :

    def __init__(self,name) -> None:
        """
        Constructor for the IDT FS2012 class
        """
        super().__init__(name)
        self.flow_sensor = None


    def initialise(self,parameters) :
        """
        Initialise hardware
        """
        self.flow_sensor = fs2012.FS2012(int(parameters['BUS']))

    
    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        # Do an initial read to set gas flow value
        self.flow_sensor.read()
        data_dict['gas_flow'] = self.flow_sensor.flow 

    def Read(self, data_dict) :
        """
        Read the flow sensor
        """
        try : 
            self.flow_sensor.read()
            data_dict['gas_flow'] = self.flow_sensor.flow
        except :
            logging.exception(f'failed to read N2 flow sensor')

    def Set(self, data_dict) :
        """
        Nothing to set
        """
        pass

    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        self.flow_sensor.close()
         