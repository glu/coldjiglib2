# Wrapper for a generic Watchdog OC

from .. import Hardware 
import logging

class Watchdog(Hardware.Hardware) :

    def __init__(self,name) -> None:
        """
        Constructor for generic Watchdog IC
        """
        super().__init__(name)


    def initialise(self,parameters) :
        """
        Initialise hardware
        nothing to set up - empty function
        """
        pass
    
    def add_data_dict_keys(self,data_dict) :
        """
        Add keys to the data dictionary
        """
        data_dict[f'set_watchdog'] = None

    def Read(self, data_dict) :
        """
        Nothing to read  - empty function
        """
        pass 

    def Set(self, data_dict) :
        """
        Toggle watchdog line 
        """
        logging.debug(f'{self.name} : line toggled')
        # WILL NEED TO ADD TOGGLE LINES FOR REAL DEVICE


    def Shutdown(self) :
        """
        Called to release resources
        Nothing to do - empty function
        """
        pass 

