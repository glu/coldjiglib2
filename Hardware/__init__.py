# Factory method to create hardware objects
import importlib
import logging

def create(hardware_name) :

    _, class_name = hardware_name.rsplit('.',1)
    hardware_module = importlib.import_module('.' + hardware_name,package='Hardware')
    hardware_class = getattr(hardware_module,class_name)

    return hardware_class


# Auxillary functions to sort hw-list order to resolve any dependencies

def resolve_inputs(hw_list, data_dict) : 
    hw_check_pass = False
    while not hw_check_pass :
        # Set all set_* in data_dict to None
        data_dict = data_dict.fromkeys(data_dict, None)
        
        # Run HW inputs check
        hw_check_pass = hw_inputs_check(hw_list,data_dict)
        
        logging.debug(f'resolve_inputs: {[hw_i.name for hw_i in hw_list]}')
  
def resolve_outputs(hw_list, data_dict) :
    hw_check_pass = False
    while not hw_check_pass :
        # Set all set_* in data_dict to None
        data_dict = data_dict.fromkeys(data_dict, None)
        
        # Run HW check
        hw_check_pass = hw_outputs_check(hw_list,data_dict)
        
        logging.debug(f'resolve_outputs: {[hw_i.name for hw_i in hw_list]}')


move_to_back = lambda x,hw_list : hw_list.append(hw_list.pop(x))
move_to_front = lambda x,hw_list : hw_list.insert(0,hw_list.pop(x))

def hw_inputs_check(hw_list,data_dict) :
    for idx,hw_i in enumerate(hw_list) :
        if not hw_inputs_set(hw_i,data_dict) :
            logging.debug("hw_inputs_check - Move back")
            move_to_back(idx,hw_list)
            return False
    return True

def hw_outputs_check(hw_list,data_dict) :
    for idx,hw_i in enumerate(hw_list) :
        if not hw_outputs_set(hw_i,data_dict) :
            #print("hw_outputs_check - Move Front")
            move_to_front(idx,hw_list)
            return False
    return True

def hw_inputs_set(hw,data_dict) :
        logging.debug(f'{hw.name}: data_dict:{data_dict}')
        
        for input_i in hw.inputs :
            if data_dict[input_i] is None :
                return False
        
        for read_i in hw.read_keys :
            data_dict[read_i] = 1

        return True

def hw_outputs_set(hw,data_dict) :
        logging.debug(f'{hw.name}: data_dict:{data_dict}')

        for set_i in hw.set_keys :
            if data_dict[set_i] is None :
                data_dict[set_i] = 1

        for output_i in hw.outputs :
            if data_dict[output_i] is None :
                data_dict[output_i] = 1
            else :
                return False

        return True

