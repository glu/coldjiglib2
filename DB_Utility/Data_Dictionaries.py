Master_Dictionary={"chiller.set_temperature" : "float",#
"chiller.set_pump"        : "float",#
"chiller.temperature"     : "float",#
"chiller.pump"            : "float",#
"thermometer.C1"          : "float",#
"thermometer.C2"          : "float",#
"thermometer.C3"          : "float",#
"thermometer.C4"          : "float",#
"thermometer.C5"          : "float",#
"thermometer.CB1"         : "float",#
"thermometer.CB2"         : "float",#
"thermometer.CB3"         : "float",#
"thermometer.CB4"         : "float",#
"thermometer.CB5"         : "float",#
"thermometer.COOL_IN"     : "float",#
"thermometer.COOL_OUT"    : "float",#
"thermometer.AIR1"        : "float",#
"thermometer.AIR2"        : "float",#
"thermometer.LAB"         : "float",#
"thermometer.SPARE"       : "float",#
"peltier.set_volt.1"      : "float",#
"peltier.set_volt.2"      : "float",#
"peltier.set_volt.3"      : "float",#
"peltier.set_volt.4"      : "float",#
"peltier.set_volt.5"      : "float",#
"peltier.set_current.1"   : "float",#
"peltier.set_current.2"   : "float",#
"peltier.set_current.3"   : "float",#
"peltier.set_current.4"   : "float",#
"peltier.set_current.5"   : "float",#
"peltier.volt.1"          : "float",#
"peltier.volt.2"          : "float",#
"peltier.volt.3"          : "float",#
"peltier.volt.4"          : "float",#
"peltier.volt.5"          : "float",#
"peltier.current.1"       : "float",#
"peltier.current.2"       : "float",#
"peltier.current.3"       : "float",#
"peltier.current.4"       : "float",#
"peltier.current.5"       : "float",#
"peltier.set_mode"        : "String",#
"gas_flow"                : "float",#
"coolant_flow"            : "float",#
"RH.1"                    : "float",#
"RH.2"                    : "float",#
"RH.3"                    : "float",#
"RH.4"                    : "float",#
"RH.5"                    : "float",#
"DP.1"                    : "float",#
"DP.2"                    : "float",#
"Lid.1"                   : "String",#
"Lid.2"                   : "String",#
"ADC.0.set_DOUT0"         : "Boolean",#
"ADC.0.DIN0"              : "Boolean",#
"ADC.0.DIN1"              : "Boolean",#
"ADC.0.AIN0"              : "float",#
"ADC.0.AIN1"              : "float",#
"ADC.0.AIN2"              : "float",#
"ADC.0.AIN3"              : "float",#
"ADC.0.AIN4"              : "float",#
"ADC.0.AIN5"              : "float",#
"ADC.0.AIN6"              : "float",#
"ADC.0.AIN7"              : "float",#	
"ADC.0.FREQ"              : "float"}#
#
UKChina_Dictionary={"chiller.set_temperature" : "float",#
"chiller.set_pump"        : "float",#
"chiller.temperature"     : "float",#
"chiller.pump"            : "float",#
"thermometer.C1"          : "float",#
"thermometer.C2"          : "float",#
"thermometer.C3"          : "float",#
"thermometer.C4"          : "float",#
"thermometer.C5"          : "float",#
"thermometer.CB1"         : "float",#
"thermometer.CB2"         : "float",#
"thermometer.CB3"         : "float",#
"thermometer.CB4"         : "float",#
"thermometer.CB5"         : "float",#
"thermometer.COOL_IN"     : "float",#
"thermometer.COOL_OUT"    : "float",#
"thermometer.AIR1"        : "float",#
"thermometer.AIR2"        : "float",#
"thermometer.LAB"         : "float",#
"thermometer.SPARE"       : "float",#
"peltier.set_volt.1"      : "float",#
"peltier.set_volt.2"      : "float",#
"peltier.set_volt.3"      : "float",#
"peltier.set_volt.4"      : "float",#
"peltier.set_volt.5"      : "float",#
"peltier.set_current.1"   : "float",#
"peltier.set_current.2"   : "float",#
"peltier.set_current.3"   : "float",#
"peltier.set_current.4"   : "float",#
"peltier.set_current.5"   : "float",#
"peltier.volt.1"          : "float",#
"peltier.volt.2"          : "float",#
"peltier.volt.3"          : "float",#
"peltier.volt.4"          : "float",#
"peltier.volt.5"          : "float",#
"peltier.current.1"       : "float",#
"peltier.current.2"       : "float",#
"peltier.current.3"       : "float",#
"peltier.current.4"       : "float",#
"peltier.current.5"       : "float",#
"peltier.set_mode"        : "String",#
"gas_flow"                : "float",#
"coolant_flow"            : "float",#
"RH.1"                    : "float",#
"RH.2"                    : "float",#
"DP.1"                    : "float",#
"DP.2"                    : "float",#
"Lid.1"                   : "String",#
"Lid.2"                   : "String",#
"ADC.0.set_DOUT0"         : "Boolean",#
"ADC.0.DIN0"              : "Boolean",#
"ADC.0.DIN1"              : "Boolean",#
"ADC.0.FREQ"              : "float"}#
#
#
US_Dictionary={"chiller.set_temperature" : "float",#
"chiller.temperature"     : "float",#
"thermometer.C1"          : "float",#
"thermometer.C2"          : "float",#
"thermometer.C3"          : "float",#
"thermometer.C4"          : "float",#
"thermometer.AIR1"        : "float",#
"thermometer.AIR2"        : "float",#
"gas_flow"                : "float",#
"RH.1"                    : "float",#
"RH.2"                    : "float",#
"DP.1"                    : "float",#
"DP.2"                    : "float",#
"Lid.1"                   : "String",#
"Lid.2"                   : "String"}
#
#
EndCap_Dictionary={"chiller.set_temperature" : "float",#
"chiller.set_pump"        : "float",#
"chiller.temperature"     : "float",#
"chiller.pump"            : "float",#
"thermometer.C1"          : "float",#
"thermometer.C2"          : "float",#
"thermometer.C3"          : "float",#
"thermometer.C4"          : "float",#
"thermometer.C5"          : "float",#
"thermometer.CB1"         : "float",#
"thermometer.CB2"         : "float",#
"thermometer.CB3"         : "float",#
"thermometer.CB4"         : "float",#
"thermometer.CB5"         : "float",#
"thermometer.COOL_IN"     : "float",#
"thermometer.COOL_OUT"    : "float",#
"thermometer.AIR1"        : "float",#
"thermometer.AIR2"        : "float",#
"thermometer.LAB"         : "float",#
"thermometer.SPARE"       : "float",#
"peltier.set_volt.1"      : "float",#
"peltier.set_volt.2"      : "float",#
"peltier.set_volt.3"      : "float",#
"peltier.set_volt.4"      : "float",#
"peltier.set_volt.5"      : "float",#
"peltier.set_current.1"   : "float",#
"peltier.set_current.2"   : "float",#
"peltier.set_current.3"   : "float",#
"peltier.set_current.4"   : "float",#
"peltier.set_current.5"   : "float",#
"peltier.volt.1"          : "float",#
"peltier.volt.2"          : "float",#
"peltier.volt.3"          : "float",#
"peltier.volt.4"          : "float",#
"peltier.volt.5"          : "float",#
"peltier.current.1"       : "float",#
"peltier.current.2"       : "float",#
"peltier.current.3"       : "float",#
"peltier.current.4"       : "float",#
"peltier.current.5"       : "float",#
"peltier.set_mode"        : "String",#
"gas_flow"                : "float",#
"coolant_flow"            : "float",#
"RH.1"                    : "float",#
"RH.2"                    : "float",#
"RH.3"                    : "float",#
"RH.4"                    : "float",#
"RH.5"                    : "float",#
"DP.1"                    : "float",#
"DP.2"                    : "float",#
"Lid.1"                   : "String",#
"Lid.2"                   : "String",#
"ADC.0.set_DOUT0"         : "Boolean",#
"ADC.0.DIN0"              : "Boolean",#
"ADC.0.DIN1"              : "Boolean",#
"ADC.0.AIN0"              : "float",#
"ADC.0.AIN1"              : "float",#
"ADC.0.AIN2"              : "float",#
"ADC.0.AIN3"              : "float",#
"ADC.0.AIN4"              : "float",#
"ADC.0.AIN5"              : "float",#
"ADC.0.AIN6"              : "float",#
"ADC.0.AIN7"              : "float",#
"ADC.0.FREQ"              : "float"}#


# # data_dict keys

# Date: 08-JUN-2021

# This document list the keys defined, their meaning, and which Coldjigs use them

# | key                     | Meaning                             | Data Type & Units               |UK_China_Barrel | US_Barrel | EndCap |
# |-------------------------|-------------------------------------|---------------------------------|----------------|-----------|--------|
# | chiller.set_temperature | Set Chiller Temperature             | float [℃]                       | X              | X         | X      |
# | chiller.set_pump        | Set Chiller pump speed              | float (units:chiller dependent) | X              |           |        |
# | chiller.temperature     | Read chiller (coolant) temperature  | float [℃]                       | X              | X         |        |
# | chiller.pump            | Read chiller pump speed             | float (units:chiller dependent) | X              |           |        |
# | thermometer.C1          | Read chuck 1 temperature            | float [℃]                       | X              | X         | X      |
# | thermometer.C2          | Read chuck 2 temperature            | float [℃]                       | X              | X         | X      |
# | thermometer.C3          | Read chuck 3 temperature            | float [℃]                       | X              | X         | X      |
# | thermometer.C4          | Read chuck 4 temperature            | float [℃]                       | X              | X         | X      |
# | thermometer.C5          | Read chuck 5 temperature            | float [℃]                       | X              |           |        |
# | thermometer.CB1         | Read chuck 1 cold-block temperature | float [℃]                       | X              |           |        |
# | thermometer.CB2         | Read chuck 2 cold-block temperature | float [℃]                       | X              |           |        |
# | thermometer.CB3         | Read chuck 3 cold-block temperature | float [℃]                       | X              |           |        |
# | thermometer.CB4         | Read chuck 4 cold-block temperature | float [℃]                       | X              |           |        |
# | thermometer.CB5         | Read chuck 5 cold-block temperature | float [℃]                       | X              |           |        |
# | thermometer.COOL_IN     | Read Coolant In temperature         | float [℃]                       | X              |           |        |
# | thermometer.COOL_OUT    | Read Coolant Out temperature        | float [℃]                       | X              |           |        |
# | thermometer.AIR1        | Read AIR1 temperature               | float [℃]                       | X              | X         |        |
# | thermometer.AIR2        | Read AIR2 temperature               | float [℃]                       | X              | X         |        |
# | thermometer.LAB         | Read LAB temperature                | float [℃]                       | X              |           |        |
# | thermometer.SPARE       | no thermocouple connected, empty    | float [℃]                       | X              |           |        |
# | peltier.set_volt.1      | Set Peltier 1 Voltage               | float [V]                       | X              |           | X      |
# | peltier.set_volt.2      | Set Peltier 2 Voltage               | float [V]                       | X              |           | X      |
# | peltier.set_volt.3      | Set Peltier 3 Voltage               | float [V]                       | X              |           | X      | 
# | peltier.set_volt.4      | Set Peltier 4 Voltage               | float [V]                       | X              |           | X      | 
# | peltier.set_volt.5      | Set Peltier 5 Voltage               | float [V]                       | X              |           |        | 
# | peltier.set_current.1   | Set Peltier 1 Current               | float [A]                       | X              |           | X      | 
# | peltier.set_current.2   | Set Peltier 2 Current               | float [A]                       | X              |           | X      | 
# | peltier.set_current.3   | Set Peltier 3 Current               | float [A]                       | X              |           | X      | 
# | peltier.set_current.4   | Set Peltier 4 Current               | float [A]                       | X              |           | X      | 
# | peltier.set_current.5   | Set Peltier 5 Current               | float [A]                       | X              |           |        | 
# | peltier.volt.1          | Read Peltier 1 Voltage              | float [V]                       | X              |           | X      | 
# | peltier.volt.2          | Read Peltier 2 Voltage              | float [V]                       | X              |           | X      |
# | peltier.volt.3          | Read Peltier 3 Voltage              | float [V]                       | X              |           | X      |
# | peltier.volt.4          | Read Peltier 4 Voltage              | float [V]                       | X              |           | X      |
# | peltier.volt.5          | Read Peltier 5 Voltage              | float [V]                       | X              |           |        |
# | peltier.current.1       | Read Peltier 1 Current              | float [A]                       | X              |           | X      |
# | peltier.current.2       | Read Peltier 2 Current              | float [A]                       | X              |           | X      |
# | peltier.current.3       | Read Peltier 3 Current              | float [A]                       | X              |           | X      |
# | peltier.current.4       | Read Peltier 4 Current              | float [A]                       | X              |           | X      |
# | peltier.current.5       | Read Peltier 5 Current              | float [A]                       | X              |           |        |
# | peltier.set_mode        | Set all Peltiers' mode (HEAT/COOL)  | String (HEAT/COOL)              | X              |           |        |
# | gas_flow                | Measure of Dry-Air / N_2 flow       | float [LPM]                     | X              | X         | X      |
# | coolant_flow            | Measure of coolant flow             | float [LPM]                     | X              |           |        |
# | RH.1                    | Humditiy sensor 1 reading           | float [%]                       | X              | X         | X      |
# | RH.2                    | Humidity sensor 2 reading           | float [%]                       | X              | X         | X      |
# | RH.3                    | Humidity sensor 3 reading           | float [%]                       |                |           | X      |
# | RH.4                    | Humidity sensor 4 reading           | float [%]                       |                |           | X      |
# | RH.5                    | Humidity sensor 5 reading           | float [%]                       |                |           | X      |
# | DP.1                    | Dew Point Sensor 1 reading          | float [℃]                       | X              | X         | X      |
# | DP.2                    | Dew Point Sensor 2 reading          | float [℃]                       | X              | X         | X      |
# | Lid.1                   | Read Lid Switch 1 state             | String (OPEN/CLOSED)            | X              | X         | X      |
# | Lid.2                   | Read Lid Switch 2 state             | String (OPEN/CLOSED)            | X              | X         | X      |
# | ADC.0.set_DOUT0         | Set DAQC2Plate 0 Digital Output 0   | Boolean                         | X              |           |        |
# | ADC.0.DIN0              | Read DAQC2Plate 0 Digital Input 0   | Boolean                         | X              |           |        |
# | ADC.0.DIN1              | Read DAQC2Plate 0 Digital Input 1   | Boolean                         | X              |           |        |
# | ADC.0.AIN0              | Read DAQC2Plate 0 Analog Input 0    | float [V]                       |                |           |        |
# | ADC.0.AIN1              | Read DAQC2Plate 0 Analog Input 1    | float [V]                       |                |           |        |
# | ADC.0.AIN2              | Read DAQC2Plate 0 Analog Input 2    | float [V]                       |                |           |        |
# | ADC.0.AIN3              | Read DAQC2Plate 0 Analog Input 3    | float [V]                       |                |           |        |
# | ADC.0.AIN4              | Read DAQC2Plate 0 Analog Input 4    | float [V]                       |                |           |        |
# | ADC.0.AIN5              | Read DAQC2Plate 0 Analog Input 5    | float [V]                       |                |           |        |
# | ADC.0.AIN6              | Read DAQC2Plate 0 Analog Input 6    | float [V]                       |                |           |        |
# | ADC.0.AIN7              | Read DAQC2Plate 0 Analog Input 7    | float [V]                       |                |           |        |
# | ADC.0.FREQ              | Read DAQC2Plate 0 Frequency         | float [Hz]                      | X              |           |        |
