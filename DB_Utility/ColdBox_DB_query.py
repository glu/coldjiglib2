from influxdb_client import InfluxDBClient, Point, Dialect
from influxdb_client.client.write_api import SYNCHRONOUS
import datetime as dt
import time
#import statistics, timedelta ---> Commented out as statistics is not used and timesdelta is not a standard python module
from datetime import datetime, timezone
import json
import configparser
import DBlogging
import random
import csv
from Data_Dictionaries import Master_Dictionary, UKChina_Dictionary, US_Dictionary, EndCap_Dictionary

logging = DBlogging.init_logger(__name__)

def influx_init(config):
    
    dbClient = InfluxDBClient(url=config_influx["url"], token=config_influx["token"], org=config_influx["org"])
    
    return dbClient

# Importing dictionary corresponding to the model of the ColdBox in order to know what the DB has, ideally could be done at the DB level by querying but being too smart could lead to having a very bad time
def coldjigDictionaryImport(ColdJigType):
    ColdJigDictionary = {}
    if ColdJigType == "0":
        ColdJigDictionary = UKChina_Dictionary
    elif ColdJigType == "1":
        ColdJigDictionary = US_Dictionary
    elif ColdJigType == "2":
        ColdJigDictionary = EndCap_Dictionary
    elif ColdJigType == "-1":
        data_dict = {}
        data_dict['thermometer.AIR1'] = "float"
        data_dict['thermometer.AIR2'] = "float"
        data_dict['peltier.set_mode'] = "string"
        data_dict['chiller.set_temperature'] = "float"
        data_dict['gas_flow'] = "float"
        data_dict['DP.1'] = "float"
        data_dict['DP.2'] = "float"
        data_dict['Rand.Test'] = "float"
        ColdJigDictionary = EndCap_Dictionary
    return ColdJigDictionary

###############################################################################################################################
###############################################################################################################################
##################################################### Get Time Flags ##########################################################
###############################################################################################################################
###############################################################################################################################

#Returns last TC_START Flag sent by the ColdjigSW as datetime object, defautls to a search in the last 14 days
def getLatestStartFlagTime(query_API,bucket, timestart="-336h"):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart}) |> filter(fn: (r) => r._measurement == "COMM" and r._field == "DATA" and r._value == "TC_START" ) |> group() |> sort(columns: ["_time"]) |> last()'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0].records[0].get_time()
    # logging.debug('Queried Start flag time')
    return Time

#Returns last TC_START Flag sent by the ColdjigSW as int epoch in ns, defautls to a search in the last 14 days
def getLatestStartFlagTimeEpoch(query_API,bucket, timestart="-336h"):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart}) |> filter(fn: (r) => r._measurement == "COMM" and r._field == "DATA" and r._value == "TC_START" ) |> group() |> sort(columns: ["_time"]) |> last()'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0].records[0].get_time()
    # logging.debug('Queried Start flag time')
    Time = Time.timestamp()*1000000000
    Time = int(Time)
    return Time

#Returns specific TC_START Flag sent by the ColdjigSW tied to a RUN_NUMBER as datetime object, defautls to a search in the last 14 days
def getRunStartFlagTime(query_API, bucket, Run_Number, timestart="-336h"):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart}) |> filter(fn: (r) => r._measurement == "COMM" and r._field =~ /DATA|RUN_NUMBER/ ) |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")|> filter(fn: (r) => r.DATA == "TC_START" and r.RUN_NUMBER == {Run_Number})'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0].records[0].get_time()
    return Time

#Returns specific TC_START Flag sent by the ColdjigSW tied to a RUN_NUMBER as int epoch in ns, defautls to a search in the last 14 days
def getRunStartFlagTimeEpoch(query_API, bucket, Run_Number, timestart="-336h"):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart}) |> filter(fn: (r) => r._measurement == "COMM" and r._field =~ /DATA|RUN_NUMBER/ ) |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")|> filter(fn: (r) => r.DATA == "TC_START" and r.RUN_NUMBER == {Run_Number})'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0].records[0].get_time()
    Time = Time.timestamp()*1000000000
    Time = int(Time)
    return Time


#Returns last TC_END Flag sent by the ColdjigSW as datetime object, defautls to a search in the last 14 days
def getLatestEndFlagTime(query_API,bucket, timestart="-336h"):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart}) |> filter(fn: (r) => r._measurement == "COMM" and r._field == "DATA" and r._value == "TC_END" ) |> group() |> sort(columns: ["_time"]) |> last()'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0].records[0].get_time()
    # logging.debug('Queried End flag time')
    return Time

#Returns last TC_END Flag sent by the ColdjigSW as int epoch in ns, defautls to a search in the last 14 days
def getLatestEndFlagTimeEpoch(query_API,bucket, timestart="-336h"):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart}) |> filter(fn: (r) => r._measurement == "COMM" and r._field == "DATA" and r._value == "TC_END" ) |> group() |> sort(columns: ["_time"]) |> last()'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0].records[0].get_time()
    # logging.debug('Queried Start flag time')
    Time = Time.timestamp()*1000000000
    Time = int(Time)
    return Time

#Returns TC_END Flag sent by the ColdjigSW tied to a RUN_NUMBER as datetime object, defautls to a search in the last 14 days
def getRunEndFlagTime(query_API, bucket, Run_Number, timestart="-336h"):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart}) |> filter(fn: (r) => r._measurement == "COMM" and r._field =~ /DATA|RUN_NUMBER/ ) |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")|> filter(fn: (r) => r.DATA == "TC_END" and r.RUN_NUMBER == {Run_Number})'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0].records[0].get_time()
    return Time
#Returns TC_END Flag sent by the ColdjigSW tied to a RUN_NUMBER as int epoch in ns, defautls to a search in the last 14 days
def getRunEndFlagTimeEpoch(query_API, bucket, Run_Number, timestart="-336h"):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart}) |> filter(fn: (r) => r._measurement == "COMM" and r._field =~ /DATA|RUN_NUMBER/ ) |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")|> filter(fn: (r) => r.DATA == "TC_END" and r.RUN_NUMBER == {Run_Number})'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0].records[0].get_time()
    Time = Time.timestamp()*1000000000
    Time = int(Time)
    return Time

#Returns TEST_START Flag sent by the ITSDAQ in the specified time range
def getTestFlagsStartTimes(query_API,bucket, timestart, timestop):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart},stop:{timestop}) |> filter(fn: (r) => r._measurement == "COMM" and r._field == "DATA" and r._value == "TEST_START") |> sort(columns: ["_time"])'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0]
    # logging.debug('Queried Start flag time')
    return Time

#Returns TEST_END Flag sent by the ITSDAQ in the specified time range
def getTestFlagsEndTimes(query_API,bucket, timestart, timestop):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart},stop:{timestop}) |> filter(fn: (r) => r._measurement == "COMM" and r._field == "DATA" and r._value == "TEST_END") |> sort(columns: ["_time"])'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0]
    # logging.debug('Queried Start flag time')
    return Time

# Returns Cycles times irrespective of their start or end times, sent by the ITSDAQ in the specified time range. 
# Given the fact that each cycle has only one start and one end this list should be even in if the start and end times correspond to TC_START and TC_END 
# Need to be written more robustly
def getTestCycleTimes(query_API,bucket, timestart, timestop):
    DB_query = f'from(bucket: \"{bucket}\") |> range(start: {timestart},stop:{timestop}) |> filter(fn: (r) => r._measurement == "COMM" and r._field == "CYCLE") |> sort(columns: ["_time"])'
    TableQuery = query_API.query(DB_query)
    Time = TableQuery[0]
    # logging.debug('Queried Start flag time')
    return Time

#Helper Epoch function not really used
def returnEpoch(Time):
    Time = Time.timestamp()*1000000000
    Time = int(time)
    return Time


#Queries require a time Delta for start time, getting it with manipulating dateime to get a timedelta object Obsolete to be deleted but left as reference for the moment
def getTimeDeltaStart(_query_api,_bucket):
    _TimeStart = getLatestStartFlagTime(_query_api,_bucket)
    _CurrentTime = datetime.now(timezone.utc)
    _TimeStartDelta= _CurrentTime-_TimeStart
    return _TimeStartDelta

#Queries require a time Delta for stop time, getting it with manipulating dateime to get a timedelta object  Obsolete to be deleted but left as reference for the moment
def getTimeDeltaEnd(_query_api,_bucket):
    _TimeEnd = getLatestEndFlagTime(_query_api,_bucket)
    _CurrentTime = datetime.now(timezone.utc)
    _TimeEndDelta= _CurrentTime-_TimeEnd
    return _TimeEndDelta



###############################################################################################################################
###############################################################################################################################
###################################################### Measurements ###########################################################
###############################################################################################################################
###############################################################################################################################

#Get the selected get the selected measurement, meaning all data from time start to time stop in table format
def getMeasurement(query_API,bucketname,measurement,timestart,timestop):
    DB_query = f'from(bucket: "{bucketname}") |> range(start: {timestart},stop:{timestop}) |> filter(fn: (r) => r["_measurement"] == "{measurement}")'
    TableQuery = query_API.query(DB_query)
    if TableQuery:
        return TableQuery[0]
    else:
        return None

#Get specific field the selected get the selected measurement, all data from time start to time stop in table format
def getMeasurementSpecific(query_API,bucketname,measurement,timestart,timestop,field):
    DB_query = f'from(bucket: "{bucketname}") |> range(start: {timestart},stop:{timestop}) |> filter(fn: (r) => r["_measurement"] == "{measurement}") |> filter(fn: (r) => r._field == "{field}")'
    TableQuery = query_API.query(DB_query)
    if TableQuery:
        return TableQuery[0]
    else:
        return None

#Get the selected get the selected measurement, meaning all data from time start to time stop in CSV format
def getMeasurement_CSV(query_API,bucketname,measurement,timestart,timestop):
    DB_query = f'from(bucket: "{bucketname}") |> range(start: {timestart},stop:{timestop}) |> filter(fn: (r) => r["_measurement"] == "{measurement}")'
    CSVQuery = query_API.query_csv(DB_query,dialect=Dialect(header=False, delimiter=",", comment_prefix="#", annotations=[],date_time_format="RFC3339"))
    return CSVQuery

#Get specific field the selected get the selected measurement, all data from time start to time stop in CSV format
def getMeasurement_CSVSpecific(query_API,bucketname,measurement,timestart,timestop,field):
    DB_query = f'from(bucket: "{bucketname}") |> range(start: {timestart},stop:{timestop}) |> filter(fn: (r) => r["_measurement"] == "{measurement}") |> filter(fn: (r) => r._field == "{field}")'
    CSVQuery = query_API.query_csv(DB_query,dialect=Dialect(header=False, delimiter=",", comment_prefix="#", annotations=[],date_time_format="RFC3339"))
    return CSVQuery

#Get the selected field on the selected measurement on the bucket (or DB) from timestart to timestop averaged for every X minutes default to 5, with the query API, works only on non str data
def getMeasurementAveraged(query_API,bucketname,measurement,timestart,timestop,field,averaged='1m'):
    DB_query = f'from(bucket: "{bucketname}") |> range(start: {timestart},stop:{timestop}) |> filter(fn: (r) => r["_measurement"] == "{measurement}") |> filter(fn: (r) => r._field == "{field}") |> aggregateWindow(every: {averaged}, fn: mean, createEmpty: false)'
    TableQuery = query_API.query(DB_query)
    if TableQuery:
        return TableQuery[0]
    else:
        return None

# returns dictionnary of the averaged values of the selected field for the selected measurement over the whole duration between timestart and timestop, NB timestart and timestop need to be datetime objetcs
def getMeasurementAveragedDuringTesting(query_API,bucketname,measurement,timestart,timestop,field):
    _measurementsAveraged={}
    diff = timestop-timestart
    timestart = timestart.timestamp()*1000000000
    timestart = int(timestart)
    timestop = timestop.timestamp()*1000000000
    timestop = int(timestop)
    # print(f'{diff.seconds}s') 
    averaged = f'{diff.seconds}s'
    DB_query_mean   = f'from(bucket: "{bucketname}") |> range(start: time(v:{timestart}),stop:time(v:{timestop})) |> filter(fn: (r) => r["_measurement"] == "{measurement}") |> filter(fn: (r) => r._field == "{field}") |> aggregateWindow(every: {averaged}, fn: mean, createEmpty: false)'
    DB_query_stddev = f'from(bucket: "{bucketname}") |> range(start: time(v:{timestart}),stop:time(v:{timestop})) |> filter(fn: (r) => r["_measurement"] == "{measurement}") |> filter(fn: (r) => r._field == "{field}") |> aggregateWindow(every: {averaged}, fn: stddev, createEmpty: false)'
    DB_query_min    = f'from(bucket: "{bucketname}") |> range(start: time(v:{timestart}),stop:time(v:{timestop})) |> filter(fn: (r) => r["_measurement"] == "{measurement}") |> filter(fn: (r) => r._field == "{field}") |> aggregateWindow(every: {averaged}, fn: min, createEmpty: false)'
    DB_query_max    = f'from(bucket: "{bucketname}") |> range(start: time(v:{timestart}),stop:time(v:{timestop})) |> filter(fn: (r) => r["_measurement"] == "{measurement}") |> filter(fn: (r) => r._field == "{field}") |> aggregateWindow(every: {averaged}, fn: max, createEmpty: false)'

    DB_query_mean_results = query_API.query(DB_query_mean)
    DB_query_stddev_results = query_API.query(DB_query_stddev)
    DB_query_min_results = query_API.query(DB_query_min)
    DB_query_max_results = query_API.query(DB_query_max)

    _measurementsAveraged = {"Mean":DB_query_mean_results[0].records[0].get_value(),
    "Stddev":DB_query_stddev_results[0].records[0].get_value(),
    "Min":DB_query_min_results[0].records[0].get_value(),
    "Max":DB_query_max_results[0].records[0].get_value()
    }
    if _measurementsAveraged:
        return _measurementsAveraged
    else:
        return None


#Prints a formatted JsonFile by passing the Table record from a query and the selected keys
def printToJSON(keys,QueryRecord,JSONOut='OutPut.json'):
    _LargeDictionary = []
    for key in keys:
        for record in QueryRecord.records:
            #print(record)
            # print(f'Time :{record["_time"]} Variable : {record["_field"]} Value : {record["_value"]}')
            _Dictionary = {'Time':record["_time"].timestamp(), 'Variable':record["_field"], 'Value':record["_value"]}
            _LargeDictionary.append(_Dictionary)
    
    with open(JSONOut, 'w', encoding='utf-8') as f:
        json.dump(_LargeDictionary, f, ensure_ascii=False, indent=4)
    
# write test for testing purpose will be removed later on
# Writes a TC_START with a RUN_NUMBER
# Writes random values before the testing starting
# Writes TEST_START and CYCLES starting from 1 to 11 (0 should be IDLE if written)
# 
def writeTestData(config_influx,data_dict):
    RUN_NUMBER = 12
    with InfluxDBClient(url=config_influx['url'], token=config_influx['token'], org=config_influx['org']) as _client:
        with _client.write_api() as write_api:
            point = Point("COMM").tag("SETUP", "Lund").tag("SENDER","COLDJIG").tag("RECEIVER","ITSDAQ").field("COMMAND","STATUS").field("DATA", "TC_START").field("RUN_NUMBER",RUN_NUMBER).field("ERROR", "")
            write_api = _client.write_api(write_options=SYNCHRONOUS)
            write_api.write(bucket=config_influx['bucket'], record=point)
    print('Sleeping for 1')
    time.sleep(1)
    for i in range(0,60):
        with InfluxDBClient(url=config_influx['url'], token=config_influx['token'], org=config_influx['org']) as _client:
            with _client.write_api() as write_api:
                data_dict['thermometer.AIR1'] = 20.0+random.randint(-10, 10)
                data_dict['thermometer.AIR2'] = 20.0+random.randint(-10, 10)
                data_dict['peltier.set_mode'] = '"COOL"'
                data_dict['chiller.set_temperature'] = 20.0+random.randint(-10, 10)
                data_dict['gas_flow'] = 6.0+random.randint(-10, 10)
                data_dict['DP.1'] = -10.0+random.randint(-10, 10)
                data_dict['DP.2'] = -10.0+random.randint(-10, 10)
                data_dict['Rand.Test'] = random.randint(-10, 10)

                write_api.write(config_influx['bucket'], config_influx['org'], {"measurement": 'Lund',"fields": data_dict})
                print(f'step {i}')
                time.sleep(1)



#---------------------------------------------------------------------------------------------
    for j in range(1,11):
        with InfluxDBClient(url=config_influx['url'], token=config_influx['token'], org=config_influx['org']) as _client:
            with _client.write_api() as write_api:
                point = Point("COMM").tag("SETUP", "Lund").tag("SENDER","ITSDAQ").tag("RECEIVER","COLDJIG").field("COMMAND","STATUS").field("DATA", "TEST_START").field("CYCLE", j).field("ERROR", "")
                write_api = _client.write_api(write_options=SYNCHRONOUS)
                write_api.write(bucket=config_influx['bucket'], record=point)
    
        for i in range(0,59):
            with InfluxDBClient(url=config_influx['url'], token=config_influx['token'], org=config_influx['org']) as _client:
                with _client.write_api() as write_api:
                    data_dict['thermometer.AIR1'] = 20.0+random.randint(-10, 10)
                    data_dict['thermometer.AIR2'] = 20.0+random.randint(-10, 10)
                    data_dict['peltier.set_mode'] = '"COOL"'
                    data_dict['chiller.set_temperature'] = 20.0+random.randint(-10, 10)
                    data_dict['gas_flow'] = 6.0+random.randint(-10, 10)
                    data_dict['DP.1'] = -10.0+random.randint(-10, 10)
                    data_dict['DP.2'] = -10.0+random.randint(-10, 10)
                    data_dict['Rand.Test'] = random.randint(-10, 10)
    
                    write_api.write(config_influx['bucket'], config_influx['org'], {"measurement": 'Lund',"fields": data_dict})
                    print(f'step {i}')
                    time.sleep(1)
    
        with InfluxDBClient(url=config_influx['url'], token=config_influx['token'], org=config_influx['org']) as _client:
            with _client.write_api() as write_api:
                point = Point("COMM").tag("SETUP", "Lund").tag("SENDER","ITSDAQ").tag("RECEIVER","COLDJIG").field("COMMAND","STATUS").field("DATA", "TEST_END").field("CYCLE", j).field("ERROR", "")
                write_api = _client.write_api(write_options=SYNCHRONOUS)
                write_api.write(bucket=config_influx['bucket'], record=point)
    
#-----------------------------------------------------------------------------------
    with InfluxDBClient(url=config_influx['url'], token=config_influx['token'], org=config_influx['org']) as _client:
        with _client.write_api() as write_api:
            point = Point("COMM").tag("SETUP", "Lund").tag("SENDER","COLDJIG").tag("RECEIVER","ITSDAQ").field("COMMAND","STATUS").field("DATA", "TC_END").field("RUN_NUMBER",RUN_NUMBER).field("ERROR", "")
            write_api = _client.write_api(write_options=SYNCHRONOUS)
            write_api.write(bucket=config_influx['bucket'], record=point)
    print('Sleeping for 1')
    time.sleep(1)
#---------



if __name__ == '__main__':
    print('--- influxDB_query ---')
    #-- these configs are for test, when runniing this module individually.
    Config_file_location = ''
    config = configparser.ConfigParser()
    config.read('influx.ini')
    config.sections()




#####################################################################
#####################################################################
#Poor's man influx initialisation need to get into it's own function#
#####################################################################
#####################################################################

    data_dict_ref = coldjigDictionaryImport(config["DEFAULT"]["ColdBoxType"])
    print(data_dict_ref)
    #placeholder in theory a dummy data dict would be loaded to get the keys, or at least have the keys in a list
    data_dict = {}
    data_dict['thermometer.AIR1'] = 20.0+random.randint(-10, 10)
    data_dict['thermometer.AIR2'] = 20.0+random.randint(-10, 10)
    data_dict['peltier.set_mode'] = '"COOL"'
    data_dict['chiller.set_temperature'] = 20.0+random.randint(-10, 10)
    data_dict['gas_flow'] = 6.0+random.randint(-10, 10)
    data_dict['DP.1'] = -10.0+random.randint(-10, 10)
    data_dict['DP.2'] = -10.0+random.randint(-10, 10)
    data_dict['Rand.Test'] = random.randint(-10, 10)

    config_influx={}
    if config["influx"]["version"]=='1':
        config_influx = {
            "url": config["influx"]["ip"]+":"+config["influx"]["port"],
            "token": f'{config["influx"]["username"]}:{config["influx"]["password"]}',
            "org":config["influx"]["org"],
            "database": config["influx"]["database"],
            "bucket": f'{config["influx"]["database"]}/{config["influx"]["retention"]}'
        }
    elif config_influx["version"]=='2':
        config_influx = {
            "url": config["influx"]["ip"]+":"+config["influx"]["port"],
            "token": f'{config["influx"]["token"]}',
            "org":config["influx"]["org"],
            "database": config["influx"]["database"],
            "bucket": f'{config["influx"]["bucket"]}'
        }
    _dbClient = influx_init(config_influx)
    _query_api = _dbClient.query_api()
    _write_api = _dbClient.write_api()
    _dbName = config_influx["database"]
    _institute = config["influx"]["measurement"]

####################################################################
############ Uncomment to push test data ###########################
####################################################################

    # writeTestData(config_influx,data_dict)

####################################################################

# Needed keys of the data dict reference, right now got way too many values need to be reduced to the number of values to be saved per coldjig types
    keys = data_dict_ref.keys()
    
# Test for finding start and end flag times of TC
    print(getLatestStartFlagTime(_query_api,config_influx['bucket']))
    print(getLatestEndFlagTime(_query_api,config_influx['bucket']))

# Test for finding start and end flag times of TC
    Timestartobject = getLatestStartFlagTime(_query_api,config_influx['bucket'])
    Timeendobject = getLatestEndFlagTime(_query_api,config_influx['bucket'])

# get the Epoch to be used in queries
    Timestart = getLatestStartFlagTimeEpoch(_query_api,config_influx['bucket'])
    Timestop = getLatestEndFlagTimeEpoch(_query_api,config_influx['bucket'])
    # Timestop = int(Timestop)

# creating dictionaries and tables
    StartTimes={}
    tabletest=[]
# saving the query
    TestFlagStart=getTestCycleTimes(_query_api,config_influx['bucket'],f'time(v: {Timestart})',f'time(v: {Timestop})')

#printing the query
    print(TestFlagStart)

# scanning the records of the query
    for record in TestFlagStart.records:
        Dictionary = {"Time":record["_time"], "Cycle":record["_value"]}
        tabletest.append(Dictionary)

# getting the flag dictionaries
    ListofFlagDictionary = []
# get one random field measurement to test the averaging and print it
    print(getMeasurementAveragedDuringTesting(_query_api,config_influx['bucket'],_institute,Timestartobject,Timeendobject,'thermometer.AIR1'))
    
# getting the test start and end flags in a dictionary for future use
    for i in range(0,len(tabletest),2):
        Dictionary={"Start":tabletest[i]["Time"],"Stop":tabletest[i+1]["Time"],"Cycle":tabletest[i]["Cycle"]}
        ListofFlagDictionary.append(Dictionary)
        
    print(ListofFlagDictionary)
    # printing the dictonary with human readable start and stop time and associated cycles
    for dic in ListofFlagDictionary:
        print(f'{dic["Start"].strftime("%c")},{dic["Stop"].strftime("%c")},{dic["Cycle"]}')

    print("-------------------------")
    # for items in getRunStartFlagTime(_query_api,config_influx['bucket'],10,'-300h'):
    #     for item in items:
    #         print(item)

# Testing getting specific run numbers, in theory should change the run number and writing other ones before testing it otherwise will return possibly an error

    # print(getRunStartFlagTime(_query_api,config_influx['bucket'],10,'-300h').strftime("%c"))
    # print(getRunEndFlagTime(_query_api,config_influx['bucket'],10,'-300h').strftime("%c"))
    # print(getRunStartFlagTime(_query_api,config_influx['bucket'],11,'-300h').strftime("%c"))
    # print(getRunEndFlagTime(_query_api,config_influx['bucket'],11,'-300h').strftime("%c"))
    print("-------------------------")
    
    for dic in tabletest:
        print(dic)



#-----------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------#
#------------------Following code needs to be deleted but left here until full cleanup----------------------------#
#-----------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------#


    # TestFlagStop=getTestCycleEndTimes(_query_api,config_influx['bucket'],f'time(v: {Timestart})',f'time(v: {Timestop})')
    # print(getTestCycleEndTimes(_query_api,config_influx['bucket'],f'time(v: {Timestart})',f'time(v: {Timestop})'))
    # for record in TestFlagStop.records:
    #     print(record)



    # print(getMeasurement(query_API,bucketname,"Lund",measurement,timestart,timestop))
    #global measurement averaged over duration 
    # for key in data_dict_ref.keys():
    #     if data_dict_ref[key] == "float":
    #         _FullQuery = getMeasurementAveraged(_query_api,config_influx['bucket'],"Lund",f'time(v: {Timestart})',f'time(v: {Timestop})',key,'30s')
    #         if _FullQuery:
    #             for record in _FullQuery.records:
    #                 print(record)

    

    
    # JSONDump = json.dumps(getMeasurement(_query_api,config_influx['bucket'],Institute,f'-{TimeDeltaStartStamp.seconds}s',f'-{TimeDeltaStopStamp.seconds}s'))

    # print(JSONDump)

#-----------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------#
#------------------------------------------TEST VARIABLES AVERAGED------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------#
    # - mean |> aggregateWindow(every: {averaged}, fn: mean, createEmpty: false)
    # - standard deviation |> aggregateWindow(every: {averaged}, fn: stddev, createEmpty: false)
    # - maximum value |> aggregateWindow(every: {averaged}, fn: max, createEmpty: false)
    # - minimum value |> aggregateWindow(every: {averaged}, fn: min, createEmpty: false)

#----------------- CSV Example -------------------
    # CSV_meas = getMeasurement_CSV(_query_api,config_influx['bucket'],Institute,f'-{TimeDeltaStartStamp.seconds}s',f'-{TimeDeltaStopStamp.seconds}s')
    # print(getMeasurement_CSV(_query_api,config_influx['bucket'],Institute,f'-{TimeDeltaStartStamp.seconds}s',f'-{TimeDeltaStopStamp.seconds}s'))

    # with open('ColdJigLogs.csv', mode='w') as ColdJigLogs:
    #     csv_writer = csv.writer(ColdJigLogs, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    #     for csv_line in CSV_meas:
    #         if not len(csv_line) == 0:
    #             csv_writer.writerow(csv_line)
#-------------------------------------------------
   ################### # BigDic = []
   ################### # printToJSON(keys,getMeasurement(_query_api,config_influx['bucket'],Institute,f'-{getTimeDeltaStart(_query_api,config_influx["bucket"]).seconds}s',f'-{getTimeDeltaEnd(_query_api,config_influx["bucket"]).seconds}s'),'OutPut.json')
    

# -----------
    # for key in keys:
    #     if 'thermometer' in key:
    #         for record in getMeasurementAveraged(_query_api,config_influx['bucket'],key,f'-{TimeDeltaStartStamp.seconds}s',f'-{TimeDeltaStopStamp.seconds}s').records:
    #             #print(record)
    #             print(f'Time :{record["_time"]} Variable : {record["_field"]} Value : {record["_value"]}')
# -----------


    # for record in getMeasurement(_query_api,config_influx['bucket'],"-24h","now()").records:
    #     print(record)

    # print(TableQuery[0])
    # print(getLatestStartFlagTime(_query_api,config_influx["bucket"],"-24h"))
    # print( get_measurement(_dbClient,_dbName,'esp32test','TrH','rH') )
    # print( get_measurement(_dbClient,_dbName,'esp32test','TrH','T') )

    # print( get_measurement_range(_dbClient,_dbName,'esp32test','TrH','T', '2021-03-03T15:00:00.000Z', '2021-03-03T17:37:00.000Z') )
