from . import ThermalCycle
import logging
import time

logger = logging.getLogger(__name__)

class Warwick_TC_TEST(ThermalCycle.ThermalCycle) :
    """
    Impementation of the Thermal Cycling stages for the 
    Warwick ColdJig
    -- SPECIAL TEST VERSION
    """

    def __init__(self,data_dict,stop_tc) -> None:
        super().__init__("Warwick_TC_TEST",data_dict,stop_tc)

    def setup(self) :
        logger.info(f'{self.name}:Setup Called - empty for now')

    def finish(self) :

        # Turn off all peltiers
        self.data_dict['peltier.set_volt.1'] = 0.0
        self.data_dict['peltier.set_volt.2'] = 0.0
        self.data_dict['peltier.set_volt.3'] = 0.0
        self.data_dict['peltier.set_volt.4'] = 0.0
        self.data_dict['peltier.set_volt.5'] = 0.0
        
        self.data_dict['peltier.set_current.1'] = 0.0
        self.data_dict['peltier.set_current.2'] = 0.0
        self.data_dict['peltier.set_current.3'] = 0.0
        self.data_dict['peltier.set_current.4'] = 0.0
        self.data_dict['peltier.set_current.5'] = 0.0
        
        # Set chiller to +20C
        self.data_dict['chiller.set_temperature'] = 20.0

        # Wait until all internal temperatures are above +15C
        not_warm = True
        all_temps = ['VC1','VC2','VC3','VC4','VC5',
                     'CB1','CB2','CB3','CB4','CB5',
                     'AIR1','AIR2']  ## MAKE LIST OF TEMPS TO MONITOR
        while (not_warm and not self.stop_tc.is_set()) :
            for t in all_temps :
                temp = self.data_dict[f'thermometer.{t}']
                if temp < 15 : 
                    not_warm = True
                    break
                else :
                    not_warm = False
            
            time.sleep(1)

        if self.stop_tc.is_set() : 
            logger.info("Thermal cycle aborted")
        else :
            logger.info("All modules above +15C")


    def cool_down(self,modules):

        # Send alert to turn up N2 flow to max
        # logger.info("Waiting to turn N_2 flow to max")

        # # NEED A WAY TO POP UP ALERT BOX ON GUI SCREEN

        # THIS CHECK IS DISABLED UNTIL N2 RETURNS 
        # AND N2 FLOW SENSOR IS CONNECTED
        # Check N2 flow reaches max
        #while (not self.stop_tc.is_set()) :
        #    if self.data_dict['gas_flow'] > 8.0 : 
        #        break
        #    time.sleep(1)
        
        # logger.info("gas_flow is set > 8.0 LPM")

        # Set Chiller to +20C & pump-speed 5
        self.data_dict['chiller.set_temperature'] = 20.0
        self.data_dict['chiller.set_pump'] = 5

        # Start cool down.  
        # # Monitor dew point and wait until it reaches -45C
        # logger.info("Waiting for box to dry out")
        # while (not self.stop_tc.is_set()) :

        #     # Get average Dew Point
        #     dp = (self.data_dict['DP.1'] + 
        #             self.data_dict['DP.2']) / 2.0

        #     logger.info(f"Current Ave Dew Point: {dp}")
        #     if dp <= -45.0 :       
        #         break
        
        #     time.sleep(1)

        # logger.info("ColdJig is dry. Turn down flow to 7 LPM")

        # # Wait for flow to reduce to 7 LPM
        # logger.info("Waiting for flow to reduce...")
        # while (not self.stop_tc.is_set()) :
        #     logger.info(f'Gas Flow: {self.data_dict["gas_flow"]}')
        #     if (self.data_dict['gas_flow'] < 8.0) and \
        #          (self.data_dict['gas_flow'] > 6.0): 
        #         break   
        #     time.sleep(1)

        # logger.info(f'N2 flow set to {self.data_dict["gas_flow"]} LPM')

        # # Wait 1 minutes to let Dew Points settle
        # logger.info("Waiting 1 min to let Dew Points settle")
        # time.sleep(3)
        
        # # Check DP again 
        # av_dp = (self.data_dict['DP.1'] + self.data_dict['DP.2']) * 0.5
        # if av_dp > -40.0 :
        #     logger.warning(f'Dew point is above -40C after turning down N2 FLow.')
        #     logger.warning(f'Aborting setup')
        #     return False

        # Set the module temp to -35C
        # for m in modules :
        #     self.data_dict[f'module.set_temperature.{m}'] = -35.0

        # Set all Peltier currents to 1A & COOL
        self.data_dict['peltier.set_mode'] = 'COOL'
        for m in modules :
            self.data_dict[f'peltier.set_current.{m}'] = 1.0
            self.data_dict[f'peltier.set_volt.{m}'] = 15.0

        # Wait until all modules are below 15C
        all_cold = False 
        while(all_cold == False and not self.stop_tc.is_set()) :
            logger.info(f'all_cold {all_cold}')
            for m in modules :
                temp = self.data_dict[f'thermometer.VC{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp > 15.0 : 
                    all_cold = False
                    break
                else :
                    all_cold = True
            
            time.sleep(1)

        if self.stop_tc.is_set() : 
            logger.info("Thermal cycle aborted")
        else :
            logger.info("All modules above +15C")

        return True


    def warm_up(self,set_temperature,modules) :

        # Turn Peltiers off
        for m in modules : 
            self.data_dict[f'peltier.set_volt.{m}'] = 0.0
            self.data_dict[f'peltier.set_current.{m}'] = 0.0  

        # Set chiller to set_temperature
        self.data_dict['chiller.set_temperature'] = set_temperature



        # Set module to set_temperature
        # self.data_dict['module.set_temperature'] = set_temperature
        # WILL NEED TO DEFINE DATA DICT KEY FOR MODULE TEMPERATURE

        # Wait until modules reach set_temperature 
        # There also needs to be a timeout here...
        still_warming = True
        while(still_warming and not self.stop_tc.is_set()) :
            logger.info(f'still_warming: {still_warming}')
            for m in modules :
                temp = self.data_dict[f'thermometer.VC{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp < set_temperature : 
                    still_warming = True
                    break
                else :
                    still_warming = False
            
            time.sleep(1)

        if self.stop_tc.is_set() : 
            logger.info("Thermal cycle aborted")
        else :
            logger.info(f'Modules have reached {set_temperature}C')

        return 

    def fast_heat(self,set_temperature,modules) :

        # Change Peltier mode 
        self.data_dict['peltier.set_mode'] = 'HEAT'

        # Set Peltier Voltage/Current    
        for m in modules :
            self.data_dict[f'peltier.set_current.{m}'] = 1.0
            self.data_dict[f'peltier.set_volt.{m}'] = 15.0


        # Set module temperature 
        # JUST A PLACE HOLDER NOW...NOT SURE I NEED IT 

        # Wait for modules to reach set_temperature+/-1C
        still_warming = True
        while(still_warming and not self.stop_tc.is_set()) :
            logger.info(f'still warming: {still_warming}')
            for m in modules :
                temp = self.data_dict[f'thermometer.VC{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp < set_temperature : 
                    still_warming = True
                    break
                else :
                    still_warming = False
            
            time.sleep(1)        

        if self.stop_tc.is_set() : 
            logger.info("Thermal cycle aborted")
        else :
            logger.info(f'Modules have reached {set_temperature}')

        return

    def fast_cool(self,set_temperature,modules) :

        # Change Peltier mode 
        self.data_dict['peltier.set_mode'] = 'COOL'

        # Set Peltier Voltage/Current    
        for m in modules :
            self.data_dict[f'peltier.set_current.{m}'] = 1.0
            self.data_dict[f'peltier.set_volt.{m}'] = 15.0


    
        # Set module temperature 
        # JUST A PLACE HOLDER NOW...NOT SURE I NEED IT 

        # Wait for modules to reach set_temperature+/-1C
        still_cooling = True
        while(still_cooling and not self.stop_tc.is_set()) :
            logger.info(f'still_cooling: {still_cooling}')
            for m in modules :
                temp = self.data_dict[f'thermometer.VC{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp > set_temperature : 
                    still_cooling = True
                    break
                else :
                    still_cooling = False
            
            time.sleep(1)        
        if self.stop_tc.is_set() : 
            logger.info("Thermal cycle aborted")
        else :
            logger.info(f'Modules have reached {set_temperature}')

        return


    def cold_characterisation(self,modules) :
        
        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('COLD_TEST')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        pass 

    
    def warm_characterisation(self,modules) :
        
        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('WARM_TEST')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        pass 


    def warm_confirmation(self,modules) :
        
        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('WARM_CONFIRMATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        pass 

    def cold_confirmation(self,modules) :
        
        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('COLD_CONFIRMATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        pass 


    def hv_stabilisation(self,modules) :
        
        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('HV_STABILISATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        pass 

