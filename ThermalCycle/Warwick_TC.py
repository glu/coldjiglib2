from . import ThermalCycle
import logging
import time

logger = logging.getLogger(__name__)

from pubsub import pub
# the defined messages topics are:
# - 'warning' : eg: "humidity increasing", "N2 flow low"
# - 'error' : eg: "No response from chiller", "can't connect to INFLUX"
# - 'alert' : "Turn down N2 flow", "Safe to open lid", "module QC completed"
# - 'danger' : "condensation on module", "interlock fired","Peltier overheating"

class Warwick_TC(ThermalCycle.ThermalCycle) :
    """
    Impementation of the Thermal Cycling stages for the 
    Warwick ColdJig
    """

    def __init__(self,data_dict,stop_tc,influx) -> None:
        super().__init__("Warwick_TC",data_dict,stop_tc,influx)

    def setup(self,modules) :
        # Put PID into IDLE
        for m in modules :
            self.data_dict[f'PID.Chuck{m}.set_mode'] = 'IDLE'
        logger.info(f'{self.name}:Setup Called - PID set to IDLE')

        self.wait_for_PID_idle(modules)

        # Now all Chuck PIDS are idle, can now set current
        # Set all Peltiers to 0V/0A
        for i in range(1,6) :
            self.data_dict[f'peltier.set_volt.{i}'] = 0.0
            self.data_dict[f'peltier.set_current.{i}'] = 0.0

        # Set peltier mode to COOL
        self.data_dict['peltier.set_mode'] = 'COOL'

        # Skip module setup if stop_tc is set
        if self.stop_tc.is_set() :
            logger.debug("Thermal Cycle aborted - skipping module setup")
        else :
            super().setup(modules)

    def finish(self) :
        logger.debug(f'{self.name}:finish() called')

        # Set all chucks' PID to IDLE
        all_modules = list(range(1,6))
        for m in all_modules :
            self.data_dict[f'PID.Chuck{m}.set_mode'] = 'IDLE'
        self.wait_for_PID_idle(all_modules)

        # Now all Chuck PIDS are idle, can now set current
        # Turn off all peltiers
        for m in all_modules :
            self.data_dict[f'peltier.set_volt.{m}'] = 0.0
            self.data_dict[f'peltier.set_current.{m}'] = 0.0

        # Set peltier mode to COOL
        self.data_dict['peltier.set_mode'] = 'COOL'

        # Set chiller to +20C
        self.data_dict['chiller.set_temperature'] = 20.0

        # Skip warm up if thermal cyle aborted
        if self.stop_tc.is_set() :
            logger.info("Thermal Cycled Aborted - warm up skipped")
            return

        # Wait until all internal temperatures are above +15C
        not_warm = True
        all_temps = ['VC1','VC2','VC3','VC4','VC5',
                     'CB1','CB2','CB3','CB4','CB5',
                     'AIR1','AIR2']  ## MAKE LIST OF TEMPS TO MONITOR
        while (not_warm and not self.stop_tc.is_set()) :
            for t in all_temps :
                temp = self.data_dict[f'thermometer.{t}']
                if temp < 15 : 
                    not_warm = True
                    break
                else :
                    not_warm = False            
            time.sleep(1)

        if self.stop_tc.is_set() :
            logger.info("Thermal Cycled Aborted")
        else :
            logger.info("All modules above +15C")


    def cool_down(self,set_temperature,modules) :
        # Skip cool down if thermal cyle aborted
        if self.stop_tc.is_set() :
            logger.info("Thermal Cycled Aborted - cool down skipped")
            return False

        logger.debug(f'{self.name}:cool_down({set_temperature},{modules}) called')

        # Send alert to turn up N2 flow to max
        logger.info("Waiting to turn N_2/Dry-Air flow to max")
        pub.sendMessage('alert',message="Start Purge: Set N2/Dry-Air flow to max")

        # Check N2 flow reaches max
        while (not self.stop_tc.is_set()) :
            if self.data_dict['gas_flow'] > 8.0 : 
                break
            time.sleep(1)
        
        if self.stop_tc.is_set() :
            logger.info("cool down aborted")
            return False
        else :
            logger.info("gas_flow is set > 8.0 LPM")

        # Set Chiller to max(set_temperatre,-20C) & pump speed 5
        self.data_dict['chiller.set_temperature'] = max(-20.0,set_temperature)
        self.data_dict['chiller.set_pump'] = 5

        # Start cool down.  
        # Monitor dew point and wait until it reaches 5C below set-temprerature
        logger.info("Waiting for box to dry out")
        while (not self.stop_tc.is_set()) :

            # Get average Dew Point
            dp = (self.data_dict['DP.1'] + 
                    self.data_dict['DP.2']) / 2.0

            logger.info(f"Current Ave Dew Point: {dp}")
            if dp <= (set_temperature - 5.0) :       
                break
        
            time.sleep(1)

        if self.stop_tc.is_set() :
            logger.info("cool down aborted")
            return False
        else :
            logger.info("ColdJig is dry. Turn down flow to 6 LPM")
            pub.sendMessage('alert',message="Purge End: Turn down N2/Dry-Air flow to 6 LPM")

        # Wait for 1 minute to allow user to reduce flow to 6 LPM
        logger.info("Waiting 1 minute for flow to reduce...")
        time_start = time.time()  # log time now
        while (not self.stop_tc.is_set()) :
            logger.info(f'Gas Flow: {self.data_dict["gas_flow"]}')
            if (time.time() - time_start) > 60.0 :
                logger.info("1 minute wait done")
                break                  
            time.sleep(1)

        if self.stop_tc.is_set() :
            logger.info("cool down aborted")
            return False
        else :        
            logger.info(f'N2 flow set to {self.data_dict["gas_flow"]} LPM')

        # Wait 1 minutes to let Dew Points settle
        logger.info("Waiting 1 min to let Dew Points settle")
        time.sleep(60)
        
        # Check DP again 
        av_dp = (self.data_dict['DP.1'] + self.data_dict['DP.2']) * 0.5
        if av_dp > (set_temperature - 5.0) :
            logger.warning(f'Dew point is above {set_temperature -5.0}C after turning down N2 FLow.')
            logger.warning(f'Aborting setup')
            return False

        # For each tested module:
        # - set Peltiers to 30V/3A & COOL
        # - set PID target temperature to set_temperature
        self.data_dict['peltier.set_mode'] = 'COOL'
        for m in modules :
            self.data_dict[f'peltier.set_current.{m}'] = 3.0
            self.data_dict[f'peltier.set_volt.{m}'] = 30.0
            self.data_dict[f'PID.Chuck{m}.targetTemperature'] = set_temperature

        # Let PID control Peltiers' currents
        for m in modules :
            self.data_dict[f'PID.Chuck{m}.set_mode'] = 'INITIAL_COOLING'

        # Wait until all tested modules within 0.5C of set_temperature
        all_cold = {i:False for i in modules} # Keep track of cold modules 
        while(all(all_cold.values()) == False and not self.stop_tc.is_set()) :
            logger.info(f'all_cold {all_cold}')
            for m in modules :
                temp = self.data_dict[f'thermometer.VC{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp <= (set_temperature+0.5) : 
                    all_cold[m] = True
            time.sleep(1)

        if self.stop_tc.is_set() : 
            logger.info("Cool down aborted")
            return False
        else :
            logger.info(f"All modules are at {set_temperature}")

        return True


    def warm_up(self,set_temperature,modules) :
        if self.stop_tc.is_set() : 
            logger.info("Thermal Cycled Aborted - Warm up skipped")
            return 

        logger.debug(f'{self.name}:warm_up({set_temperature},{modules}) called')

        # # Turn Peltiers off
        # for m in modules : 
        #     self.data_dict[f'peltier.set_volt.{m}'] = 0.0
        #     self.data_dict[f'peltier.set_current.{m}'] = 0.0  

        # Set Peltiers to HEAT 
        self.data_dict['peltier.set_mode'] = 'HEAT'

        # Let PID control warm up
        # Set PID target temperature
        for m in modules :
            self.data_dict[f'PID.Chuck{m}.set_mode'] = 'FINAL_HEATING'
            self.data_dict[f'PID.Chuck{m}.targetTemperature'] = set_temperature

        # Set chiller to set_temperature
        self.data_dict['chiller.set_temperature'] = set_temperature

        # Set module to set_temperature
        # self.data_dict['module.set_temperature'] = set_temperature
        # WILL NEED TO DEFINE DATA DICT KEY FOR MODULE TEMPERATURE

        # Wait until modules reach 0.5C of set_temperature
        # There also needs to be a timeout here...
        all_warm = {i:False for i in modules} # Keep track of warm modules
        while(all(all_warm.values()) == False and not self.stop_tc.is_set()) :
            logger.info(f'all_warm: {all_warm}')
            for m in modules :
                temp = self.data_dict[f'thermometer.VC{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp >= (set_temperature-0.5) : 
                    all_warm[m] = True            
            time.sleep(1)

        if self.stop_tc.is_set() : 
            logger.info("Thermal cycle aborted")
        else :
            logger.info(f'Modules have reached {set_temperature}C')

        return 

    def fast_heat(self,set_temperature,modules) :
        if self.stop_tc.is_set() : 
            logger.info("Thermal Cycled Aborted - Fast Heat skipped")
            return 

        logger.debug(f'{self.name}:fast_heat({set_temperature},{modules}) called')

        # adjust coolant temp if chucks have to be >+55C above coolant
        if (set_temperature - self.data_dict['chiller.set_temperature']) >= 55.0 :
            # Raise coolant temp to reduce temperature difference to 55C
            logger.debug(f'set_temperature:{set_temperature} >= 55C above chiller temperature')
            self.data_dict['chiller.set_temperature'] = set_temperature - 55.0 
            logger.debug(f'Setting new chiller temperature: {self.data_dict["chiller.set_temperature"]}')

 
        # # Set chiller to -15C if current chiller is at <= -20C
        # # otherwise leave 
        # self.data_dict['chiller.set_temperature'] = -15.0

        # Change Peltier mode 
        self.data_dict['peltier.set_mode'] = 'HEAT'

        # Set Peltier Voltage/Current   
        # and set PID target temperarture 
        for m in modules :
            self.data_dict[f'peltier.set_current.{m}'] = 3.0
            self.data_dict[f'peltier.set_volt.{m}'] = 30.0
            self.data_dict[f'PID.Chuck{m}.targetTemperature'] = set_temperature

        # Set PID to HEATING MODE - PID now control Peltiers' currents
        for m in modules :
            self.data_dict[f'PID.Chuck{m}.set_mode'] = 'INTERMEDIATE_HEATING'

        # Set module temperature 
        # JUST A PLACE HOLDER NOW...NOT SURE I NEED IT 

        # Wait for modules to reach 0.5C of set_temperature
        all_warm = {i:False for i in modules} # Keep track of warm modules
        while(all(all_warm.values()) == False and not self.stop_tc.is_set()) :
            logger.info(f'still warming: {all_warm}')
            for m in modules :
                temp = self.data_dict[f'thermometer.VC{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp >= (set_temperature-0.5) : 
                    all_warm[m] = True            
            time.sleep(1)        

        if self.stop_tc.is_set() : 
            logger.info("Thermal cycle aborted")
        else :
            logger.info(f'Modules have reached {set_temperature}')

        return

    def fast_cool(self,set_temperature,modules) :
        if self.stop_tc.is_set() : 
            logger.info("Thermal Cycled Aborted - Fast Cool aborted")
            return 

        logger.debug(f'{self.name}:fast_cool({set_temperature},{modules}) called')

        # Set chiller to -20C
        self.data_dict['chiller.set_temperature'] = -20.0

        # Change Peltier mode 
        self.data_dict['peltier.set_mode'] = 'COOL'
    
        # Set Peltier Voltage/Current    
        # and set PID target temperature
        for m in modules :
            self.data_dict[f'peltier.set_current.{m}'] = 3.0
            self.data_dict[f'peltier.set_volt.{m}'] = 30.0
            self.data_dict[f'PID.Chuck{m}.targetTemperature'] = set_temperature

        # Set PID to HEATING MODE - PID now control Peltiers' currents
        for m in modules :
            self.data_dict[f'PID.Chuck{m}.set_mode'] = 'INTERMEDIATE_COOLING'

        # Set module temperature 
        # JUST A PLACE HOLDER NOW...NOT SURE I NEED IT 

        # Wait for modules to reach 0.5C of set_temperature
        all_cold = {i:False for i in modules} # Keep track of cold modules 
        while(all(all_cold.values()) == False and not self.stop_tc.is_set()) :
            logger.info(f'all_cold: {all_cold}')
            for m in modules :
                temp = self.data_dict[f'thermometer.VC{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp <= (set_temperature+0.5) : 
                    all_cold[m] = True
            time.sleep(1)        

        if self.stop_tc.is_set() : 
            logger.info("Thermal cycle aborted")
        else :
            logger.info(f'Modules have reached {set_temperature}')

        return


    def cold_characterisation(self,modules) :
        if self.stop_tc.is_set() : 
            logger.info("Thermal Cycled Aborted - Cold characterisation skipped")
            return 

        logger.debug(f'{self.name}:cold_characterisation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('COLD_TEST')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        
    #     time.sleep(60)  # Just wait for 1 second 
        super().cold_characterisation(modules)
        return True
    
    
    def warm_characterisation(self,modules) :
        if self.stop_tc.is_set() : 
            logger.info("Thermal Cycled Aborted - Warm characterisation skipped")
            return 

        logger.debug(f'{self.name}:warm_characterisation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('WARM_TEST')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        # time.sleep(60) # Just wait for 1 second 
        super().warm_characterisation(modules)
        return True


    def warm_confirmation(self,modules) :
        if self.stop_tc.is_set() : 
            logger.info("Thermal Cycled Aborted - Warm confirmation skipped")
            return 


        logger.debug(f'{self.name}:warm_confirmation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('WARM_CONFIRMATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        # time.sleep(60)  # Just wait for 1 second 
        super().warm_confirmation(modules)
        return True


    def cold_confirmation(self,modules) :
        if self.stop_tc.is_set() : 
            logger.info("Thermal Cycled Aborted - Cold confirmation skipped")
            return 

        logger.debug(f'{self.name}:cold_confirmation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('COLD_CONFIRMATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        # time.sleep(60)  # Just wait for 1 second 
        super().cold_confirmation(modules)
        return True



    def hv_stabilisation(self,modules) :
        if self.stop_tc.is_set() : 
            logger.info("Thermal Cycled Aborted - HV stabilisation skipped")
            return 

        logger.debug(f'{self.name}:hv_stabilisation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('HV_STABILISATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        # time.sleep(60)   # Just wait for 1 second 
        super().hv_stabilisation(modules)
        return True



    def wait_for_PID_idle(self,modules) :
        """
        wait for PID mode to be set to IDLE 
        Stops if tc_set is set
        """
        all_pid_idle = {i:False for i in modules} # Keep PIDs that are IDLE
        while(all(all_pid_idle.values()) == False and not self.stop_tc.is_set()) :
            logger.info(f'all_pid_idle: {all_pid_idle}')
            for m in modules :
                all_pid_idle[m] = self.data_dict[f'PID.Chuck{m}.mode'] == "IDLE" 
            time.sleep(1)        

        logger.debug("All modules PID are IDLE")


