from . import ThermalCycle
import logging
import time

logger = logging.getLogger(__name__)

#from pubsub import pub
# the defined messages topics are:
# - 'warning' : eg: "humidity increasing", "N2 flow low"
# - 'error' : eg: "No response from chiller", "can't connect to INFLUX"
# - 'alert' : "Turn down N2 flow", "Safe to open lid", "module QC completed"
# - 'danger' : "condensation on module", "interlock fired","Peltier overheating"

class endCap_TC(ThermalCycle.ThermalCycle) :
    """
    Impementation of the Thermal Cycling stages for the 
    Warwick ColdJig
    """

    def __init__(self,data_dict,stop_tc) -> None:
        super().__init__("endCap_TC",data_dict,stop_tc)

    def setup(self) :
        logger.info(f'{self.name}:Setup Called - empty for now')
        time.sleep(1)  # Just wait for 1 second 

    def cool_down(self,modules):
        if not self.stop_tc.is_set():
            self.data_dict['PID.set_mode'] = 'INITIAL_COOLING'

            logger.debug(f'{self.name}:cool_down({modules}) called')
            tempList=[]
            while( (sum(i < (self.data_dict[f'PID.Chuck1.targetTemperature']+0.2) for i in tempList)<60)):
                logging.debug(f'targetTemperature: {self.data_dict[f"PID.Chuck1.targetTemperature"]}')
                time.sleep(1)
                tempList.append(self.data_dict[f'thermometer.C1'])
        return

    def fast_heat(self,set_temperature,modules) :
        if not self.stop_tc.is_set():
            self.data_dict['PID.set_mode'] = 'INTERMEDIATE_HEATING'

            logger.debug(f'{self.name}:fast_heat({set_temperature},{modules}) called')
            tempList=[]
            while( (sum(i > (self.data_dict[f'PID.Chuck1.targetTemperature']-0.2) for i in tempList)<60)):
                logging.debug(f'targetTemperature: {self.data_dict[f"PID.Chuck1.targetTemperature"]}')
                time.sleep(1)
                tempList.append(self.data_dict[f'thermometer.C1'])
        return

    def fast_cool(self,set_temperature,modules) :
        if not self.stop_tc.is_set():
            self.data_dict['PID.set_mode'] = 'INTERMEDIATE_COOLING'

            logger.debug(f'{self.name}:fast_cool({set_temperature},{modules}) called')
            tempList=[]
            while( (sum(i < (self.data_dict[f'PID.Chuck1.targetTemperature']+0.2) for i in tempList)<60)):
                logging.debug(f'targetTemperature: {self.data_dict[f"PID.Chuck1.targetTemperature"]}')
                time.sleep(1)
                tempList.append(self.data_dict[f'thermometer.C1'])
        return

    def warm_up(self,set_temperature,modules) :
        if not self.stop_tc.is_set():
            self.data_dict['PID.set_mode'] = 'FINAL_HEATING'
            logger.debug(f'{self.name}:warm_up({set_temperature},{modules}) called')
            tempList=[]
            while( (sum(i > (self.data_dict[f'PID.Chuck1.targetTemperature']-0.2) for i in tempList)<60)):
                logging.debug(f'targetTemperature: {self.data_dict[f"PID.Chuck1.targetTemperature"]}')
                time.sleep(1)
                tempList.append(self.data_dict[f'thermometer.C1'])
        return

    def finish(self) :
        logger.debug(f'{self.name}:finish() called')
        time.sleep(1)

    def cold_characterisation(self,modules) :
        logger.debug(f'{self.name}:cold_characterisation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('COLD_TEST')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        
        time.sleep(1)  # Just wait for 1 second 

    
    def warm_characterisation(self,modules) :
        logger.debug(f'{self.name}:warm_characterisation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('WARM_TEST')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        time.sleep(1) # Just wait for 1 second 


    def warm_confirmation(self,modules) :
        logger.debug(f'{self.name}:warm_confirmation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('WARM_CONFIRMATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        time.sleep(1)  # Just wait for 1 second 

    def cold_confirmation(self,modules) :
        logger.debug(f'{self.name}:cold_confirmation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('COLD_CONFIRMATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        time.sleep(1)  # Just wait for 1 second 


    def hv_stabilisation(self,modules) :
        logger.debug(f'{self.name}:hv_stabilisation({modules}) called')

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('HV_STABILISATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
        time.sleep(1)   # Just wait for 1 second 

