from . import ThermalCycle
import logging
import time

logger = logging.getLogger(__name__)



class BNL_TC(ThermalCycle.ThermalCycle) :
    """
    Impementation of the Thermal Cycling stages for the 
    BNL ColdJig
    """

    def __init__(self,data_dict,stop_tc,Influx) -> None:
        super().__init__("BNL_TC",data_dict,stop_tc,Influx)
        
    def setup(self,modules) :
        logger.info(f'{self.name}:Setup Called')
        super().setup(modules)


    def finish(self) :        
        # Set chiller to +20C
        self.data_dict['chiller.set_temperature'] = 20.0

        # Wait until all internal temperatures are above +15C
        not_warm = True
        all_temps = ['thermometer.C0','thermometer.C1','thermometer.C2','thermometer.C3']  ## MAKE LIST OF TEMPS TO MONITOR
        while (not_warm and not self.stop_tc.is_set()) :
            for t in all_temps :
                temp = self.data_dict[t]
                if temp < 18 : 
                    not_warm = True
                    break
                else :
                    not_warm = False            
            time.sleep(1)

        if self.stop_tc.is_set() :
            logger.info("Thermal Cycled Aborted")
        else :
            logger.info("All modules above +18C")


    def cool_down(self,tc_cool_temp,modules):
    
        # Send alert to turn up N2 flow to max
        #logger.info("Waiting to turn N_2 flow to max")

        # # NEED A WAY TO POP UP ALERT BOX ON GUI SCREEN

        # Check Dry air flow reaches max
        while (not self.stop_tc.is_set()) :
            if self.data_dict['gas_flow'] > 1.5 : 
                break
            time.sleep(1)
        
        if self.stop_tc.is_set() :
            logger.info("cool down aborted")
            return False
        #else :
        #    logger.info("gas_flow is set > 8.0 LPM")

        # Set Chiller to -45C
        self.data_dict['chiller.set_temperature'] = float(tc_cool_temp)+ 5.0

        # Start cool down.  
        # Monitor dew point and wait until it reaches -45C
        logger.info("Waiting for box to dry out")
        logger.info(f'{modules}')
        while (not self.stop_tc.is_set()) :
            # Get average Dew Point
            dp = (self.data_dict['DP.1'] + self.data_dict['DP.2']) / 2.0

            logger.info(f"Current Ave Dew Point: {dp}")
            if dp <= -50.0 :
                break
            time.sleep(1)
        if self.stop_tc.is_set() :
            logger.info("cool down aborted")
            return False
        
        # Wait until all modules at or below -35C
        all_cold = False 
        while(all_cold == False and not self.stop_tc.is_set()) :
            logger.info(f'all_cold {all_cold}')
            for m in modules :
                temp = self.data_dict[f'thermometer.C{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp > tc_cool_temp : 
                    all_cold = False
                    break
                else :
                    all_cold = True
            
            time.sleep(1)

        if self.stop_tc.is_set() : 
            logger.info("Cool down aborted")
            return False
        else :
            logger.info("All Chucks are at or cooler than {tc_cool_temp}C")

        return True

    def warm_up(self,set_temperature,modules) :

        # Set chiller to set_temperature
        self.data_dict['chiller.set_temperature'] = set_temperature

        # Set module to set_temperature
        # self.data_dict['module.set_temperature'] = set_temperature
        # WILL NEED TO DEFINE DATA DICT KEY FOR MODULE TEMPERATURE

        # Wait until modules reach set_temperature +/-1C
        # There also needs to be a timeout here...
        still_warming = True
        while(still_warming and not self.stop_tc.is_set()) :
            logger.info(f'still_warming: {still_warming}')
            for m in modules :
                temp = self.data_dict[f'thermometer.C{m}']
                logger.info(f"Module {m}, temp={temp} ")
                if temp < set_temperature -3.0 : 
                    still_warming = True
                    break
                else :
                    still_warming = False
            
            time.sleep(1)

        if self.stop_tc.is_set() : 
            logger.info("Thermal cycle aborted")
        else :
            logger.info(f'Modules have reached {set_temperature}C')

        return 

    def fast_heat(self,set_temperature,modules) :
        # JUST A PLACE HOLDER NOW...NOT SURE I NEED IT
        self.warm_up(set_temperature,modules)
        return

    def fast_cool(self,set_temperature,modules) :
        # JUST A PLACE HOLDER NOW...NOT SURE I NEED IT
       self.cool_down(set_temperature, modules)
       return

    def cold_characterisation(self,modules):
        super().cold_characterisation(modules)
        return True

    def warm_characterisation(self,modules) :
        super().warm_characterisation(modules)
        return True

    def warm_confirmation(self,modules) :
        super().warm_confirmation(modules)
        return True

    def cold_confirmation(self,modules) :
        super().cold_confirmation(modules)
        return True

    def hv_stabilisation(self,modules) :
        super().hv_stabilisation(modules)
        return True

    def stop_thermalcycle_routine(self,modules): 
        self.data_dict['chiller.set_temperature'] = 20.0
        for i in modules:
            self.data_dict[f'HV.set_voltage_{i}'] = 0.0
        return True
