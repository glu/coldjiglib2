# Factory method to create thermal-cycle (tc) objects
import importlib
import logging

def create(tc_name) :
    """
    Dynamically load the thermal-cycle class from the Thermal Cycle package
    """
    tc_module = importlib.import_module('.' + tc_name,package='ThermalCycle')
    tc_class = getattr(tc_module,tc_name)

    return tc_class
