from abc import ABC, abstractmethod
import time
from pubsub import pub
import logging
from ITSDAQ_COMM import Influx_coldjig
logger = logging.getLogger(__name__)

class ThermalCycle(ABC) :
    """
    Definition of the Thermal Cycle Interface (Abstract Base Class)

    The module QC (aka thermal-cycling) stages from start to end are:
    1. Setup (run any checks before starting cooldown)
    2. Cooldown (initial cooldown and drying ColdJig interior)
    3. Cold module characterisation tests
    4. Thermal Cycles (repeat N times)
      - Fast warmup
      - Warn confirmation test
      - Fast cooldown
      - Cold confirmation test or Cold characterisation test for last cycle
    5. Warm up to +20C
    6. Warm characterisation test
    7. 2hr HV stabilisation test
    8. Finish (power down modules, warm up until safe to open)

    Each mandatory function in the interface represents a thermal-cycling stage
    """
    def __init__(self,name,data_dict,stop_tc,Influx) :
        """ 
        Constructor for ThermalCycle Base class
        """
        # Plain name such as Warwick_TC, BNL_TC
        logger.info(f'initializing thermal_cycling')
        self.name = name
        self.Influx_communication_handler=Influx_coldjig.Coldjig_Influx_COMM(Influx)
        #self.Influx_communication_handler= none
        # Threading event to signal if thermal cycling has to abort
        #logger.info(f'stop_tc is {stop_tc}')
        self.stop_tc = stop_tc
 
        self.data_dict = data_dict # reference to global data_dict

        # Plain string representing which stage is running
        self.stage = "Not running"
        #logger.info(f'done with initializing ABC thermal_cycling')

        
    # --- START / FINISH  ---

    @abstractmethod
    def setup(self, modules) :
        """
        Called at start of thermal-cycle
        - runs any setup and checks before starting cool down
        """
        self.Init_modules(modules)
        return


    @abstractmethod
    def finish(self) :
        """
        Warm up coldjig and power down all modules, peltiers
        Monitor until safe to open
        """
        pass 


    # --- COOL DOWN / WARM UP  ---

    @abstractmethod 
    def cool_down(self,set_temperature,modules) :
        """
        - Starts initial cool down from room-temperature to cold
        - Function exits when set temperarture has been reached or forced to stop
        """
        pass

    @abstractmethod
    def warm_up(self,set_temperature,modules) :
        """
        Warm up ColdJig to new set_temperature
        """
        pass
    
    # --- FAST HEAT  / FAST COOL ---

    @abstractmethod
    def fast_heat(self,set_temperature,modules) :
        """
        Engage fast heat up as part of thermal cycling stage
        - This involves peltiers for EC and Warwick ColdJigs
        - EC also engages valves to bypass coolant flow
        """
        pass

    @abstractmethod
    def fast_cool(self,set_temperature,modules) :
        """ 
        Engage fast cool down as part of thermal cycling stage
        - This involves peltiers for EC and Warwick ColdJigs
        - EC also engages valves to open coolant flow
        """
        pass

    # --- ITSDAQ MODULE TESTING ---
    @abstractmethod
    def cold_characterisation(self,modules) :
        start_time = time.time_ns()
        self.Influx_communication_handler.RunColdCharacterisation(modules)
        status = self.Influx_communication_handler.is_command_complete(start_time,"RUN_COLD_CHARACTERISATION")
        while (status is None and not self.stop_tc.is_set()):
            logger.info('Waiting for ITSDAQ')
            time.sleep(5)
            status = self.Influx_communication_handler.is_command_complete(start_time,"RUN_COLD_CHARACTERISATION")
            
        logger.debug(f'{status["DATA"]}')
        if status['ERROR'] is not None :
            logger.error(f'{status["ERROR"]}')
            pub.sendMessage('error',message=f'Error reading {status["ERROR"]}')
        #pub.sendMessage('data',message=f'{status["DATA"]}')
        return None
        
        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('COLD_TEST')...OR SOMETHING LIKETHAT
        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED
        # EMPTY FOR NOW....

    @abstractmethod
    def warm_characterisation(self,modules) :
        start_time = time.time_ns()
        self.Influx_communication_handler.RunWarmCharacterisation(modules)
        status = self.Influx_communication_handler.is_command_complete(start_time,"RUN_WARM_CHARACTERISATION")
        while (status is None and not self.stop_tc.is_set()):
            logger.info('Waiting for ITSDAQ')
            time.sleep(5)
            status = self.Influx_communication_handler.is_command_complete(start_time,"RUN_WARM_CHARACTERISATION")
            
        logger.debug(f'{status["DATA"]}')
        if status['ERROR'] is not None :
            logger.error(f'{status["ERROR"]}')
            pub.sendMessage('error',message=f'Error reading {status["ERROR"]}')
        #pub.sendMessage('data',message=f'{status["DATA"]}')
        return None
        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('WARM_TEST')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....

    @abstractmethod
    def warm_confirmation(self,modules) :
        start_time = time.time_ns()
        self.Influx_communication_handler.RunWarmConfirmation(modules)
        status = self.Influx_communication_handler.is_command_complete(start_time,"RUN_WARM_CONFIRMATION")
        while (status is None and not self.stop_tc.is_set()):
            logger.info('Waiting for ITSDAQ')
            time.sleep(5)
            status = self.Influx_communication_handler.is_command_complete(start_time,"RUN_WARM_CONFIRMATION")
            
        logger.debug(f'{status["DATA"]}')
        if status['ERROR'] is not None :
            logger.error(f'{status["ERROR"]}')
            pub.sendMessage('error',message=f'Error reading {status["ERROR"]}')
        #pub.sendMessage('data',message=f'{status["DATA"]}')
        return None

        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('WARM_CONFIRMATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
    
    @abstractmethod
    def cold_confirmation(self,modules) :
        start_time = time.time_ns()
        self.Influx_communication_handler.RunColdConfirmation(modules)
        status = self.Influx_communication_handler.is_command_complete(start_time,"RUN_COLD_CONFIRMATION")
        while (status is None and not self.stop_tc.is_set()):
            logger.info('Waiting for ITSDAQ')
            time.sleep(5)
            status = self.Influx_communication_handler.is_command_complete(start_time,"RUN_COLD_CONFIRMATION")

        logger.debug(f'{status["DATA"]}')
        if status['ERROR'] is not None :
            logger.error(f'{status["ERROR"]}')
            pub.sendMessage('error',message=f'Error reading {status["ERROR"]}')
        #pub.sendMessage('data',message=f'{status["DATA"]}')
        return None
        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('COLD_CONFIRMATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW....
    
    @abstractmethod
    def hv_stabilisation(self,modules) :
        start_time = time.time_ns()
        self.Influx_communication_handler.RunHVStability(modules)
        status = self.Influx_communication_handler.is_command_complete(start_time,"HV_STABILISATION")
        while (status is None and not self.stop_tc.is_set()):
            logger.info('Waiting for ITSDAQ')
            time.sleep(5)
            status = self.Influx_communication_handler.is_command_complete(start_time,"HV_STABILISATION")

        logger.debug(f'{status["DATA"]}')
        if status['ERROR'] is not None :
            logger.error(f'{status["ERROR"]}')
            pub.sendMessage('error',message=f'Error reading {status["ERROR"]}')
        #pub.sendMessage('data',message=f'{status["DATA"]}')
        return None
        # ITSDAQ-COLDJIG PROTOCOL HERE
        # NEED TO WRITE TO INFLUX DB...
        # INFLUXDB.WRITE('HV_STABILISATION')...OR SOMETHING LIKETHAT

        # WAIT FOR ITSDAQ TO COMPLETE
        # WHILE():
        #   GET LATEST ENTRIES FROM COMM
        #   KEEP CHECKING UNTIL TEST FINISHED

        # EMPTY FOR NOW...

    def Init_modules(self, modules):
        logger.info(f'in TC with {modules}')
        start_time = time.time_ns()
        self.Influx_communication_handler.Init_modules(modules)
        #logger.info(f'getting status')
        status = self.Influx_communication_handler.is_command_complete(start_time,"INIT_MODULES")
        while (status is None and not self.stop_tc.is_set()):
            logger.info('Waiting for ITSDAQ')
            time.sleep(5)
            status = self.Influx_communication_handler.is_command_complete(start_time,"INIT_MODULES")

        logger.debug(f'{status["DATA"]}')
        if status['ERROR'] is not None :
            logger.error(f'{status["ERROR"]}')
            pub.sendMessage('error',message=f'Error reading {status["ERROR"]}')
        #pub.sendMessage('data',message=f'{status["DATA"]}')
        return None
