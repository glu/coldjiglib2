# Short script to create a softlink to the correct 
# HW INI file based on R-Pi's serial number
import logging
logger = logging.getLogger(__name__)

import configparser
import os

# Function copied from
# https://www.raspberrypi-spy.co.uk/2012/09/getting-your-raspberry-pi-serial-number-using-python/
def getserial():
  # Extract serial from cpuinfo file
  cpuserial = "0000000000000000"
  try:
    f = open('/proc/cpuinfo','r')
    for line in f:
      if line[0:6]=='Serial':
        cpuserial = line[10:26]
    f.close()
  except:
    cpuserial = "ERROR"
 
  return cpuserial


def read_rpi_ini(ini_filename) :
    """
    Read the R-Pi INI file and parse it
    """
    parser = configparser.ConfigParser()
    parser.optionxform = str
    parser.read(ini_filename)

    # Get all the items in RPI section
    rpi_items = parser.items("RPI")

    # Pack into a dictionary
    rpi_dict = { i[1]:i[0] for i in rpi_items }
 
    return rpi_dict

def set_soft_link(base_path,link_from) :
  """
  Create softlink from link_from to HW.INI
  """
  src = link_from + ".ini"
  dst = os.path.join(base_path,"hw.ini")

  # Remove existing softlink
  if os.path.exists(dst) :
    os.remove(dst)

  logging.info(f'Create softlimk: {dst} --> {src} ')

  os.symlink(src,dst)


if __name__ == "__main__" : 

  logging.basicConfig(level=logging.INFO,format='%(asctime)s %(name)s %(levelname)s:%(message)s')
  base_path = "configs/UK_China_Barrel"

  serial = getserial()
  logging.info(f'RPi Serial: {serial}')
  
  rpi_dict = read_rpi_ini("configs/UK_China_Barrel/rpi.ini")
  institute = rpi_dict[serial]
  logging.info(f'Institute: {institute}')

  set_soft_link(base_path,institute)
