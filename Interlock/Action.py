# Abstract base class to actions taken when the interlock fires

from abc import ABC, abstractmethod
import logging

logger = logging.getLogger(__name__)

class Action(ABC) :

    def __init__(self,name="UNKNOWN_ACTION") -> None:
        super().__init__()
        self.name = name  # simple string to ID   
                          # which action type is loaded
                          # when subclassed
        logger.info(f'{self.name} Interlock Action created')

    @abstractmethod
    def rh_high(self,data_dict,idle) :
        """
        Abstract method to define action to take 
        when relative humidity is high
        """
        pass 

    @abstractmethod
    def temp_excursion(self,data_dict) :
        """
        Abstract method to define action to take
        when high temperature detected
        """
        pass

    @abstractmethod
    def dp_high(self,data_dict,idle) :
        """
        Abstract method to define action to take
        when dew point is high
        """
        pass

    @abstractmethod
    def no_air(self,data_dict,idle) :
        """
        Abstract method to define action to take
        when zero dry-air/N2 flow detected 
        """
        pass

    @abstractmethod
    def lid_open(self,data_dict,idle) :
        """
        Abstract method to define action to take
        when open lid detected
        """
        pass

    def action(self,interlock,status,data_dict) :
        """
        From the interlock bits and status, determine
        action to take, if any
        """

        idle = (status == "IDLE")

        # Temperature Excursion
        if interlock.temperature == False :
            self.temp_excursion(data_dict)

        # Dew Point High
        if interlock.dp == False :
            self.dp_high(data_dict,idle)

        # RH high 
        if interlock.rh == False :
            self.rh_high(data_dict,idle)

        # Lid Open
        if interlock.lid == False :
            self.lid_open(data_dict,idle)

        # No dry-air/N2
        if interlock.air == False :
            self.no_air(data_dict,idle)
