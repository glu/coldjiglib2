# Factory method to create Interlock Action Objects
import importlib

def create(ia_name) :
    """
    Dynamically load the Interlock Action class from the Interlock package
    """
    ia_module = importlib.import_module('.' + ia_name,package='Interlock')
    ia_class = getattr(ia_module,ia_name)

    return ia_class
