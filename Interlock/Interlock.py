
# Named ntuple to store interlock bits
import collections
Interlock_Bits = collections.namedtuple('Interlock_Bits',
                                        'rh temperature dp air lid')

from pubsub import pub
# the defined messages topics are:
# - 'warning' : eg: "humidity increasing", "N2 flow low"
# - 'error' : eg: "No response from chiller", "can't connect to INFLUX"
# - 'alert' : "Turn down N2 flow", "Safe to open lid", "module QC completed"
# - 'danger' : "condensation on module", "interlock fired","Peltier overheating"

import re

import logging
logger = logging.getLogger(__name__)

class InterlockError(Exception) :
    """
    Exceptions raised when interlock condition fails

    Attributes:
        interlock: interlock that failed
        message: plain english explanation of failed interlock conditon

    """

    def __init__(self,interlock_bit,message) -> None:
        self.interlock_bit = interlock_bit
        self.message = message
                 

class Interlock :
    """
    Implementation of the ColdJig Interlock
    - Interlock is TRUE, all parameters are within bounds, FALSE otherwise
    - several interlock conditions are evaluated
    - each condition implemented as a function
    - a function returns TRUE if the reading is within bounds, FALSE otherwise
    - a single FALSE of any interlock condition renders entire interlock FALSE
    """

    def __init__(self,data_dict,gas_flow_min) :
        self.data_dict = data_dict
        self.gas_flow_minimum = gas_flow_min

        self.status = "Interlock not running yet"
        self.state = True 

        # Sift data_dict keys to find
        # ... all thermometers
        self.all_thermometers = [a for a in self.data_dict.keys() \
                                        if a.startswith('thermometer')]

        # ... all humidity sensors 
        regex = r"^RH\.\d$"
        self.all_humidity = [a for a in self.data_dict.keys() \
                                        if re.search(regex,a)]

        # ... all Dew-Points 
        self.all_DP = [a for a in self.data_dict.keys() \
                                        if a.startswith('DP')]

        # ... all lid-switches
        self.all_lid_switches = [a for a in self.data_dict.keys() \
                                        if a.startswith('Lid')]


        #.... all gas-flow sensors
        self.all_gas_flow = [a for a in self.data_dict.keys() \
                                        if a.startswith('gas_flow')]


        # check for any missing sensors and send a warning
        if not self.all_thermometers :
            pub.sendMessage('warning',message="No thermometers defined. Temperature interlock disabled")
            logger.info("No thermometers defined. Temperature interlock disabled")

        if not self.all_humidity :
            pub.sendMessage('warning',message="No humidity sensors defined. Humidity interlock disabled")
            logger.info("No humidity sensors defined. Humidity interlock disabled")

        if not self.all_DP :
            pub.sendMessage('warning',message="No Dew Point sensors defined. Dew Point interlock disabled")
            logger.info("No Dew Point sensors defined. Dew Point interlock disabled")

        if not self.all_lid_switches :
            logger.info("No lid switches defined. Dew Point interlock disabled")

        if not self.all_gas_flow :
            pub.sendMessage('warning',message="No gas flow sensors defined. Gas flow interlock disabled")
            logger.info("No gas flow sensors defined. Gas flow interlock disabled")


    def eval(self) :
        """
        Implementation of the interlock code for ColdJig.
        For this initial version, the interlock limits are hardcoded. It is 
        planned that these limits to be programmable and set via an external 
        configuration file.
        """

        max_dp = self.max_dp()

        rh_ok,rh_status = self.rh_eval()   
        temp_ok,temp_status,dp_ok,dp_status = self.temperature_eval(max_dp)
        air_ok,air_status = self.air_flow_eval()
        lid_ok,lid_status = self.lid_eval()

        interlock_ok = rh_ok & temp_ok & dp_ok & air_ok & lid_ok 

        # Update data_dict
        self.data_dict['interlock.rh'] = rh_ok
        self.data_dict['interlock.temperature'] = temp_ok
        self.data_dict['interlock.dp'] = dp_ok
        self.data_dict['interlock.air'] = air_ok
        self.data_dict['interlock.lid'] = lid_ok

        if interlock_ok :
            self.status = "Interlock running: All OK"
        else :
            interlock_bit = Interlock_Bits(rh=rh_ok, temperature=temp_ok, 
                                        dp=dp_ok, air=air_ok, lid=lid_ok)
            self.status = "\n".join(
                (rh_status,temp_status,dp_status,air_status,lid_status)) 
            raise InterlockError(interlock_bit,self.status)
            
        return interlock_ok


    def temperature_eval(self,max_dp) :
        """
        Evaluate all the thermometers & check are within normal
        parameters
        """
    
        too_cold_thermometers = []
        too_hot_thermometers = []
        too_close_to_dp = []
        temp_ok = True
        temp_status = ""
        dp_ok = True
        dp_status = ""

        # skip if no thermometers defined
        if not self.all_thermometers :
            return temp_ok,temp_status,dp_ok,dp_status

        # Intitial values for min/max temperature
        min_temp = self.data_dict[self.all_thermometers[0]]
        max_temp = self.data_dict[self.all_thermometers[0]]


        for t in self.all_thermometers : 

            # Collate all thermometry data
            #therm_data[t] = self.data_dict[t]
            # COMMENTED OUT FOR NOW 

            # Update min/max temperature
            if self.data_dict[t] < min_temp :
                min_temp = self.data_dict[t]

            if self.data_dict[t] > max_temp :
                max_temp = self.data_dict[t]

            # Make list of thermometers above max allowed temperature
            if self.data_dict[t] < -50.0 :
                too_cold_thermometers.append(t)

            # Make list of thermometers below min allowed temperature
            if self.data_dict[t] > 50.0 :
                too_hot_thermometers.append(t)

            # Make list of thermometers that are within 5C of max-DP
            if self.data_dict[t] < (max_dp + 5.0) :
                too_close_to_dp.append(t)

        # if max temp > 50.0 - interlock failed
        if len(too_hot_thermometers) > 0 :
            temp_status += "OVER TEMP: " \
                + ",".join(too_hot_thermometers)
            temp_ok = False

        # if min temp < -50.0 - interlock failed
        if len(too_cold_thermometers) > 0 :
            temp_status = "UNDER TEMP: " \
                + ",".join(too_cold_thermometers)
            temp_ok = False

        # if min temp < min-DP + 10C - interlock failed 
        if len(too_close_to_dp) > 0 :
            dp_status = "DP : " \
                + ",".join(too_close_to_dp)
            dp_ok = False

        # Return the evaluation
        return temp_ok,temp_status,dp_ok,dp_status



    def max_dp(self) :
        """
        Find max Dew Point
        """

        # Skip if no DP sensor defined
        # return hardcoded value of -100.0
        if not self.all_DP :
            return -100.0

        # Find max DP
        max_dp = self.data_dict[self.all_DP[0]]
        for d in self.all_DP :
            dp = self.data_dict[d]
            if dp > max_dp :
                max_dp = dp

        return max_dp


    def lid_eval(self) : 
        """
        Evaluate the lid switches and confirm all are closed
        """
        # Check all lid switches are closed
        open_switch = []
        lid_ok = True
        lid_status = ""

        # Skip if no switches defined
        if not self.all_lid_switches :
            return lid_ok,lid_status


        for l in self.all_lid_switches :
            if self.data_dict[l] == 'OPEN' :
                open_switch.append(l)

        if len(open_switch) > 0 :
            lid_status = f"LID OPEN: " \
                + ",".join(open_switch)
            lid_ok = False
        ## glu
        lid_ok = True
        return lid_ok,lid_status

    def air_flow_eval(self) :
        """
        Evaluate the air/N_2 flow and check it is within limits
        """

        air_ok = True
        air_status = ""
        too_low_air = []

        # Skip if no gas flow sensor defined
        if not self.all_gas_flow :
            return air_ok,air_status

        for g in self.all_gas_flow :
            if self.data_dict[g] < self.gas_flow_minimum :
                too_low_air.append(g)

        if len(too_low_air) > 0 :
            air_status += f"N2/DRY-AIR FLOW IS TOO LOW: " \
                + ",".join(too_low_air)
            air_ok = False

        return air_ok,air_status

    def rh_eval(self) :
        # Check all RH readings > 0.0 & < 70.0

        too_low_rh = []
        too_high_rh = []

        rh_ok = True
        rh_status = ""

        # Skip if no humidity sensors installed
        if not self.all_humidity :
            return rh_ok,rh_status

        for h in self.all_humidity :
            rh = self.data_dict[h]
            if rh < 0.0 :
                too_low_rh.append(h)
            if rh > 70.0 :
                too_high_rh.append(h)

        if len(too_low_rh) > 0 :
            rh_status += f"RH LOW : " \
                + ",".join(too_low_rh)
            rh_ok = False

        if len(too_high_rh) > 0 :
            rh_status = f"RH HIGH: " \
                + ",".join(too_high_rh) 
            rh_ok = False                

        return rh_ok,rh_status

