# Implementation of interlock actions for UK/China Barrel (BNL) ColdJig

from . import Action
import logging
from pubsub import pub
import time

logger = logging.getLogger(__name__)

class BNL_Action(Action.Action) :

    def __init__(self, name="BNL_Action") -> None:
        super().__init__(name=name)

    def rh_high(self,data_dict,idle) :
        """
        Action to take when relative humidity is high
        """
        logger.info("High Relative Humidity: Taking action to avoid condensation")

        # Set Chiller to +20C
        data_dict['chiller.set_temperature'] = 20.0

        # if idle, tell user to close lid
        if idle :
            pub.sendMessage('alert',message='close lid now')

        # Tel user to set dry-air/N2 to max
        pub.sendMessage('alert',message="Set Dry-Air/N2 to MAX")

        # TELL ITSDAQ TO TURN OFF MODULE LV & HV
        # HOW TO IMPLEMENT THIS ?

        if idle :  
            # No more to do if idle
            return 

        # If not idle (ie: thermal-cycle) warm up modules ASAP
        # Set all Peltiers to 30V/3A & HEAT
        #data_dict['peltier.set_mode'] = 'HEAT'
        #for i in range(1,6) :
        #    data_dict[f'peltier.set_current.{i}'] = 3.0
        #    data_dict[f'peltier.set_volt.{i}'] = 30.0

        # Wait until chuck reaches +20C
        logger.info("Waiting for all chucks to reach +20C")
        all_chucks_warm = False
        while (not all_chucks_warm) :
            # Check all chuck temperatures
            for i in range(1,6) :
                if data_dict[f'thermometer.VC{i}'] < 20.0 :
                    break
                # got here...all chucks must > 20C
                all_chucks_warm = True 

            time.sleep(1) # Check every second

        # Modules warm, switch off Peltiers & check current goes to zero
        




    def temp_excursion(self,data_dict) :
        """
        Action to take when high temperature detected
        """
        logger.info("Temperature Excursion: Taking action to prevent damage")

        # Set Chiller to +20C
        data_dict['chiller.set_temperature'] = 20.0

        # Set all Peltiers to 0V/0A
        #for i in range(1,6) :
        #    data_dict[f'peltier.set_current.{i}'] = 0.0
        #    data_dict[f'peltier.set_volt.{i}'] = 0.0

        # TELL ITSDAQ TO TURN OFF MODULE LV & HV
        # HOW TO IMPLEMENT THIS ?

        # Wait for peltier currents to go to zero
        # NEED TO ADD MONITORING LOOP HERE...

        

    def dp_high(self,data_dict,idle) :
        """
        Action to take when dew point is high
        """
        logger.info("High Dew Point: Taking action to prevent condensation")

        # Follow same routine as rh_high
        self.rh_high(data_dict,idle)


    def no_air(self,data_dict,idle) :
        """
        Action to take when zero dry-air/N2 flow detected 
        """
        if idle :
            pass # Do nothing
        else :
            pub.sendMessage('alert',message="Turn dry-air/N2 back on immediatley")
            # WAIT 5 MINUTES FOR AIR TO RETURN
            # OTHERWISE FOLLOW RH_HIGH
            # Q: HOW TO IMPLEMENT WAIT FOR 5 MINUTES FOR INTERLOCK TO CLEAR ?

    def lid_open(self,data_dict,idle) :
        """
        Action to take when lid opened
        """
        if idle : 
            pass # Do nothing
        else :
            logger.critical('Lid opened when not idle')
            self.rh_high(data_dict,idle) # Take same action as high RH

