from . import Action
import logging
from pubsub import pub
import time

logger = logging.getLogger(__name__)

class endCap_action(Action.Action) :

    def __init__(self, name="endCap_action") -> None:
        super().__init__(name=name)

    def rh_high(self,data_dict,idle) :
        """
        Action to take when relative humidity is high
        """
        pass

    def temp_excursion(self,data_dict) :
        """
        Action to take when high temperature detected
        """
        pass

    def dp_high(self,data_dict,idle) :
        """
        Action to take when dew point is high
        """
        pass


    def no_air(self,data_dict,idle) :
        """
        Action to take when zero dry-air/N2 flow detected 
        """
        pass

    def lid_open(self,data_dict,idle) :
        """
        Action to take when lid opened
        """
        pass

