import sys
import os
sys.path.append(os.getcwd())

import time

import logging
import Hardware.ADC.DAQC2Plate
import Hardware.Flow.RSFlowSensor

logging.basicConfig(level=logging.DEBUG)
piplate = Hardware.ADC.DAQC2Plate.DAQC2Plate("PiPlate")

parameters = {'Address':'0',
                'DIN':'0,1',
                'DOUT':'0',
                'FREQ':'1'
            }

piplate.initialise(parameters)
breakpoint()

flow = Hardware.Flow.RSFlowSensor.RSFlowSensor("CoolantFlow")
flow_params = {'Input':'ADC.0.FREQ'}
flow.initialise(flow_params)

data_dict = {}
print(data_dict)

piplate.add_data_dict_keys(data_dict)
logging.info(f'PiPlate DATA DICT KEYS: {data_dict}')

flow.add_data_dict_keys(data_dict)
logging.info(f'PiPlate+Flow DATA DICT KEYS: {data_dict}')

piplate.Read(data_dict)
logging.info(f'READING PiPlate : {data_dict}')

flow.Read(data_dict)
logging.info(f'READING PiPlate+Flow : {data_dict}')

logging.info("Sleep for 10s")
time.sleep(10)

piplate.Read(data_dict)
flow.Read(data_dict)
logging.info(f'READING PiPlate+Flow: {data_dict}')


print("PIPLATE+Flow SHUTDOWN")
piplate.Shutdown()
flow.Shutdown()
