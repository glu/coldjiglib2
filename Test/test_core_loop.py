# Script to test the core loop

# This is a hack for now to import coldjiglib & Warwick_TC
import time
import sys
import os
sys.path.append(os.getcwd())

import threading
from influxdb import InfluxDBClient
import coldjiglib

# Set Hardware INI file
coldjiglib.hardware_ini_file = "configs/UK_China_Barrel/single_tc08.ini"

# Set INFLUX parameters
coldjiglib.influx_IP = '137.205.164.168'
coldjiglib.influx_PORT = 8086
coldjiglib.influx_DB = 'coldjig'
coldjiglib.influx_MEAS = 'warwick'

import logging
logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logging.captureWarnings(True)

logger = logging.getLogger(__name__)

# Function to reset data_dict
def reset_data_dict(data_dict) :

    for i in range(1,6) :
        data_dict[f'thermometer.VC{i}'] = 20.0
        data_dict[f'thermometer.CB{i}'] = 20.0
        data_dict[f'peltier.set_volt.{i}'] = 0.0
        data_dict[f'peltier.set_current.{i}'] = 0.0

    data_dict['thermometer.AIR1'] = 20.0
    data_dict['thermometer.AIR2'] = 20.0
    data_dict['peltier.set_mode'] = 'COOL'
    data_dict['chiller.set_temperature'] = 20.0
    data_dict['gas_flow'] = 6.0
    data_dict['DP.1'] = -10.0
    data_dict['DP.2'] = -10.0


# Variable to stop core_loop
core_loop_event = threading.Event()

# Run background task to set core_loop_event after 1 minute
def stop_core_loop() :
    logger.info("stop_core_loop started: Will stop core loop after 60s")
    time.sleep(60)
    core_loop_event.set()

# Run background task to create Dew Point spike after 10s 
def humidity_spike(data_dict) :
    logger.info("after 10s generate Dew Point spike")
    time.sleep(10)
    logger.info("HUMIDITY SPIKE!")
    data_dict['DP.1'] = 20.0

# Background task to create temperature spike in VC1
def temp_spike(data_dict) :
    logger.info("After 10s, make VC5 go to +70C")
    time.sleep(10)
    logger.info("VC5 TEMP SPIKE")
    data_dict['thermometer.VC5'] = 70.0


coldjiglib.start()

# Reset data_dict
reset_data_dict(coldjiglib.data_dict)

# Launch background threads
# threading.Thread(target=stop_core_loop).start()
# threading.Thread(target=humidity_spike,args=(coldjiglib.data_dict,)).start()
threading.Thread(target=temp_spike,args=(coldjiglib.data_dict,)).start()

time.sleep(30)

# # Setup INFLUX
# influx_client = InfluxDBClient(host=coldjiglib.influx_IP,port=coldjiglib.influx_PORT)

# # Run core loop
# coldjiglib.core_loop(core_loop_event,[],coldjiglib.data_dict,influx_client)

# # Close INFLUX connection
# influx_client.close()

coldjiglib.shutdown()


logger.info("END TEST")