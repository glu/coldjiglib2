from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

from ITSDAQ_COMM.ITK_INFLUX import ITK_INFLUX

# INFLUX Settings
url = '137.205.164.168'
port = '8087'

token = "VJhJamrror6M0eXXoY72GpeXXJtWRC6KbOVJOB5wTDGZaxdeOO7jX9-MCwJTqpAXgOI78nDuapM_NE9Jdje42w=="
org = "ATLAS"
bucket = "coldjig"

# Write to INFLUX directly 
def influx_direct_write() :

    influx_url = f'http://{url}:{port}'
    client = InfluxDBClient(url=influx_url,token=token)

    write_api = client.write_api(write_options=SYNCHRONOUS)

    data = "mem,host=host1 used_percent=23.43234543"
    write_api.write(bucket, org, data)

    point = Point("mem")\
        .tag("host", "host1")\
        .field("used_percent", 23.43234543)\
        .time(datetime.utcnow(), WritePrecision.NS)

    write_api.write(bucket, org, point)


def influx_dict_write() :
    influx_url = f'http://{url}:{port}'
    client = InfluxDBClient(url=influx_url,token=token)

    write_api = client.write_api(write_options=SYNCHRONOUS)

    data_dict = {'chiller.set_temperature': 20.0, 
                'chiller.temperature': 20.0, 
                'chiller.set_state': 'OFF', 
                'chiller.read_state': 'OFF', 
                'SHT_H.Left': 0.0, 
                'SHT_T.Left': 20.0, 
                'SHT_H.Right': 0.0, 
                'SHT_T.Right': 20.0, 
                'thermometer.Chuck0': 0.0, 
                'thermometer.Chuck1': 0.0, 
                'thermometer.Chuck2': 0.0, 
                'thermometer.Chuck3': 0.0, 
                'thermometer.AIR_left': 0.0, 
                'thermometer.Air_right': 0.0, 
                'Air_flow': None, 
                'Caen.set_voltge_0': 0.0, 
                'Caen.set_current_0': None, 
                'Caen.set_state_0': 'OFF', 
                'Caen.state_0': 'OFF', 
                'Caen.ovp_0': 500, 
                'Caen.current_0': 0.0, 
                'Caen.voltage_0': 0.0, 
                'Caen.set_voltge_1': 0.0, 
                'Caen.set_current_1': None, 
                'Caen.set_state_1': 'OFF', 
                'Caen.state_1': 'OFF', 
                'Caen.ovp_1': 500, 
                'Caen.current_1': 0.0, 
                'Caen.voltage_1': 0.0, 
                'Caen.set_voltge_2': 0.0, 
                'Caen.set_current_2': None, 
                'Caen.set_state_2': 'OFF', 
                'Caen.state_2': 'OFF', 
                'Caen.ovp_2': 500, 
                'Caen.current_2': 0.0, 
                'Caen.voltage_2': 0.0, 
                'Caen.set_voltge_3': 0.0, 
                'Caen.set_current_3': None, 
                'Caen.set_state_3': 'OFF', 
                'Caen.state_3': 'OFF', 
                'Caen.ovp_3': 500, 
                'Caen.current_3': 0.0, 
                'Caen.voltage_3': 0.0, 
                'LV.set_voltge_1_0': 11.0, 
                'LV.set_current_1_0': 0.0, 
                'LV.set_state_1': 'OFF', 
                'LV.ocp_1_0': 11.5, 
                'LV.current_1_0': 0.0, 
                'LV.voltage_1_0': 0.0, 
                'LV.set_voltge_1_1': 11.0, 
                'LV.set_current_1_1': 0.0, 
                'LV.ocp_1_1': 11.5, 
                'LV.current_1_1': 0.0, 
                'LV.voltage_1_1': 0.0, 
                'LV.set_voltge_2_0': 11.0, 
                'LV.set_current_2_0': 0.0, 
                'LV.set_state_2': 'OFF', 
                'LV.ocp_2_0': 11.5, 
                'LV.current_2_0': 0.0, 
                'LV.voltage_2_0': 0.0, 
                'LV.set_voltge_2_1': 11.0, 
                'LV.set_current_2_1': 0.0, 
                'LV.ocp_2_1': 11.5, 
                'LV.current_2_1': 0.0, 
                'LV.voltage_2_1': 0.0}
    
    write_api.write(bucket,org,
                    {"measurement":'warwick',
                    "tags": {"status":'TEST'},
                    "fields": data_dict, 
                    "time": datetime.utcnow()} )
                




def itk_influx_write(influx_config_file) :
    itk_influx = ITK_INFLUX(influx_config_file)

    data_dict = {'chiller.set_temperature': 20.0, 
                'chiller.temperature': 20.0, 
                'chiller.set_state': 'OFF', 
                'chiller.read_state': 'OFF', 
                'SHT_H.Left': 0.0, 
                'SHT_T.Left': 20.0, 
                'SHT_H.Right': 0.0, 
                'SHT_T.Right': 20.0, 
                'thermometer.Chuck0': 0.0, 
                'thermometer.Chuck1': 0.0, 
                'thermometer.Chuck2': 0.0, 
                'thermometer.Chuck3': 0.0, 
                'thermometer.AIR_left': 0.0, 
                'thermometer.Air_right': 0.0, 
                'Air_flow': None, 
                'Caen.set_voltge_0': 0.0, 
                'Caen.set_current_0': None, 
                'Caen.set_state_0': 'OFF', 
                'Caen.state_0': 'OFF', 
                'Caen.ovp_0': 500, 
                'Caen.current_0': 0.0, 
                'Caen.voltage_0': 0.0, 
                'Caen.set_voltge_1': 0.0, 
                'Caen.set_current_1': None, 
                'Caen.set_state_1': 'OFF', 
                'Caen.state_1': 'OFF', 
                'Caen.ovp_1': 500, 
                'Caen.current_1': 0.0, 
                'Caen.voltage_1': 0.0, 
                'Caen.set_voltge_2': 0.0, 
                'Caen.set_current_2': None, 
                'Caen.set_state_2': 'OFF', 
                'Caen.state_2': 'OFF', 
                'Caen.ovp_2': 500, 
                'Caen.current_2': 0.0, 
                'Caen.voltage_2': 0.0, 
                'Caen.set_voltge_3': 0.0, 
                'Caen.set_current_3': None, 
                'Caen.set_state_3': 'OFF', 
                'Caen.state_3': 'OFF', 
                'Caen.ovp_3': 500, 
                'Caen.current_3': 0.0, 
                'Caen.voltage_3': 0.0, 
                'LV.set_voltge_1_0': 11.0, 
                'LV.set_current_1_0': 0.0, 
                'LV.set_state_1': 'OFF', 
                'LV.ocp_1_0': 11.5, 
                'LV.current_1_0': 0.0, 
                'LV.voltage_1_0': 0.0, 
                'LV.set_voltge_1_1': 11.0, 
                'LV.set_current_1_1': 0.0, 
                'LV.ocp_1_1': 11.5, 
                'LV.current_1_1': 0.0, 
                'LV.voltage_1_1': 0.0, 
                'LV.set_voltge_2_0': 11.0, 
                'LV.set_current_2_0': 0.0, 
                'LV.set_state_2': 'OFF', 
                'LV.ocp_2_0': 11.5, 
                'LV.current_2_0': 0.0, 
                'LV.voltage_2_0': 0.0, 
                'LV.set_voltge_2_1': 11.0, 
                'LV.set_current_2_1': 0.0, 
                'LV.ocp_2_1': 11.5, 
                'LV.current_2_1': 0.0, 
                'LV.voltage_2_1': 0.0}
    
    itk_influx.WriteToDatabase(data_dict,"TEST_WRITE")


# import json
# import datetime
# import time as t

# #data_dict={'chiller.set_temperature': 20.0, 'chiller.temperature': 20.0, 'chiller.set_state': 'OFF', 'chiller.read_state': 'OFF', 'SHT_H.Left': 0.0, 'SHT_T.Left': 20.0, 'SHT_H.Right': 0.0, 'SHT_T.Right': 20.0, 'thermometer.Chuck0': 0.0, 'thermometer.Chuck1': 0.0, 'thermometer.Chuck2': 0.0, 'thermometer.Chuck3': 0.0, 'thermometer.AIR_left': 0.0, 'thermometer.Air_right': 0.0, 'Air_flow': None, 'Caen.set_voltge_0': 0.0, 'Caen.set_current_0': None, 'Caen.set_state_0': 'OFF', 'Caen.state_0': 'OFF', 'Caen.ovp_0': 500, 'Caen.current_0': 0.0, 'Caen.voltage_0': 0.0, 'Caen.set_voltge_1': 0.0, 'Caen.set_current_1': None, 'Caen.set_state_1': 'OFF', 'Caen.state_1': 'OFF', 'Caen.ovp_1': 500, 'Caen.current_1': 0.0, 'Caen.voltage_1': 0.0, 'Caen.set_voltge_2': 0.0, 'Caen.set_current_2': None, 'Caen.set_state_2': 'OFF', 'Caen.state_2': 'OFF', 'Caen.ovp_2': 500, 'Caen.current_2': 0.0, 'Caen.voltage_2': 0.0, 'Caen.set_voltge_3': 0.0, 'Caen.set_current_3': None, 'Caen.set_state_3': 'OFF', 'Caen.state_3': 'OFF', 'Caen.ovp_3': 500, 'Caen.current_3': 0.0, 'Caen.voltage_3': 0.0, 'LV.set_voltge_1_0': 11.0, 'LV.set_current_1_0': 0.0, 'LV.set_state_1': 'OFF', 'LV.ocp_1_0': 11.5, 'LV.current_1_0': 0.0, 'LV.voltage_1_0': 0.0, 'LV.set_voltge_1_1': 11.0, 'LV.set_current_1_1': 0.0, 'LV.ocp_1_1': 11.5, 'LV.current_1_1': 0.0, 'LV.voltage_1_1': 0.0, 'LV.set_voltge_2_0': 11.0, 'LV.set_current_2_0': 0.0, 'LV.set_state_2': 'OFF', 'LV.ocp_2_0': 11.5, 'LV.current_2_0': 0.0, 'LV.voltage_2_0': 0.0, 'LV.set_voltge_2_1': 11.0, 'LV.set_current_2_1': 0.0, 'LV.ocp_2_1': 11.5, 'LV.current_2_1': 0.0, 'LV.voltage_2_1': 0.0}
# username = 'grafana'
# password = 'bnlphysics'
# database = 'coldjig'
# retention_policy = 'autogen'
# buckett = f'{database}/{retention_policy}'
# #buckett= 'coldbox1'
# #tokenn='5oWocWC7kJB0QVeHZFFDVN3XzurD_ICcIiaUVen95X1GaCbGrNVmyD58BVNY8LHPeppHg7NKjE5sAzxYDLE5bw=='
# ifclient = InfluxDBClient(url='http://169.254.115.145:8086', token=f'{username}:{password}', org='-')
# #ifclient = InfluxDBClient(url='http://localhost:8086', token=tokenn, org='coldboxSW')
# write_ap = ifclient.write_api(write_options=SYNCHRONOUS) 
# setup='BNL'
# sender='ITSDAQ'
# reciever='COLDJIG'
# measurement= 'COMM'
# modules_list=[0,1,2,3]
# json_d={'modules':modules_list}
# modules=json.dumps(json_d)
# #print(modules)
# point = Point("COMM").tag("SETUP",setup).tag("SENDER",sender).tag("RECEIVER",reciever).field("COMMAND","STATUS").field("DATA", modules).field("ERROR", "module 4 is dead")
# print(point.to_line_protocol())
# timestart=t.time_ns()
# #print(timestart)
# write_ap.write(bucket=buckett, record= point)

# t.sleep(5)
# query_api = ifclient.query_api()
# timenow=t.time_ns()
# print(timenow)

# def get_ITSDAQ_status(timestart):
#     query= f'''
#     from(bucket: \"{buckett}\") 
#     |> range(start: time(v: {timestart}), stop: now()) 
#     |> filter(fn: (r) => r["_measurement"] == \"{measurement}\") 
#     |> filter(fn: (r) => r["SENDER"] == \"{sender}\") 
#     |> filter(fn: (r) => r["SETUP"] == \"{setup}\")  
#     |> filter(fn: (r) => r["_field"] == \"COMMAND\" or r["_field"] == \"DATA\" or r["_field"] == \"ERROR\")  
#     |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: \"_value\") 
#     |> yield(name: \"last\")'''
#     try:
#         tables = query_api.query(query)
#         for record in tables[0].records:
#             if record["COMMAND"] == "STATUS":
#                 #print(f'{record["DATA"]}')
#                 #print(f'{record["ERROR"]}')
#                 return {"ERROR": record["ERROR"], "DATA": record["DATA"]}
#             else :
#                 print("Waiting for status from ITSDAQ")
#                 return None
#     except Exception as e:
#         print("Waiting for status from ITSDAQ")
#         return None

# while True:
#     status = get_ITSDAQ_status(timestart)
#     if status == None:
#         t.sleep(5)
#     else:
#         print(f'{status["DATA"]}')
#         print(f'{status["ERROR"]}')
#         break


# You can generate a Token from the "Tokens Tab" in the UI
# token = "jJDEP_5o-UxKPtfEKrWSv_bEYvJw4XBygYJYxrmwtmi02ucVrfAmJa_3UjGjuKKpSOH5nt7B6UdcPrcdYCloDQ=="
# org = "ATLAS"
# bucket = "TEST"

# token = "S6xWfdZz5lWwGaLBdOKjF_-7HEyOUWxpPbaHZugB1xZv3O7htvOD7O1A3eBvs0J6v0r38V879fhZnxtpVxi9tg=="
# org = "ATLAS"
# bucket = "TEST"


if __name__ == "__main__" :
    # influx_direct_write()

    # influx_dict_write()

    influx_config_file = "configs/UK_China_Barrel/warwick_influx.ini"
    itk_influx_write(influx_config_file)

