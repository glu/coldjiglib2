import sys
import os
sys.path.append(os.getcwd())

import time

import logging
import Hardware.Flow.FS2012
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)s:%(message)s')

breakpoint()

n2_flow = Hardware.Flow.FS2012.FS2012('N2')

parameters = { 'BUS':'4'}
n2_flow.initialise(parameters)

data_dict = {}
logging.debug(data_dict)

n2_flow.add_data_dict_keys(data_dict)
logging.debug(data_dict)

n2_flow.Set(data_dict)
logging.debug(data_dict)

n2_flow.Read(data_dict)
logging.debug(data_dict)

for i in range(10) :
    time.sleep(1)
    n2_flow.Read(data_dict)
    logging.debug(data_dict)

print("N2 SHUTDOWN")
n2_flow.Shutdown()


