import sys
import os
sys.path.append(os.getcwd())

import Hardware.Temperature.TC08
import Hardware.Humidity.HYT939
import Hardware.DewPoint.DewPoint

import time

import logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)s:%(message)s')

unit = Hardware.Temperature.TC08.TC08("TC08_2")

unit_params = { 'Serial':'A0043/762',
               'CH1':'K,VC5','CH2':'K,CB5',
               'CH3':'K,COOL_IN','CH4':'K,COOL_OUT',
               'CH5':'K,AIR1','CH6':'K,AIR2',
               'CH7':'K,LAB','CH8':'X,SPARE'
            }

unit.initialise(unit_params)


rh = Hardware.Humidity.HYT939.HYT939('RH1')
rh_params = { 'ID':'1', 'BUS':'3','ADDR':'0x28'}
rh.initialise(rh_params)

#breakpoint()

dp = Hardware.DewPoint.DewPoint.DewPoint("DP1")
dp_params = {'ID':'1','Input_Air_Temp':'thermometer.AIR1','Input_RH':'RH.1'}
dp.initialise(dp_params)

data_dict = {}
print(data_dict)

unit.add_data_dict_keys(data_dict)
logging.info(f'TC08 DATA DICT KEYS: {data_dict}')

rh.add_data_dict_keys(data_dict)
logging.info(f'TC08+RH DATA DICT KEYS: {data_dict}')

dp.add_data_dict_keys(data_dict)
logging.info(f'TC08+RH+DP DATA DICT KEYS: {data_dict}')

unit.Read(data_dict)
logging.info(f'READING TC08: {data_dict}')

rh.Read(data_dict)
logging.info(f'READING TC08+RH {data_dict}')

dp.Read(data_dict)
logging.info(f'READING TC08+RH+DP : {data_dict}')


logging.info("Sleep for 10s")
time.sleep(10)

unit.Read(data_dict)
rh.Read(data_dict)
dp.Read(data_dict)
logging.info(f'READING TC08+RH+DP: {data_dict}')


print("TC08+RH+DP SHUTDOWN")
unit.Shutdown()
rh.Shutdown()
dp.Shutdown()

