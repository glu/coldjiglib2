# Test code of hardware integration

# This is a hack for now to import coldjiglib & Warwick_TC
import time
import sys
import os
sys.path.append(os.getcwd())

from pubsub import pub
# the defined messages topics are:
# - 'warning' : eg: "humidity increasing", "N2 flow low"
# - 'error' : eg: "No response from chiller", "can't connect to INFLUX"
# - 'alert' : "Turn down N2 flow", "Safe to open lid", "module QC completed"
# - 'danger' : "condensation on module", "interlock fired","Peltier overheating"

# Define functions that will run when receiving messages
def pub_warning(message="NOT DEFINED") :
    print(">>> WARNING:",message)

def pub_error(message="NOT DEFINED") :
    print(">>> ERROR",message)

def pub_alert(message="NOT DEFINED") :
    print(">>> ALERT",message)

def pub_danger(message="NOT DEFINED") :
    print(">>> DANGER",message)

def pub_heartbeat(message="NOT DEFINED") :
    print(">>> HEARTBEAT",message)

# Subscribe to messages
pub.subscribe(pub_warning,'warning')
pub.subscribe(pub_error,'error')
pub.subscribe(pub_alert,'alert')
pub.subscribe(pub_danger,'danger')
pub.subscribe(pub_heartbeat,'heartbeat')

import coldjiglib

# Set Hardware INI file
coldjiglib.hardware_ini_file = "configs/UK_China_Barrel/hw.ini"

# Set INFLUX parameters
coldjiglib.influx_ini_file = "configs/UK_China_Barrel/warwick_influx.ini"
# coldjiglib.influx_IP = '137.205.164.168'
# coldjiglib.influx_PORT = 8086
# coldjiglib.influx_DB = 'coldjig'
# coldjiglib.influx_MEAS = 'warwick'

# Set Interlock Action Module
coldjiglib.interlock_action_module = "Warwick_Action" # Name of the Interlock Action module to use 

# Set minimum gas flow 
coldjiglib.gas_flow_min = 0.1

import logging
logging.basicConfig(level=logging.INFO,format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logging.captureWarnings(True)

cj_logger = coldjiglib.logger
cj_logger.setLevel(logging.DEBUG)

# Grab data_dcit -- only do this for testing
data_dict = coldjiglib.data_dict


# Start the core_loop
coldjiglib.start()

# hw_list = coldjiglib.load_hardware("configs/UK_China_Barrel/warwick.ini")
# print(f'hw_list: {hw_list}')

# import threading
# core_loop_event = threading.Event()

# from influxdb import InfluxDBClient
# influx_client = InfluxDBClient(host='137.205.164.168',port=8086)

# breakpoint()
# coldjiglib.core_loop(core_loop_event,hw_list,data_dict,influx_client)


# sleep for 5s and then set chiller to 20.0C
# logging.info("Sleep for 5s and then set chiller to 20.0C and Pump Speed to 5")
# time.sleep(5)

# data_dict['chiller.set_temperature'] = 20.0
# logging.info("Chiller set to 20C")

#data_dict['chiller.set_pump'] = 5
#logging.info("Chiller Pump set to 5")


#logging.info("Now wait 10s to set chiller to 15.0C and pump speed to 1")
#time.sleep(10)

#data_dict['chiller.set_temperature'] = 15.0
#logging.info("Chiller set to 15C")

#data_dict['chiller.set_pump'] = 20
#logging.info("Chiller Pump set to 1")
#time.sleep(10)


# logging.info("Setting Peltiers to HEAT")
# data_dict['peltier.set_mode'] = 'HEAT'
# time.sleep(10)

# logging.info("Setting Peltiers to COOL")
# data_dict['peltier.set_mode'] = 'COOL'
# time.sleep(10)

# # Set Tenma1 to 5V/0.5A - run it in CC mode
# logging.info("Set TENMA1 to 0.5A")
# data_dict['peltier.set_current.1'] = 0.5
# data_dict['peltier.set_volt.1'] = 5.0

# # wait for temperature change
# logging.info("Wait 10s for change in temperatue")
# time.sleep(10)

# # Set Tenma1 back to 0A/0V
# logging.info("Set TENMA1 to 0.0A")
# data_dict['peltier.set_current.1'] = 0.0
# data_dict['peltier.set_volt.1'] = 0.0

# wait for temperature change
# logging.info("Wait 60s for change in temperatue")
# time.sleep(60)

time.sleep(60)
print("SHUTTING DOWN COLDJIG")
coldjiglib.shutdown()
logging.info("Coldjiglib shutdown")

