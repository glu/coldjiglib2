# Test code to check interlock 

import sys
import os
sys.path.append(os.getcwd())

from Interlock.Interlock import Interlock, InterlockError

import logging
logging.basicConfig(level=logging.DEBUG,handlers=[logging.StreamHandler(sys.stdout)])
logging.captureWarnings(True)

# Function to reset data_dict
def reset_data_dict(data_dict) :

    for i in range(1,6) :
        data_dict[f'thermometer.VC{i}'] = 20.0
        data_dict[f'thermometer.CB{i}'] = 20.0
        data_dict[f'peltier.set_volt.{i}'] = 0.0
        data_dict[f'peltier.set_current.{i}'] = 0.0

    data_dict['thermometer.AIR1'] = 20.0
    data_dict['thermometer.AIR2'] = 20.0
    data_dict['peltier.set_mode'] = 'COOL'
    data_dict['chiller.set_temperature'] = 20.0
    data_dict['gas_flow'] = 6.0
    data_dict['DP.1'] = 10.0
    data_dict['DP.2'] = 10.0
    data_dict['RH.1'] = 1.0
    data_dict['RH.2'] = 1.0
    data_dict['Lid.1'] = 'CLOSED'
    data_dict['Lid.2'] = 'CLOSED'


# Setup test data_dict
data_dict = { }
reset_data_dict(data_dict)

interlock = Interlock(data_dict)

print(f'All thermometers: {interlock.all_thermometers}')
print(f'All humidity: {interlock.all_humidity}')
print(f'All DP {interlock.all_DP}')
print(f'All lid switches {interlock.all_lid_switches}')

print(f'RH Interlock {interlock.rh_eval()}')
max_dp = interlock.max_dp()
print(f'Max DP {max_dp}')
print(f'Thermometer Interlock {interlock.temperature_eval(max_dp)}')
print(f'Gas flow interlock {interlock.air_flow_eval()}')
print(f'Lid interlock {interlock.lid_eval()}')


# Under temp each thermometer
print("********* UNDER TEMP CHECK ************")
reset_data_dict(data_dict)
#breakpoint()
for t in interlock.all_thermometers :
    data_dict[t] = -100.0
    try :
        interlock.eval()
    except InterlockError as e :
        print("INTERLOCK FIRED")
        print(interlock.status)
        print(e)
    finally :            
        reset_data_dict(data_dict)

reset_data_dict(data_dict)
for t in interlock.all_thermometers :
    data_dict[t] = -100.0
    try:
        interlock.eval()
    except InterlockError as e :
        print("INTERLOCK FIRED")
        print(interlock.status)
        print(e)

# Over temp each thermometer
print("********* OVER TEMP CHECK ************")
reset_data_dict(data_dict)
for t in interlock.all_thermometers :
    data_dict[t] = 100.0
    try:
        interlock.eval()
    except InterlockError as e :
        print("INTERLOCK ERROR")
        print(interlock.status)
        print(e)
    finally :
        reset_data_dict(data_dict)

reset_data_dict(data_dict)
for t in interlock.all_thermometers :
    data_dict[t] = 100.0
    try :
        interlock.eval()
    except InterlockError as e :
        print(interlock.status)
        print(e)

# DP temp each thermometer
print("********* DP TEMP CHECK ************")
reset_data_dict(data_dict)
for t in interlock.all_thermometers :
    data_dict[t] = 0.0
    try :
        interlock.eval()
    except InterlockError as e:
        print("INTERLOCK FIRED")
        print(interlock.status)
        print(e)
    finally :
        reset_data_dict(data_dict)

reset_data_dict(data_dict)
for t in interlock.all_thermometers :
    data_dict[t] = 0.0
    try :
        interlock.eval()
    except InterlockError as e:
        print("INTERLOCK FIRED")
        print(interlock.status) 
        print(e)

# MAX DP check
data_dict['DP.1'] = 10
data_dict['DP.2'] = 0
print(f'Max DP: {interlock.max_dp()}')

data_dict['DP.1'] = -10
data_dict['DP.2'] = 0
print(f'Max DP: {interlock.max_dp()}')

# Lid Switch Check
reset_data_dict(data_dict)
print(f'Lid Eval: {interlock.lid_eval()}')
print(interlock.status)

# - Open Lid.1
data_dict['Lid.1'] = 'OPEN'
data_dict['Lid.2'] = 'CLOSED'
print(f'Lid Eval: {interlock.lid_eval()}')
print(interlock.status)

# - Open Lid.2
data_dict['Lid.1'] = 'CLOSED'
data_dict['Lid.2'] = 'OPEN'
print(f'Lid Eval: {interlock.lid_eval()}')
print(interlock.status)

# - Open both lid switches
data_dict['Lid.1'] = 'OPEN'
data_dict['Lid.2'] = 'OPEN'
print(f'Lid Eval: {interlock.lid_eval()}')
print(interlock.status)

# Check gas flow
reset_data_dict(data_dict)
print(f'Gas Flow: {data_dict["gas_flow"]}')
print(f'Gas Eval: {interlock.air_flow_eval()}')
print(interlock.status)

# Lower gas flow
data_dict['gas_flow'] = 0.0
print(f'Gas Flow: {data_dict["gas_flow"]}')
print(f'Gas Eval: {interlock.air_flow_eval()}')
print(interlock.status)

# Raise gas flow
interlock.status = "OK - No error"
data_dict['gas_flow'] = 10.0
print(f'Gas Flow: {data_dict["gas_flow"]}')
print(f'Gas Eval: {interlock.air_flow_eval()}')
print(interlock.status)

# Check RH
interlock.status = "OK - No error"
reset_data_dict(data_dict)
print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
print(f'RH Eval: {interlock.rh_eval()}')
print(interlock.status)

data_dict['RH.1'] = -1.0
data_dict['RH.2'] = 1.0
print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
print(f'RH Eval: {interlock.rh_eval()}')
print(interlock.status)

data_dict['RH.1'] = 1.0
data_dict['RH.2'] = -1.0
print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
print(f'RH Eval: {interlock.rh_eval()}')
print(interlock.status)

data_dict['RH.1'] = 90.0
data_dict['RH.2'] = 1.0
print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
print(f'RH Eval: {interlock.rh_eval()}')
print(interlock.status)

data_dict['RH.1'] = 1.0
data_dict['RH.2'] = 90.0
print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
print(f'RH Eval: {interlock.rh_eval()}')
print(interlock.status)

data_dict['RH.1'] = 90.0
data_dict['RH.2'] = 90.0
print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
print(f'RH Eval: {interlock.rh_eval()}')
print(interlock.status)


