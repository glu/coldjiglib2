# Test code to check interlock action
import sys
import os
sys.path.append(os.getcwd())


from pubsub import pub
# the defined messages topics are:
# - 'warning' : eg: "humidity increasing", "N2 flow low"
# - 'error' : eg: "No response from chiller", "can't connect to INFLUX"
# - 'alert' : "Turn down N2 flow", "Safe to open lid", "module QC completed"
# - 'danger' : "condensation on module", "interlock fired","Peltier overheating"

# Define functions that will run when receiving messages
def pub_warning(message="NOT DEFINED") :
    print(">>> WARNING:",message)

def pub_error(message="NOT DEFINED") :
    print(">>> ERROR",message)

def pub_alert(message="NOT DEFINED") :
    print(">>> ALERT",message)

def pub_danger(message="NOT DEFINED") :
    print(">>> DANGER",message)

# Subscribe to messages
pub.subscribe(pub_warning,'warning')
pub.subscribe(pub_error,'error')
pub.subscribe(pub_alert,'alert')
pub.subscribe(pub_danger,'danger')


import coldjiglib

# Set Hardware INI file
coldjiglib.hardware_ini_file = "configs/UK_China_Barrel/single_tc08.ini"

# Set INFLUX parameters
coldjiglib.influx_ini_file = "configs/UK_China_Barrel/warwick_influx.ini"
# coldjiglib.influx_IP = '137.205.164.168'
# coldjiglib.influx_PORT = 8086
# coldjiglib.influx_DB = 'coldjig'
# coldjiglib.influx_MEAS = 'warwick'

# Set interlock action module
coldjiglib.interlock_action_module = "Warwick_Action"

import logging
logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logging.captureWarnings(True)

import time

# Function to reset data_dict
def reset_data_dict(data_dict) :

    for i in range(1,6) :
        data_dict[f'thermometer.VC{i}'] = -35.0
        data_dict[f'thermometer.CB{i}'] = -20.0
        data_dict[f'peltier.set_volt.{i}'] = 10.0
        data_dict[f'peltier.set_current.{i}'] = 1.0

    data_dict['thermometer.AIR1'] = 5.0
    data_dict['thermometer.AIR2'] = 5.0
    data_dict['peltier.set_mode'] = 'COOL'
    data_dict['chiller.set_temperature'] = -20.0
    data_dict['gas_flow'] = 6.0
    data_dict['DP.1'] = -45.0
    data_dict['DP.2'] = -45.0
    data_dict['RH.1'] = 1.0
    data_dict['RH.2'] = 1.0
    data_dict['Lid.1'] = 'CLOSED'
    data_dict['Lid.2'] = 'CLOSED'

data_dict = coldjiglib.data_dict
reset_data_dict(data_dict)

# Start the core_loop
coldjiglib.start()

time.sleep(20)
coldjiglib.status = "COOL_DOWN_TEST"

# -- DP HIGH 
data_dict['DP.1'] = 0.0
time.sleep(20)
reset_data_dict(data_dict)
time.sleep(20)

# -- RH HIGH CHECK ---- 
data_dict['thermometer.VC5'] = -35.0
data_dict['RH.1'] = 80.0
time.sleep(20)
reset_data_dict(data_dict)
time.sleep(20)

# -- OVER TEMP CHECK
data_dict['thermometer.VC5'] = 60.0
time.sleep(20)
reset_data_dict(data_dict)
time.sleep(20)

# -- UNDER TEMP CHECK
data_dict['thermometer.VC5'] = -60.0
time.sleep(20)
reset_data_dict(data_dict)
time.sleep(20)


print("SHUTTING DOWN COLDJIG")
coldjiglib.shutdown()
logging.info("Coldjiglib shutdown")


# data_dict = coldjiglib.data_dict   # Grab reference to data_dict
# reset_data_dict(data_dict)  # reset and add any keys required for testing

# coldjiglib.status = "IDLE"

#breakpoint()

# Under temp each thermometer
#time.sleep(60)
# print("********* UNDER TEMP CHECK ************")
# reset_data_dict(data_dict)
# data_dict['thermometer.VC.5'] = -100.0
# time.sleep(5)
# print(data_dict)



# for t in interlock.all_thermometers :
#     data_dict[t] = -100.0
#     try :
#         interlock.eval()
#     except InterlockError as e :
#         print("INTERLOCK FIRED")
#         print(interlock.status)
#         print(e)
#         wa.action(e.interlock_bit,status,data_dict)
#     finally :            
#         reset_data_dict(data_dict)


# # Over temp each thermometer
# print("********* OVER TEMP CHECK ************")
# reset_data_dict(data_dict)
# for t in interlock.all_thermometers :
#     data_dict[t] = 100.0
#     try:
#         interlock.eval()
#     except InterlockError as e :
#         print("INTERLOCK ERROR")
#         print(interlock.status)
#         print(e)
#         wa.action(e.interlock_bit,status,data_dict)      
#     finally :
#         reset_data_dict(data_dict)


# # DP temp each thermometer
# print("********* DP TEMP CHECK ************")
# reset_data_dict(data_dict)
# for t in interlock.all_thermometers :
#     data_dict[t] = 0.0
#     try :
#         interlock.eval()
#     except InterlockError as e:
#         print("INTERLOCK FIRED")
#         print(interlock.status)
#         print(e)
#         wa.action(e.interlock_bit,status,data_dict)  
#     finally :
#         reset_data_dict(data_dict)


# # MAX DP check
# data_dict['DP.1'] = 10
# data_dict['DP.2'] = 0
# print(f'Max DP: {interlock.max_dp()}')
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 

# data_dict['DP.1'] = -10
# data_dict['DP.2'] = 0
# print(f'Max DP: {interlock.max_dp()}')
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# # Lid Switch Check
# reset_data_dict(data_dict)
# print(f'Lid Eval: {interlock.lid_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# # - Open Lid.1
# data_dict['Lid.1'] = 'OPEN'
# data_dict['Lid.2'] = 'CLOSED'
# print(f'Lid Eval: {interlock.lid_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# # - Open Lid.2
# data_dict['Lid.1'] = 'CLOSED'
# data_dict['Lid.2'] = 'OPEN'
# print(f'Lid Eval: {interlock.lid_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# # - Open both lid switches
# data_dict['Lid.1'] = 'OPEN'
# data_dict['Lid.2'] = 'OPEN'
# print(f'Lid Eval: {interlock.lid_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# # Check gas flow
# reset_data_dict(data_dict)
# print(f'Gas Flow: {data_dict["gas_flow"]}')
# print(f'Gas Eval: {interlock.air_flow_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# # Lower gas flow
# data_dict['gas_flow'] = 0.0
# print(f'Gas Flow: {data_dict["gas_flow"]}')
# print(f'Gas Eval: {interlock.air_flow_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# # Raise gas flow
# interlock.status = "OK - No error"
# data_dict['gas_flow'] = 10.0
# print(f'Gas Flow: {data_dict["gas_flow"]}')
# print(f'Gas Eval: {interlock.air_flow_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# # Check RH
# interlock.status = "OK - No error"
# reset_data_dict(data_dict)
# print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
# print(f'RH Eval: {interlock.rh_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# data_dict['RH.1'] = -1.0
# data_dict['RH.2'] = 1.0
# print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
# print(f'RH Eval: {interlock.rh_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# data_dict['RH.1'] = 1.0
# data_dict['RH.2'] = -1.0
# print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
# print(f'RH Eval: {interlock.rh_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# data_dict['RH.1'] = 90.0
# data_dict['RH.2'] = 1.0
# print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
# print(f'RH Eval: {interlock.rh_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# data_dict['RH.1'] = 1.0
# data_dict['RH.2'] = 90.0
# print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
# print(f'RH Eval: {interlock.rh_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 


# data_dict['RH.1'] = 90.0
# data_dict['RH.2'] = 90.0
# print(f'RH1:{data_dict["RH.1"]} RH2:{data_dict["RH.2"]}')
# print(f'RH Eval: {interlock.rh_eval()}')
# print(interlock.status)
# try :
#     interlock.eval()
# except InterlockError as e:
#     print("INTERLOCK FIRED")
#     print(interlock.status)
#     print(e)
#     wa.action(e.interlock_bit,status,data_dict) 



