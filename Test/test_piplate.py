import sys
import os
sys.path.append(os.getcwd())

import time

import logging
import Hardware.ADC.DAQC2Plate

logging.basicConfig(level=logging.DEBUG)
#breakpoint()
piplate = Hardware.ADC.DAQC2Plate.DAQC2Plate("PiPlate")

parameters = {'Address':'0',
                'DIN':'0,1',
                'DOUT':'0',
                'FREQ':'1'
            }

piplate.initialise(parameters)

data_dict = {}
#print(data_dict)

piplate.add_data_dict_keys(data_dict)
logging.info(f'PiPlate DATA DICT KEYS: {data_dict}')

piplate.Read(data_dict)
logging.info(f'READING Pi PLATE: {data_dict}')

logging.info("Setting DOUT0 to 1")
data_dict['ADC.0.set_DOUT0'] = 1

logging.info("Setting DOUT0 to 1")
piplate.Set(data_dict)
logging.info(f'PiPlate DATA DICT KEYS: {data_dict}')

logging.info("Sleep for 10s")
time.sleep(10)

logging.info("Now set DOUT0 to 0")
data_dict['ADC.0.set_DOUT0'] = 0
piplate.Set(data_dict)
logging.info(f'PiPlate DATA DICT KEYS: {data_dict}')




# for i in range(10) :
#     time.sleep(10)
#     unit.Read(data_dict)
#     print(data_dict)

print("PIPLATE SHUTDOWN")
piplate.Shutdown()


