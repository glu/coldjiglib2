import logging
logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logging.captureWarnings(True)

# This is a hack for now to import coldjiglib & Warwick_TC
import sys
import os
sys.path.append(os.getcwd())


import Hardware.Peltier.PID_HW as PID_HW

pid_params = {  
                'WARM': {'kp':-1.0, 'ki':-0.0}
             }

current_limit = 10.0

pid = PID_HW.PID_Controller(pid_params,current_limit)
pid.mode = 'WARM'

# dirty peltier model - have temperature be proportional to current
peltier  = lambda t,x : t + (x * 0.1)

# run through 100 steps
pid.targetTemperature = 5.0
current_temperature = 1.0

for i in range(100) :
    logging.info(f"Peltier T:{current_temperature}")
    new_current = pid.setCurrent(current_temperature)
    new_temp = peltier(current_temperature,new_current)
    current_temperature = new_temp
    # new_temperature = peltier(current_temperature,new_current)
    # current_temperature = new_temperature

pid.targetTemperature = 10.0
for i in range(100) :
    logging.info(f"Peltier T:{current_temperature}")
    new_current = pid.setCurrent(current_temperature)
    new_temp = peltier(current_temperature,new_current)
    current_temperature = new_temp

pid.mode = 'IDLE'
pid.targetTemperature = 0.0
for i in range(100) :
    logging.info(f"Peltier T:{current_temperature}")
    new_current = pid.setCurrent(current_temperature)
    new_temp = peltier(current_temperature,new_current)
    current_temperature = new_temp

