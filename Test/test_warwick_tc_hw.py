# Test code to test Warwick_TC.py code 
# 
# Warwick_TC is the implementation of the ThermalCyele ABC (interface)

# This is a hack for now to import coldjiglib & Warwick_TC
from ThermalCycle.ThermalCycle import ThermalCycle
import time
import sys
import os
sys.path.append(os.getcwd())

import coldjiglib

# Setup logging
import logging
# Default level is ERROR
timestr = time.strftime("%Y%m%d-%H%M%S")
logFile_name = 'log/'+timestr+'_ColdJigGUI.log'

logging.basicConfig(level=logging.ERROR,
                    format="%(asctime)s:%(levelname)s:%(name)s:%(funcName)s:%(threadName)s::%(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S",
                    filename=logFile_name,
                    filemode='w')
logging.captureWarnings(True)

console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s: %(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger().addHandler(console)

# Set this script's log-level to DEBUG
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# Import Warwick_TC
# from ThermalCycle.Warwick_TC_TEST import Warwick_TC_TEST
from ThermalCycle.Warwick_TC import Warwick_TC

# Set Warwick_TC log-level to DEBUG
import ThermalCycle.Warwick_TC
tc_logger = ThermalCycle.Warwick_TC.logger
tc_logger.setLevel(logging.DEBUG)

# Set ColdJigLib log level to INFO
cj_logger = coldjiglib.logger
cj_logger.setLevel(logging.INFO)

# Set PID log level to DEBUG
import Hardware.Peltier.PID
pid_logger = Hardware.Peltier.PID.logger
pid_logger.setLevel(logging.DEBUG)

# Create a threading event used to stop Warwick_TC thread
import threading
stop_tc = threading.Event()

# Grab data_dcit -- only do this for testing
data_dict = coldjiglib.data_dict

# Add some artificial sensors
#data_dict['gas_flow'] = 0.0
#data_dict['DP.1'] = 0.0
#data_dict['DP.2'] = 0.0

# Set Hardware INI file
coldjiglib.hardware_ini_file = "configs/UK_China_Barrel/warwick.ini"

# Set INFLUX parameters
coldjiglib.influx_ini_file = "configs/UK_China_Barrel/warwick_influx.ini"

# Set Interlock Action Module
coldjiglib.interlock_action_module = "Warwick_Action" # Name of the Interlock Action module to use 

coldjiglib.thermal_cycle_module = "Warwick_TC"

# Set the minimum gas flow for the interlock
coldjiglib.gas_flow_min = 0.1

# Start the core_loop
coldjiglib.start()
time.sleep(10)

# logger.info('Instantiate Warwick TC')
# warwick_tc = Warwick_TC(data_dict,stop_tc)

# logger.debug(warwick_tc.name)
# logger.debug(warwick_tc.stop_tc)
# logger.debug(warwick_tc.data_dict)
# logger.debug(warwick_tc.stage)

# Test Warwick_TC functions
# logger.info('Run SETUP')
# warwick_tc.setup()

# logger.info('Run FINISH')
# warwick_tc.finish()

# #### COOL DOWN ###############
# breakpoint()
# logger.info("Run COOL DOWN")
# warwick_tc.cool_down(modules)

# Run 3 cycles
# for cycle in range(1,4) :

#     logger.debug(f'THERMAL CYCLE {cycle}')
#     ####  TEST FAST HEAT ###############
#     warwick_tc.fast_heat(-15.0,modules)

#     ####  TEST FAST COOL ###############
#     warwick_tc.fast_cool(-25.0,modules)

# warwick_tc.warm_up(-20.0,modules)

# coldjiglib.thermal_cycle(modules,-15.0,-25.0,-20.0)

ncycle = 1
modules = [1,2,3,4,5]
coldjiglib.start_thermal_cycle(modules,40.0,-35.0,20.0,ncycle)
thermal_cycle_future = coldjiglib.thermal_cycle_future

# Wait for thermal cycle to complete
import concurrent.futures
logger.info("Wait for thermal cycling to complete")
concurrent.futures.wait([thermal_cycle_future])
logger.info("thermal cycling completed")



# coldjiglib.start_thermal_cycle([1,2,3,4,5],25.0,15.0,20.0)

# logger.info('Artificially setting N2 flow to max')
# data_dict['gas_flow'] = 10.0

# time.sleep(10)
# data_dict['chiller.set_temperature'] = 20.0

# time.sleep(60)

# logger.info('Artificially setting Dew Point to -50C')
# data_dict['DP.1'] = -50.0
# data_dict['DP.2'] = -50.0

# logger.info('Artificially setting N2 flow to 6')
# time.sleep(10)
# data_dict['gas_flow'] = 6.0
# time.sleep(60)


# Wait 2 mins and then stop
# time.sleep(120)
# coldjiglib.stop_thermal_cycle()





# ####  TEST WARM UP ###############
# warwick_tc.warm_up(20.0,modules)

# warwick_tc.finish()

logger.info("SHUTTING DOWN COLDJIG")
coldjiglib.shutdown()
logging.info("Coldjiglib shutdown")
