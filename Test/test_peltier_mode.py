import sys
import os
sys.path.append(os.getcwd())

import Hardware.Peltier.Relay
import Hardware.ADC.DAQC2Plate

import time

import logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)s:%(message)s')

piplate = Hardware.ADC.DAQC2Plate.DAQC2Plate("PiPlate")

parameters = {'Address':'0',
                'DIN':'0,1',
                'DOUT':'0',
                'FREQ':'1'
            }

piplate.initialise(parameters)


#breakpoint()
relay = Hardware.Peltier.Relay.Relay("PeltierRelay")
relay_params = {'Output':'ADC.0.set_DOUT0'}
relay.initialise(relay_params)

data_dict = {}
print(data_dict)

piplate.add_data_dict_keys(data_dict)
logging.info(f'PIPLATE DATA DICT KEYS: {data_dict}')

relay.add_data_dict_keys(data_dict)
logging.info(f'PIPLATE+RELAY DATA DICT KEYS: {data_dict}')

piplate.Read(data_dict)
logging.info(f'READING PIPLATE: {data_dict}')

relay.Read(data_dict)
logging.info(f'READING PIPLATE+RELAY {data_dict}')

logging.info(f'Setting Relay outputs')
relay.Set(data_dict)
logging.info(f'DATA DICT: {data_dict}')

logging.info(f'Setting PiPlate outputs')
piplate.Set(data_dict)
logging.info(f'DATA DICT: {data_dict}')

logging.info("Sleep for 10s")
time.sleep(10)

#breakpoint()
logging.info("Set Peltier to COOL mode")
data_dict["peltier.set_mode"] = 'COOL'
relay.Set(data_dict)
piplate.Set(data_dict)
logging.info(f'DATA DICT: {data_dict}')

logging.info("Sleep for 10s")
time.sleep(10)

logging.info("Set Peltier to HEAT mode")
data_dict["peltier.set_mode"] = 'HEAT'
relay.Set(data_dict)
piplate.Set(data_dict)
logging.info(f'DATA DICT: {data_dict}')

logging.info("Sleep for 10s")
time.sleep(10)

piplate.Read(data_dict)
relay.Read(data_dict)
logging.info(f'READING PIPLATE+RELAY: {data_dict}')

print("PIPLATE+RELAY SHUTDOWN")
piplate.Shutdown()
relay.Shutdown()

