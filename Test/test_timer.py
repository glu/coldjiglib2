# Test file to test timing decorator

import sys
import os
sys.path.append(os.getcwd())

from Decorator import timer

import time

@timer
def long_task() :
    print("Print countdown every 1s")

    for i in range(10) :
        print(i)
        time.sleep(1)


long_task()
