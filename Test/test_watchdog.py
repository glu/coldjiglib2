import time
import RPi.GPIO as GPIO

# Set up GPIO constants 
EN = 5
WDI = 6
WDO = 7

GPIO.setmode(GPIO.BCM)
GPIO.setup(EN,GPIO.OUT)
GPIO.setup(WDI,GPIO.OUT)
GPIO.setup(WDO,GPIO.IN)

def toggle_up() :
    GPIO.output(WDI,GPIO.LOW)
    time.sleep(1E-3)
    GPIO.output(WDI,GPIO.HIGH)


# Test watchdog
GPIO.output(EN,GPIO.LOW)
print(f'WDO {GPIO.input(WDO)}')



print('Toggling Input')
toggle_up()
toggle_up()
toggle_up()
print(f'WDO {GPIO.input(WDO)}')
time.sleep(1)
print(f'WDO {GPIO.input(WDO)}')
time.sleep(1)
print(f'WDO {GPIO.input(WDO)}')

GPIO.cleanup()


