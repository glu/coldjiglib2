# Test code of BNL Caenintegration
# Call core-loop directly
# Call top Python debugger available to track control flow, line by line
#   - currently commented out

import time
import coldjiglib

import logging
logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logging.captureWarnings(True)

# Grab data_dcit -- only do this for testing
data_dict = coldjiglib.data_dict


hw_list = coldjiglib.load_hardware("configs/US_Barrel/bnl.ini")
print(f'hw_list: {hw_list}')

import threading
core_loop_event = threading.Event()


#breakpoint()    # --- call to Python debugger

# Call core-loop
coldjiglib.core_loop(core_loop_event,hw_list,data_dict)
# NB: After calling core_loop, it will run indefinitely
# unless core_loop_event.set() is called.
#
# If running in debugger, this can called by hand.  If not, then you'll
# have to ctrl-c to stop it

time.sleep(60)
print("SHUTTING DOWN COLDJIG")
coldjiglib.shutdown() 
logging.info("Coldjiglib shutdown")

