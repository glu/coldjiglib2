# Example / Test code for ColdJigLib

import sys 
import time 
import logging
import concurrent
#import Hardware.Test.Test as Test 

# This is a hack for now to import coldjiglib
import os
sys.path.append(os.getcwd())

import coldjiglib

# Grab data_dcit -- only do this for testing
data_dict = coldjiglib.data_dict

# Function to reset data_dict
def reset_data_dict(data_dict) :

    for i in range(1,6) :
        data_dict[f'thermometer.VC{i}'] = 20.0
        data_dict[f'thermometer.CB{i}'] = 20.0
        data_dict[f'peltier.set_volt.{i}'] = 0.0
        data_dict[f'peltier.set_current.{i}'] = 0.0

    data_dict['thermometer.AIR1'] = 20.0
    data_dict['thermometer.AIR2'] = 20.0
    data_dict['peltier.set_mode'] = 'COOL'
    data_dict['chiller.set_temperature'] = 20.0
    data_dict['gas_flow'] = 0.0
    data_dict['DP.1'] = 5.0
    data_dict['DP.2'] = 5.0
    data_dict['Lid.1'] = 'CLOSED'
    data_dict['Lid.2'] = 'CLOSED'


logging.basicConfig(level=logging.DEBUG,handlers=[logging.StreamHandler(sys.stdout)])
logging.captureWarnings(True)

# Star the core_loop
coldjiglib.start()

# Grab reference to Test H/W 
hw = coldjiglib.hw_list[0]

# Sleep for 5s before starting thermal cycling
time.sleep(5) 
coldjiglib.start_thermal_cycle([1,2,3,4,5])

logging.info("Turn up N2 flow...")
hw.N2 = 10.0

time.sleep(2)
logging.info("DP drops 1C per second")
for i in range(5,-47,-1) :
    hw.DP1 = i
    hw.DP2 = i
    time.sleep(1)
time.sleep(2)

logging.info("Turning down N2 flow....")
hw.N2 = 7

logging.info("cooling modules....")

for i in range(20,-36,-1) :
    hw.VC1 = i
    hw.VC2 = i
    hw.VC3 = i
    hw.VC4 = i
    hw.VC5 = i

logging.info("starting thermal cycle")
time.sleep(2)

for cycle in range(5) :
    logging.info(f'Cycle -- {cycle}')
    # HEAT UP
    for t in range(-36,41) :
        hw.VC1 = t
        hw.VC2 = t
        hw.VC3 = t
        hw.VC4 = t
        hw.VC5 = t
        time.sleep(1)
    time.sleep(2)
    # COOL DOWN
    for t in range(41,-36,-1) :
        hw.VC1 = t
        hw.VC2 = t
        hw.VC3 = t
        hw.VC4 = t
        hw.VC5 = t
        time.sleep(1)
    time.sleep(2)

logging.info("Starting warmup")
for t in range(-36,21) :
    hw.VC1 = t
    hw.VC2 = t
    hw.VC3 = t
    hw.VC4 = t
    hw.VC5 = t
    time.sleep(1)

logging.info("Finish warm up")
hw.CB1 = 20
hw.CB2 = 20
hw.CB3 = 20
hw.CB4 = 20
hw.CB5 = 20
hw.AIR1 = 20
hw.AIR2 = 20



time.sleep(5)

coldjiglib.shutdown()
print(data_dict)
print(coldjiglib.hw_list)


# coldjiglib.status()
# time.sleep(3)
# coldjiglib.stop_thermal_cycle()
# coldjiglib.status()
# coldjiglib.shutdown()
# coldjiglib.status()
# print(type(coldjiglib.core_loop_future))
# core_loop_error = coldjiglib.core_loop_future.exception()
# if core_loop_error is not None :
#     logging.info(f'core_loop_error {core_loop_error}')
# logging.info("END OF RUN")
