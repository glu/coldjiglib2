# Test code to test Warwick_TC.py code 
# 
# Warwick_TC is the implementation of the ThermalCyele ABC (interface)

# This is a hack for now to import coldjiglib & Warwick_TC
import time
import sys
import os
sys.path.append(os.getcwd())

import logging
logging.basicConfig(level=logging.DEBUG,handlers=[logging.StreamHandler(sys.stdout)])
logging.captureWarnings(True)


# Import Warwick_TC
from ThermalCycle.Warwick_TC import Warwick_TC

# Function to reset data_dict
def reset_data_dict(data_dict) :

    for i in range(1,6) :
        data_dict[f'thermometer.VC{i}'] = 20.0
        data_dict[f'thermometer.CB{i}'] = 20.0
        data_dict[f'peltier.set_volt.{i}'] = 0.0
        data_dict[f'peltier.set_current.{i}'] = 0.0

    data_dict['thermometer.AIR1'] = 20.0
    data_dict['thermometer.AIR2'] = 20.0
    data_dict['peltier.set_mode'] = 'COOL'
    data_dict['chiller.set_temperature'] = 20.0
    data_dict['gas_flow'] = 0.0
    data_dict['DP.1'] = 10.0
    data_dict['DP.2'] = 10.0

# Create a threading event used to stop Warwick_TC thread
import threading
stop_tc = threading.Event()

# Local data_dict for testing & fill for testing
data_dict = { }
reset_data_dict(data_dict)
modules = [1]

# Open connection to INFLUX
from ITSDAQ_COMM.ITK_INFLUX import ITK_INFLUX
influx_ini_file = "configs/UK_China_Barrel/warwick_influx.ini"
influx = ITK_INFLUX(influx_ini_file)

warwick_tc = Warwick_TC(data_dict,stop_tc,influx)

logging.debug(warwick_tc.name)
logging.debug(warwick_tc.stop_tc)
logging.debug(warwick_tc.data_dict)
logging.debug(warwick_tc.stage)

# Test Warwick_TC functions
warwick_tc.setup(modules)
warwick_tc.finish()

reset_data_dict(data_dict)

####  TEST COOL DOWN ###############

# Launch warwick_tc.cool_down as background thread
import concurrent.futures
task_pool = concurrent.futures.ThreadPoolExecutor()
cool_down_future = task_pool.submit(warwick_tc.cool_down,[1,2,3,4,5])

# sleep 1s, then put N2 flow to max
logging.debug("Wait 1s, then put N2 Flow to max")
time.sleep(1)
data_dict['gas_flow'] = 10.0

# Reduce DP by 5C every second
for dp in range(10,-50,-5) :
    data_dict['DP.1'] = dp
    data_dict['DP.2'] = dp
    time.sleep(1)

# sleep 1s, then turn down N2 flow to 7 LPM
logging.debug("Wait 1s, then reduce N2 Flow to 7 SLPM")
time.sleep(1)
data_dict['gas_flow'] = 7.0

# reduce VC temps by 5C every second
for t in range(20,-40,-5) :
    for m in range(1,6) :
        data_dict[f'thermometer.VC{m}'] = t
    time.sleep(1)

concurrent.futures.wait([cool_down_future],timeout=10)


####  TEST WARM UP ###############
reset_data_dict(data_dict)
#warwick_tc.warm_up(40.0,[1,2,3,4,5])
warm_up_future = task_pool.submit(warwick_tc.warm_up,40.0,[1,2,3,4,5])

# wait 1s, then increment VC from -35 to +40 in 5C increments, every 1s
logging.debug('Waiting 1s before warming up VC*')

for t in range(-35,45,5) :
    for m in range(1,6) :
        data_dict[f'thermometer.VC{m}'] = t
    time.sleep(1)

concurrent.futures.wait([warm_up_future],timeout=10)


####  TEST FAST HEAT ###############
reset_data_dict(data_dict)
#warwick_tc.fast_heat(40.0,[1,2,3,4,5])

fast_cool_future = task_pool.submit(warwick_tc.fast_heat,40.0,[1,2,3,4,5])

# Wait 1s, then increment VC* from -35 to +40 in 5C increments, every 1s
logging.debug('Waiting 1s before warming up VC*')

time.sleep(1)
for t in range(-35,45,5) :
    for m in range(1,6) :
        data_dict[f'thermometer.VC{m}'] = t
    time.sleep(1)

concurrent.futures.wait([fast_cool_future],timeout=10)

####  TEST FAST COOL ###############
reset_data_dict(data_dict)
#warwick_tc.fast_cool(-35.0,[1,2,3,4,5])

fast_cool_future = task_pool.submit(warwick_tc.fast_cool,-35.0,[1,2,3,4,5])

# Wait 1s, then increment VC* from +40 to -35 in 5C increments, every 1s
logging.debug('Waiting 1s before cooling VC*')

time.sleep(1)
for t in range(40,-40,-5) :
    for m in range(1,6) :
        data_dict[f'thermometer.VC{m}'] = t
    time.sleep(1)

concurrent.futures.wait([fast_cool_future],timeout=10)
