import sys
import os
sys.path.append(os.getcwd())

import time

import logging
import Hardware.Temperature.TC08

logging.basicConfig(level=logging.DEBUG)
#breakpoint()
unit = Hardware.Temperature.TC08.TC08("TC08_1")

parameters = { 'Serial':'A0043/757',
               'CH1':'K,VC1','CH2':'K,CB1',
               'CH3':'K,VC2','CH4':'K,CB2',
               'CH5':'K,VC3','CH6':'K,CB3',
               'CH7':'K,VC4','CH8':'K,CB4'
           }

unit.initialise(parameters)

data_dict = {}
#print(data_dict)

unit.add_data_dict_keys(data_dict)
#print(data_dict)

unit.Set(data_dict)
#print(data_dict)

unit.Read(data_dict)
print(data_dict)

# for i in range(10) :
#     time.sleep(10)
#     unit.Read(data_dict)
#     print(data_dict)

print("TC08 SHUTDOWN")
unit.Shutdown()


