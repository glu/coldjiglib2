import sys
import os
sys.path.append(os.getcwd())

import time

import logging
import Hardware.Humidity.HYT939
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)s:%(message)s')

#breakpoint()

rh1 = Hardware.Humidity.HYT939.HYT939('RH1')

parameters = { 'ID':'1', 'BUS':'3','ADDR':'0x28'}
rh1.initialise(parameters)

data_dict = {}
logging.debug(data_dict)

rh1.add_data_dict_keys(data_dict)
logging.debug(data_dict)

rh1.Set(data_dict)
logging.debug(data_dict)

rh1.Read(data_dict)
logging.debug(data_dict)

for i in range(10) :
    time.sleep(1)
    rh1.Read(data_dict)
    logging.debug(data_dict)

print("RH1 SHUTDOWN")
rh1.Shutdown()


