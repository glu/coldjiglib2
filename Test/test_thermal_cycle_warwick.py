# Code to test ColdJigLib's thermal cycle functions for Warwick ColdJig

# This is a hack for now to import coldjiglib & Warwick_TC
import sys
import os
sys.path.append(os.getcwd())

# Import ColdJigLib and ThermalCycle
import coldjiglib
from ThermalCycle.ThermalCycle import ThermalCycle

# Import concurrent and time 
# - concuerrent: wait on ThermalCycle thread to complete
# - get sleep function
import concurrent
import time

# Set up logging
import logging
logging.basicConfig(level=logging.INFO,handlers=[logging.StreamHandler(sys.stdout)],
                    format='%(asctime)s - %(levelname)s - %(name)s - %(funcName)s - %(message)s')
logging.captureWarnings(True)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# Set Hardware INI file
# coldjiglib.hardware_ini_file = "configs/UK_China_Barrel/single_tc08.ini"
coldjiglib.hardware_ini_file = "configs/UK_China_Barrel/minimal.ini"

# Set INFLUX parameters
coldjiglib.influx_ini_file = "configs/UK_China_Barrel/warwick_influx.ini"

# Set Interlock Action Module
coldjiglib.interlock_action_module = "Warwick_Action" # Name of the Interlock Action module to use 

# Set Thermal Cycle Module
coldjiglib.thermal_cycle_module = "Warwick_TC"   # Name of the Thermal Cycle modle to test 

# Function to reset data_dict
def reset_data_dict(data_dict) :

    for i in range(1,6) :
        data_dict[f'thermometer.VC{i}'] = 20.0
        data_dict[f'thermometer.CB{i}'] = 20.0
        data_dict[f'peltier.set_volt.{i}'] = 0.0
        data_dict[f'peltier.set_current.{i}'] = 0.0
        data_dict[f'PID.Chuck{i}.mode'] = "IDLE" 

    data_dict['thermometer.AIR1'] = 20.0
    data_dict['thermometer.AIR2'] = 20.0
    data_dict['peltier.set_mode'] = 'COOL'
    data_dict['chiller.set_temperature'] = 20.0
    data_dict['gas_flow'] = 6.0
    data_dict['DP.1'] = 10.0
    data_dict['DP.2'] = 10.0
    data_dict['RH.1'] = 1.0
    data_dict['RH.2'] = 1.0
    data_dict['Lid.1'] = 'CLOSED'
    data_dict['Lid.2'] = 'CLOSED'

# Function to set all PIDs to IDLE
def set_PID_IDLE(data_dict) :
    for i in range(6) :
        data_dict[f'PID.Chuck{i}.mode'] = "IDLE" 

# Wait for INFLUX to update with command from ColdJig
# influx - pass in coldjig.Influx
def wait_for_influx(influx,start_time) :

    while (True) :
        # Set up DB query
        db_query = f'''
            from(bucket: \"{influx.bucket}\") 
            |> range(start: time(v: {start_time}), stop: now()) 
            |> filter(fn: (r) => r["_measurement"] == \"{influx.comm_measurement}\")   
            |> filter(fn: (r) => r["SENDER"] == \"COLDJIG\") 
            |> filter(fn: (r) => r["RECEIVER"] == \"ITSDAQ\") 
            |> filter(fn: (r) => r["SETUP"] == \"{influx.setup}\")
            |> filter(fn: (r) => r["_field"] == \"COMMAND\" or r["_field"] == \"DATA\" or r["_field"] == \"ERROR\")  
            |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value") 
            |> yield()
        '''
        logger.debug(f'FLUX Query: {db_query}')
        tables = influx.query_api.query(db_query)
        logger.debug(f'tables are {tables}')
        if len(tables) > 0 :
            break
        else :
            logger.info("Still waiting for COLDJIG to send command")
            time.sleep(1)

    logger.info("COLDJIG sent command to ITSDAQ")
    

# Write ITSDAQ status to INFLUX DB
from influxdb_client import Point
def set_ITSDAQ_status(influx) :
    logger.debug("Write ITSDAQ status")
    point = Point(influx.comm_measurement)\
            .tag("SETUP",influx.setup)\
            .tag("SENDER","ITSDAQ")\
            .tag("RECEIVER","COLDJIG")\
            .field("COMMAND","STATUS")\
            .field("DATA", "OK")\
            .field("ERROR", "NONE")
    logger.debug(point)
    influx.write_comm_point(point)





# Reset data_dict to known values
reset_data_dict(coldjiglib.data_dict)

# # Set Thermal Cycle logger 
# logging.getLogger("ThermalCycle.ThermalCycle").setLevel(logging.DEBUG)

# # Set Warwick TC logger
# logging.getLogger("ThermalCycle.Warwick_TC").setLevel(logging.DEBUG)

# # Set ITSDAQ_COMM.Influx_coldjig logger 
# logging.getLogger("ITSDAQ_COMM.Influx_coldjig").setLevel(logging.DEBUG)

logging.getLogger("ITSDAQ_COMM.ITK_INFLUX").setLevel(logging.DEBUG)
logging.getLogger("ITSDAQ_COMM").setLevel(logging.DEBUG)
logging.getLogger("coldjiglib").setLevel(logging.INFO)
logging.getLogger("ITSDAQ_COMM.Influx_coldjig").setLevel(logging.DEBUG)
logging.getLogger("ThermalCycle.ThermalCycle").setLevel(logging.DEBUG)
logging.getLogger("ThermalCycle").setLevel(logging.DEBUG)

# loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]
# print('*' * 10)
# for l in loggers :
#     print(l)
# print('*' * 10)


logger.debug("Starting...")
coldjiglib.start()

logger.debug("wait 5s before starting thermal cycle")
time.sleep(5)

# breakpoint()
ncycle = 5
coldjiglib.start_thermal_cycle([1,2,3,4,5],40.0,-35.0,20.0,ncycle)

logger.debug("wait for COLDJIG to send INIT_MODULES")
wait_for_influx(coldjiglib.Influx,time.time_ns())
set_ITSDAQ_status(coldjiglib.Influx)
logger.debug("Wait 5s for ColdJig to read back ITSDAQ status")
time.sleep(5)

# sleep 1s, then put N2 flow to max
logger.debug("Wait 1s, then put N2 Flow to max")
time.sleep(1)
coldjiglib.data_dict['gas_flow'] = 10.0

# Reduce DP by 5C every second
logger.debug("Drop DP by 5C every second")
for dp in range(10,-50,-5) :
    coldjiglib.data_dict['DP.1'] = float(dp)
    coldjiglib.data_dict['DP.2'] = float(dp)
    time.sleep(1)

# sleep 1s, then turn down N2 flow to 7 LPM
logging.debug("Wait 1s, then reduce N2 Flow to 6 SLPM")
time.sleep(1)
coldjiglib.data_dict['gas_flow'] = 6.0

# Now wait 2m10s for settling time
logging.debug("Waiting 2m10s DP-settling before advancing")
time.sleep(130)

# reduce VC temps by 5C every second
logger.debug("Reduce VC temps by 5C every second")
for t in range(20,-40,-5) :
    for m in range(1,6) :
        coldjiglib.data_dict[f'thermometer.VC{m}'] = float(t)
    time.sleep(1)

# Now cold...run characterisation
logger.debug("Run PRE_TC_CHARACTERISATION")
wait_for_influx(coldjiglib.Influx,time.time_ns())
set_ITSDAQ_status(coldjiglib.Influx)

# Now run ncycle thermal cycles
logger.debug(f"Run {ncycle} thermal cycles")

# Define fast heat and fast cool down functions
def fast_heat(data_dict) :
    time.sleep(1)
    for t in range(-35,45,5) :
        for m in range(1,6) :
            data_dict[f'thermometer.VC{m}'] = float(t)
        time.sleep(1)

def fast_cool(data_dict) :
    time.sleep(1)
    for t in range(40,-40,-5) :
        for m in range(1,6) :
            data_dict[f'thermometer.VC{m}'] = float(t)
        time.sleep(1)

for i in range(1,ncycle+1) :
    logger.debug(f'Starting thermal cycle {i}')

    fast_heat(coldjiglib.data_dict)
    logger.info("ITSDAQ warm test")
    wait_for_influx(coldjiglib.Influx,time.time_ns())
    set_ITSDAQ_status(coldjiglib.Influx)

    fast_cool(coldjiglib.data_dict)
    logger.info("ITSDAQ cold test")
    wait_for_influx(coldjiglib.Influx,time.time_ns())
    set_ITSDAQ_status(coldjiglib.Influx)


logger.debug('Waiting 1s before warming up VC*')
time.sleep(1)
for t in range(-35,25,5) :
    for m in range(1,6) :
        coldjiglib.data_dict[f'thermometer.VC{m}'] = float(t)
    time.sleep(1)

logger.info("HV stabilisation and warm ITSDAQ") 
wait_for_influx(coldjiglib.Influx,time.time_ns())
set_ITSDAQ_status(coldjiglib.Influx)
wait_for_influx(coldjiglib.Influx,time.time_ns())
set_ITSDAQ_status(coldjiglib.Influx)
# logger.debug("Wait another 5s")
# time.sleep(120)

set_PID_IDLE(coldjiglib.data_dict)

concurrent.futures.wait([coldjiglib.thermal_cycle_future])

thermal_cycle_error = coldjiglib.thermal_cycle_future.exception()
if thermal_cycle_error is not None:
    logging.error(f'Thermal Cycle error {thermal_cycle_error}')

coldjiglib.shutdown()
logger.info("END OF RUN")