import time
import sys
import os
sys.path.append(os.getcwd())

import logging
logging.basicConfig(level=logging.DEBUG,handlers=[logging.StreamHandler(sys.stdout)])
logging.captureWarnings(True)



from ThermalCycle.BNL_TC import BNL_TC

# Function to reset data_dict
def reset_data_dict(data_dict) :

    for i in range(0,4) :
        data_dict[f'thermometer.Chuck{i}'] = 20.0
        #data_dict[f'thermometer.CB{i}'] = 20.0
        #data_dict[f'peltier.set_volt.{i}'] = 0.0
        #data_dict[f'peltier.set_current.{i}'] = 0.0

    data_dict['thermometer.AIR_left'] = 20.0
    data_dict['thermometer.AIR_right'] = 20.0
    #data_dict['peltier.set_mode'] = 'COOL'
    data_dict['chiller.set_temperature'] = 20.0
    data_dict['AIR_flow'] = 0.0
    data_dict['DP.1'] = 10.0
    data_dict['DP.2'] = 10.0

# Create a threading event used to stop BNL_TC thread
import threading
stop_tc = threading.Event()

# Local data_dict for testing & fill for testing
data_dict = { }
reset_data_dict(data_dict)

BNL_tc = BNL_TC(data_dict,stop_tc)

logging.debug(BNL_tc.name)
logging.debug(BNL_tc.stop_tc)
logging.debug(BNL_tc.data_dict)
logging.debug(BNL_tc.stage)

# Test BNL_TC functions
BNL_tc.setup()
BNL_tc.finish()

reset_data_dict(data_dict)

####  TEST COOL DOWN ###############

# Launch BNL_tc.cool_down as background thread
import concurrent.futures
task_pool = concurrent.futures.ThreadPoolExecutor()
cool_down_future = task_pool.submit(BNL_tc.cool_down,[0,1,2,3])

# sleep 1s, then put N2 flow to max
#logging.debug("Wait 1s, then put N2 Flow to max")
time.sleep(1)
data_dict['Air_flow'] = 3.0

# Reduce DP by 5C every second
for dp in range(10,-50,-5) :
    data_dict['DP.1'] = dp
    data_dict['DP.2'] = dp
    time.sleep(1)

# sleep 1s, then turn down N2 flow to 7 LPM
#logging.debug("Wait 1s, then reduce N2 Flow to 7 SLPM")
time.sleep(1)
data_dict['Air_flow'] = 3.0

# reduce VC temps by 5C every second
for t in range(20,-40,-5) :
    for m in range(0,4) :
        data_dict[f'thermometer.Chuck{m}'] = t
    time.sleep(1)

concurrent.futures.wait([cool_down_future],timeout=10)


####  TEST WARM UP ###############
reset_data_dict(data_dict)
#BNL_tc.warm_up(40.0,[1,2,3,4,5])
warm_up_future = task_pool.submit(BNL_tc.warm_up,40.0,[0,1,2,3])

# wait 1s, then increment VC from -35 to +40 in 5C increments, every 1s
logging.debug('Waiting 1s before warming up VC*')

for t in range(-35,45,5) :
    for m in range(1,6) :
        data_dict[f'thermometer.Chuck{m}'] = t
    time.sleep(1)

concurrent.futures.wait([warm_up_future],timeout=10)

