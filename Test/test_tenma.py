import sys
import os
sys.path.append(os.getcwd())

import time

import logging
import Hardware.Peltier.Tenma
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)s:%(message)s')

breakpoint()

# TENMA 1
tenma1 = Hardware.Peltier.Tenma.Tenma("TENMA1")
tenma1_params = {'UsbPort':'/dev/ttyACM0','Chuck':'1'}
tenma1.initialise(tenma1_params)

data_dict = {}
logging.debug(data_dict)

tenma1.add_data_dict_keys(data_dict)
logging.debug(data_dict)

tenma1.Set(data_dict)
logging.debug(data_dict)

tenma1.Read(data_dict)
logging.debug(data_dict)

for i in range(10) :
    time.sleep(1)

    if i % 2 :
        volt = 1
        current = 0.1
    else :
        volt = 2
        current = 0.2

    logging.info(f'Setting voltage to {volt}C and current to {current}')
    data_dict['peltier.set_volt.1'] = volt
    data_dict['peltier.set_current.1'] = current

    tenma1.Read(data_dict)
    tenma1.Set(data_dict)
    logging.debug(data_dict)

print("TENMA1 SHUTDOWN")
tenma1.Shutdown()


