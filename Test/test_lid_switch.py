import sys
import os
sys.path.append(os.getcwd())

import time

import logging
import Hardware.ADC.DAQC2Plate
import Hardware.Lid.Lid

logging.basicConfig(level=logging.DEBUG)
piplate = Hardware.ADC.DAQC2Plate.DAQC2Plate("PiPlate")

parameters = {'Address':'0',
                'DIN':'0,1',
                'DOUT':'0',
                'FREQ':'1'
            }

piplate.initialise(parameters)
breakpoint()

lid1 = Hardware.Lid.Lid.Lid("Lid1")
lid1_params = {'ID':'1','Input':'ADC.0.DIN0'}
lid1.initialise(lid1_params)

lid2 = Hardware.Lid.Lid.Lid("Lid2")
lid2_params = {'ID':'2','Input':'ADC.0.DIN1'}
lid2.initialise(lid2_params)


data_dict = {}
print(data_dict)

piplate.add_data_dict_keys(data_dict)
logging.info(f'PiPlate DATA DICT KEYS: {data_dict}')

lid1.add_data_dict_keys(data_dict)
logging.info(f'PiPlate+Lid1 DATA DICT KEYS: {data_dict}')

lid2.add_data_dict_keys(data_dict)
logging.info(f'PiPlate+Lid1+Lid2 DATA DICT KEYS: {data_dict}')

piplate.Read(data_dict)
logging.info(f'READING PiPlate PLATE: {data_dict}')

lid1.Read(data_dict)
logging.info(f'READING PiPlate+Lid1 PLATE: {data_dict}')

lid2.Read(data_dict)
logging.info(f'READING PiPlate+Lid1+Lid2 PLATE: {data_dict}')

logging.info("Sleep for 10s")
time.sleep(10)

piplate.Read(data_dict)
lid1.Read(data_dict)
logging.info(f'READING PiPlate+Lid1+Lid2 PLATE: {data_dict}')


print("PIPLATE+Lid1 SHUTDOWN")
piplate.Shutdown()
lid1.Shutdown()
lid2.Shutdown()
