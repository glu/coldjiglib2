# Utility to group all decorators here
import logging
logger = logging.getLogger(__name__)

# This timing function was taken from Real Python Decorators tutorial
# https://realpython.com/primer-on-python-decorators/#timing-functions
import functools
import time

def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      
        run_time = end_time - start_time
        if run_time >= 1.0 :
            logger.critical(f"SLOW FUNCTION: {func.__name__!r} took {run_time:.4f} secs")
        else :
            logger.debug(f"{func.__name__!r} took {run_time:.4f} secs")
        return value
    return wrapper_timer

