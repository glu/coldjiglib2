class Mediator :
    """
    Mediator object:  Pass events between objects
    Allows updated data to be pushed to other objects that are depenedent 
    on it 
    """

    def __init__(self) :
        self.__event_list = {} # dictionary event list

    def add_subscriber(self,event, observer) :
        try :
            (self.__event_list[event]).append(observer)
        except KeyError :
            (self.__event_list)[event] = [observer]

    def notify(self,event) :
        try : 
            obsever_list = self.__event_list[event]
            for obs in obsever_list :
                obs.update(event) 
        except :
            print(f'No subscribers for event {event}')

