from concurrent.futures import ThreadPoolExecutor
from typing import MutableMapping

# -- NEED TO RATIONALISE THESE INTERLOCK IMPORTS
from Interlock.Interlock import Interlock, InterlockError,Interlock_Bits
from Interlock import create as interlock_action_create
# from Interlock.Interlock import Interlock
# from Interlock.Interlock import InterlockError, Interlock_Bits
# from Interlock.Interlock import Interlock
# import Interlock
#---
import logging
import time
import threading
import configparser
import Hardware
from ITSDAQ_COMM.ITK_INFLUX import ITK_INFLUX
import ThermalCycle
import sys 
from pubsub import pub
#from influxdb_client import InfluxDBClient
#from influxdb_client.client.write_api import SYNCHRONOUS

# the defined messages topics are:
# - 'warning' : eg: "humidity increasing", "N2 flow low"
# - 'error' : eg: "No response from chiller", "can't connect to INFLUX"
# - 'alert' : "Turn down N2 flow", "Safe to open lid", "module QC completed"
# - 'danger' : "condensation on module", "interlock fired","Peltier overheating"
# - 'heartbeat' : Special topic, sent every iteration of core loop. Message is current date/time 
# - 'thermalcycle' : Special topic, signals current thermal cycle: 
#                      - 1-N -> thermal cycle number, END: thermal cycle ended 

# Print out warnings of deprecated functions
import warnings
warnings.simplefilter('default')

#import modules.GUIlogging as GUIlogging
## GLOBAL VALUES
logger = logging.getLogger(__name__)
#logger = GUIlogging.init_logger(__name__)

# -----------------------------------------------------------------------------
# ----------------------------------- SETTINGS --------------------------------
# -----------------------------------------------------------------------------
# ColdJigLib Settings
# - Hardware INI File
hardware_ini_file =  ""     # full path to Hardware INI file
# - INFLUX INI File
influx_ini_file = ""        # full path to INFLUX INI file
# - Thermal Cycling Module
thermal_cycle_module = ""   # Name of the thermal cycle module to use
# - Interlock minimum gas flow
gas_flow_min = 0.0          # Initialise minimum gas flow to 0.0
status = "IDLE"             # String to log what coldjig is doing now

"""
The defined status messages are:
- IDLE : Default state. Doing nothing
- TC_START : Thermal Cycling routine started
- PRE_TC_CHARACTERISATION : Module cold turn-on and first cold testing
- TC_INITAL_COOLDOWN : First step in ColdJig Thermal Cycling routine - cooling ColdJig
- TC_WARMUP_<N> : Fast warm up of thermal cycle <N> (1 < N < 1000)
- TC_COOLDOWN_<N> : Fast cool down of themal cycle <N>  (1 < N < 1000)
- TEST_START : Called at start of ITSDAQ module testing
- TEST_END : Called at end of ITSDAQ module testing
- POST_TC_WARMUP : After thermal cycling complete, warm up for POST_TC and HV stabilisation
- POST_TC_CHARACTERISATION : Final module warm testing
- HV_STABILISATION : Set when running HV stabilisation test
- TC_FINAL_WARMUP : Last step in ColdJig thermal cyling routine. final warm up to room temperature 
- TC_END : Thermal cycling routine has finished
"""

data_dict = {}  # Global data dictionary :
                # Mediates values read/set between hardware and routines

hw_list = []    # Global H/W list
                # Filled by as part of hardware initiialisation

core_task = ThreadPoolExecutor()    # thread pool for core tasks
                                    # these threads are expected to begin at start of
                                    # coldjig and run until ColdJig is shutdown

task_pool = ThreadPoolExecutor()    # thread pool for speicific tasks
                                    # that start and run to completion

task_futures = [] # Global list of futures after submitting to task pool

Influx = None # WILL BE CONNECTION TO INFLUX DB

# - Interlock Action Module (what to do when interlock fires)
interlock_action_module = "" # Name of the Interlock Action module to use 

# keep track of current interlock status
#  - TRUE  :  INTERLOCK OK
#  - FALSE :  INTERLOCK BAD; something bad happend 
interlock_status_bits = Interlock_Bits(rh=True, temperature=True, 
                                        dp=True, air=True, lid=True)
interlock_fired = threading.Event()  # Event set when interlock fires         

# -----------------------------------------------------------------------------
# ---------------------------------- CORE LOOP --------------------------------
# -----------------------------------------------------------------------------
"""
# Core tasks :

As part of core_loop, these functions are executed in this order:
1. read_hw() : read out hardware (eg: read chiller temperature)
2. interlock() : check interlocks using just read-out data
3. PID() : Calculate PID corrections for Peltiers
    - this will be an empty function for BNL ColdJig
4. set_hw(): Set hardware values (eg: set Peltier current)
5. upload_to_db() : Upload data to DB
6. heart_beat() : Send heart beat to acknowledge core_loop is still running
7. Sleep : dynamacially adjusted to ensure each loop iteration meets RATE

core_loop starts when ColdJig starts and keeps running until ColdJig is shutdown
"""
RATE = 1.0  # set how often to run core_loop
            # (eg: every 1s(default), every 10s, etc)


def read_hw(hw_list,data_dict) :
    """
    Read all hardware in hw_list and save to data_dict
    """
    for hw in hw_list:
        logger.debug(f'Reading {hw.name}')
        try :
            hw.Read(data_dict)
        except KeyError as e:
            logger.error(f"Error reading {hw.name}")
            logger.error(f"Dictionary Key Error: {e}")
            pub.sendMessage('error',message=f'Error reading {hw.name}')
            raise


def set_hw(hw_list,data_dict) :
    """
    Set all hardware on hw_list using values in data_dict
    """
    for hw in hw_list :        
        logger.debug(f'Setting {hw.name}')
        try :
            hw.Set(data_dict)
            #print(data_dict)
        except KeyError as e:
            logger.error(f"Error setting {hw.name}")
            logger.error(f"Dictionary Key Error: {e}")
            pub.sendMessage('error',message=f'Error setting {hw.name}')
            raise



# def interlock(data_dict) :
#     """
#     check all parameters are OK
#     NB: This may end up having Interlock ABC to allow
#     customisation of the interlock check and/or action
#     if interlock fails
#     """
#     logger.debug("Checking interlocks")
#     return False 
    
def PID(data_dict) :
    """
    Calcualte Peltier currents
    This may need to PID ABC to allow different algorithms to run.
    As the BNL version will be an empty function as it has no Peltiers
    """
    logger.debug("PID Update")


def upload_to_db(data_dict) :
    """
    Upload data_dict to Influx DB
    Interface to INFLUX to be implemented
    """
    logger.debug("Upload latest data to INFLUX")
    #influx_write_API = influx_client.write_api(write_options=SYNCHRONOUS)
    #global influx_MEAS, influx_bucket, influx_org
    #logger.debug(f'{influx_bucket} is the bucket')
    #logger.debug(f'{influx_MEAS} is the bucket')
    Influx.WriteToDatabase(data_dict,status)
    #influx_write_API.write(influx_bucket, influx_org, {"measurement": influx_MEAS,"fields": data_dict, "time": time.time_ns()})


def heart_beat() :
    """
    Heart beat is to let other parties know core_loop is running
    It will need to toggle the Watchdog, and send heart-beat to GUI
    """
    time_now = time.time()
    pub.sendMessage('heartbeat',message=time_now)
    logger.debug(f'Sent heart-beat: {time_now}')

def core_loop(core_loop_stop,hw_list,data_dict) :
    """
    This is main background task that start with ColdJig.
    After sorting HW lists to remove read/set dependencies,
    it start the while loop that keeps running until the ColdJig
    shutsdown.

    The while loop:
    - read/set all hardware
    - checks interlock
    - updates PID
    - uploads data_dict to DB
    - sends heart beat

    There is a sleep at the end that is adjusted to maintain the
    defined RATE
    """

    global interlock_status_bits

    logger.info("Optimising HW list for Read/Set")

    hw_read_list = hw_list.copy()
    Hardware.resolve_inputs(hw_read_list,data_dict)
    logger.debug(f'HW READ LIST: {[hw.name for hw in hw_read_list]}')

    hw_set_list = hw_list.copy()
    Hardware.resolve_outputs(hw_set_list,data_dict)
    logger.debug(f'HW SET LIST: {[hw.name for hw in hw_set_list]}')

    interlock = Interlock(data_dict,gas_flow_min)

    logger.info("core loop started")
    while(not core_loop_stop.is_set()) :
        t0 = time.monotonic()

        try :
            read_hw(hw_read_list,data_dict)
            PID(data_dict)
            set_hw(hw_set_list,data_dict)
            interlock.eval()
        except InterlockError as e:
            upload_to_db(data_dict)       # Upload data to DB to capture state when interlock fired           
            interlock_status_bits = e.interlock_bit
            interlock_fired.set()
            logger.critical("Interlock Fired !")
            logger.critical(f"Interock Bits: {e.interlock_bit}")
            logger.critical(e.message)
            pub.sendMessage('danger',message=f'Interlock Fired! {e.message}')
        except KeyError as e:
            logger.error(f"Dictonary Key Error: {e}")
            pub.sendMessage('error',message=f'Dictionary Key Error {e}')
        except :
            logger.exception(f"Unknown error: {sys.exc_info()[0]}")
            pub.sendMessage('error',message='Unknown error in core loop - check log files')
            raise
        finally :
            data_dict['status'] = status  # Record current status
            upload_to_db(data_dict)       # Upload data to DB            
            heart_beat()                  # Send heart-beat

            t1 = time.monotonic()
            dt = t1 - t0
            sleep_time = RATE - dt

            if (sleep_time <= 0.0) :
                logger.warning(f"slow down rate - each loop iteration takes {dt} seconds")
            else :
                logger.debug(f"Loop took {dt} seconds")
                logger.debug(f"sleep for {sleep_time} seconds")
                time.sleep(sleep_time)
    
    logger.info("core loop shut down")


# -----------------------------------------------------------------------------
# ---------------------------------- INTERLOCK --------------------------------
# -----------------------------------------------------------------------------
def interlock_loop(ia_action) :
    """
    The interlock loop runs only when the interlock fires.  
    """
    logger.info("Interlock started")
    logger.info(f'Interlock Action: {ia_action.name}')
    while (not core_loop_event.is_set()) :
        interlock_fired.wait()
        logger.debug("Interlock fired.  Taking action")
        ia_action.action(interlock_status_bits,status,data_dict)
        interlock_fired.clear()
        
    logger.info("Interlock Stopped")


def load_hardware(ini_filename) :
    """
    The INI file describes the connected hardware.  Each
    driver is loaded dynamically, much like a plugin, which allows
    new hardware to be easily included/excluded without changing
    the core code.
    """

    parser = configparser.ConfigParser()
    parser.optionxform = str
    parser.read(ini_filename)
    sections=parser.sections()
    hw_list=[]
    for section in sections:
        logger.info(f'Connecting to {section}')
        hardware_class = Hardware.create(parser[section]['Type'])

        keyword_dict = dict(parser.items(section))
        keyword_dict.pop('Type',None)

        hw = hardware_class(section)
        hw.initialise_hw(keyword_dict)
        hw.add_data_dict_keys_hw(data_dict)
        hw_list.append(hw)
    
    return hw_list

## --> Thermal Cycle will probably have to migrate to a ThermalCycle interface
## to accommodate specfics of each ColdJig, if a generic routine cannot be
## written
stop_task = threading.Event()

# hardcoded to Warwick TC script -- will need to be dynamically loaded
#import ThermalCycle.Warwick_TC as Warwick_TC
#import ThermalCycle.Warwick_TC_TEST as Warwick_TC_TEST

def thermal_cycle(modules,
                  tc_warm_temp,tc_cool_temp,tc_warm_up_temp,ncycle) :

    #global thermal_cycle_module
    global status, data_dict, Influx, stop_task

    # Reset stop_task 
    stop_task.clear()
    
    # Set thermal cycle message to 0
    pub.sendMessage('thermalcycle',message='0')

    # Import the thermal cycle module & class
    # NEED TO ADD TRY/EXCEPT CLAUSE HERE IF MODULE LOADING FAILS
    # AND TO GRACEFULLY LEAVE THIS FUNCTIONS AND CLOSE THE THREAD
    logger.debug(f'loading {thermal_cycle_module}')
    tc_class = ThermalCycle.create(thermal_cycle_module)
    # Create an instance of the loaded thermal cycle class
    tc_instance = tc_class(data_dict,stop_task,Influx)
    logger.debug(f'Starting {tc_instance.name}')
    

    # Increment run_number 
    # - saving old value & then increment to variable to avoid any threading issues
    old_run_number = data_dict['RUN_NUMBER']
    logger.debug(f"old run number: {old_run_number}")

    data_dict['RUN_NUMBER'] = old_run_number +  1
    logger.debug(f"data_dict run number: {data_dict['RUN_NUMBER']}")

    logger.info(f'Starting Thermal Cycle: RUN NUMBER:{data_dict["RUN_NUMBER"]}')
    logger.info(f'Starting Thermal Cycle: Modules: {modules}')
    logger.info(f'Starting Thermal Cycle: High-Temp:{tc_warm_temp} Low-Temp:{tc_cool_temp} Warm-Up:{tc_warm_up_temp}')
    logger.info(f'Starting Thermal Cycle: N-Cycles: {ncycle}')

    # Start the thermal cycle setup and cool down
    status = "TC_START"
    
    tc_instance.setup(modules)

    status = "TC_INITIAL_COOLDOWN"
    tc_instance.cool_down(tc_cool_temp,modules)

    # Module cold - run cold characterisation
    status = "PRE_TC_CHARACTERISATION"
    tc_instance.cold_characterisation(modules)

    # Run the cycles
    for i in range(1,ncycle+1) :
        # Skip thermal cycling if stop_tc is set
        if stop_task.is_set() : 
            logger.info("Thermal Cycling Aborted - thermal cycles skipped")
            break 

        # update cycle number
        data_dict['CYCLE'] = i
        logger.info(f"thermal cycle {i}")
        pub.sendMessage('thermalcycle',message=str(i))

        # warm up modules
        status = f'TC_WARMUP_{i}'
        tc_instance.fast_heat(tc_warm_temp,modules)

        # run warm confirmation test
        status = 'TEST_START'
        tc_instance.warm_confirmation(modules)
        status = 'TEST_END'

        # cool down modules
        status = f'TC_COOLDOWN_{i}'
        tc_instance.fast_cool(tc_cool_temp,modules)

        # run cold confirmation test
        status = 'TEST_START'
        tc_instance.cold_confirmation(modules)
        status = 'TEST_END'

    # Warm up modules
    status = "POST_TC_WARMUP"
    tc_instance.warm_up(tc_warm_up_temp,modules)

    # Run warm characterisation and HV stabilisation
    status = 'POST_TC_WARM_CHARACTERISATION'
    tc_instance.warm_characterisation(modules)
    status = 'HV_STABILISATION'
    tc_instance.hv_stabilisation(modules)

    # Module QC completed - finish
    status = "TC_FINAL_WARMUP"
    tc_instance.finish()
    status = "TC_END"

    if stop_task.is_set() :
        logger.info("Thermal cycle aborted")
        tc_instance.stop_thermalcycle_routine(modules)
        pub.sendMessage('warning',message='Thermal cycle aborted')
    else :
        logger.info("Thermal Cycle Complete")
        pub.sendMessage('alert',message="Thermal Cycle completed")

    # Thermal Cycle finised
    data_dict['CYCLE'] = 0   # reset cycle number
    status = "IDLE"          #  return to IDLE state
    pub.sendMessage('thermalcycle',message='END')

    """
    NB: THIS IS TEMPORARY.  TIME BETWEEN TC_END AND IDLE IS LIKELY << 1SEC
    SO UNLIKELY TC_END WILL GET SAVED IN INFLUX
    LONGER TERM IS TO HAVE A BACKGROUND THREAD TO CHECK IF ANY TASKS THREADS ARE
    RUNNING.  IF THERE ARE NO TASK THREADS, MUST BE IN IDLE STATE.
    STARTED WRITING THIS OUT BUT COMMENTED OUT FOR NOW.  WILL COMPLETE AFTER ESSENTIAL 
    WORK DONE
    """
    return 

############### COLDJIG API ###################
## PUBLIC INTERFACE TO THE COLDJIGLIB MODULE ##

# GLOBAL variable to core_loop thread
core_loop_future = None

# GLOBAL variable to stop core_loop
core_loop_event = threading.Event()

def start() :
    """
    - Non-blocking function
    - Connects and initialises hardware
    - Loads Interlock Action : what to do if interlock fires
    - Opens INFLUX DB connection
    - starts core loop
    - return false if there's a problem
    """
    global hardware_ini_file
    global core_loop_future, hw_list
    global interlock_action_module
    global Influx
    global data_dict

    hw_list = load_hardware(hardware_ini_file) 
    Influx = ITK_INFLUX(influx_ini_file) 

    # Find latest run number
    latest_run_number = Influx.get_latest_value("RUN_NUMBER")

    # Add run number to data_dict and set to zero
    data_dict['RUN_NUMBER'] = latest_run_number if latest_run_number is not None else 0
    logger.debug(f'Latest Run Number: {latest_run_number}')
    logger.debug(f"data_dict Run Number: {data_dict['RUN_NUMBER']}")

    # Add CYCLE to data_dict and set to zero
    data_dict['CYCLE'] = 0

    core_loop_future = start_core(core_loop_event,hw_list,data_dict)
    interlock_action = interlock_action_create(interlock_action_module)
    core_task.submit(interlock_loop,interlock_action())

    # Hardcoded to return True....need to trap for errors ?
    return True

def shutdown() :
    """
    - immediately shut down all jobs, threads, tasks
    - stop core loop
    - Gracefully disengage hardware
    - return false if there's a problem
    """
    logger.info("Shutting down all tasks and core_loop")
    end(task_futures)

    # End core_loop
    core_loop_event.set()

    # Set interlock fired to force interlock loop to run 
    interlock_fired.set()

    # Wait for core_loop to stop 
    logger.info('waiting for core tasks to stop')
    core_task.shutdown(wait=True)
    #logger.debug(f'Core Loop running: {core_loop_future.running()}')
    logger.info("All core tasks stopped")

    # Shutdown hardwarwe
    for hw in hw_list :
        hw.Shutdown()
    
    # Close INFLUX DB connection ---> SHOULD ADD EXPLICIT INFLUX CLOSE HERE
    #influx_client.close()

    # Hardcoded to return True....need to trap for errors ?
    return True


thermal_cycle_future = None # GLOBAL : thermal cycle future
def start_thermal_cycle(modules,
                        tc_warm_temp,tc_cool_temp,
                        tc_warm_up_temp,ncycle) :
    """
    - start the module QC thermal cycle process for modules in list
    - launch thread to run thermal cycle sequence
    - return false for any errors
    """

    # ---> DOES NOT USE LIST OF MODULES
    # ---> thermal_cycle WILL PROBABLY EVOLVE TO AN ABC TO ALLOW
    # ---> EACH COLDJIG TO DEFINITE ITS OWN THERMAL-CYCLING
    global thermal_cycle_future
    thermal_cycle_future = task_pool.submit(thermal_cycle,modules,tc_warm_temp,
                                            tc_cool_temp,tc_warm_up_temp,ncycle)  #---> NCYLE SHOULD NOT BE A GLOBAL
    task_futures.append(thermal_cycle_future)

    # Hardcoded to return True....need to trap for errors ?
    return True

def stop_thermal_cycle() :
    """
    - immediately stop thermal cycle
    - turns off Peltiers if running
    - return false for any errors
    """

    #status = thermal_cycle_future.cancel()
    stop_task.set()
    # NEED TO DO SOMETHING WITH STATUS TO DETERMINE IF thermal_cycle STOPS

    # TURN OFF PELTIERS
    # NEED SOMETHING TO LAUNCH A THREAD TO CHECK PELTIERS ARE OFF
    # AND REPORT BACK STATUS IN CASE OF PROBLEMS
    # NUMBER OF PELTIERS NEED TO BE DYNAMIC / SETTING
    # WARWICK 5 CHUCKS / ENDCAP 4 CHUCKS
    #data_dict['peltier.set_current.1'] = 0.0
    #data_dict['peltier.set_current.2'] = 0.0
    #data_dict['peltier.set_current.3'] = 0.0
    #data_dict['peltier.set_current.4'] = 0.0

    #data_dict['peltier.set_voltage.1'] = 0.0
    #data_dict['peltier.set_voltage.2'] = 0.0
    #data_dict['peltier.set_voltage.3'] = 0.0
    #data_dict['peltier.set_voltage.4'] = 0.0

    # check_peltier_off() --> TO BE WRITTEN AND PROBABLY NEEDS TO BE THREADED

    # Hardcoded to return True....need to trap for errors ?
    return True


##### AUXILLARY FUNCTIONS ##########

def start_core(e,hw_dict,data_dict) :
    core_task_future = core_task.submit(core_loop,e,hw_dict,data_dict)
    return core_task_future


def end(futures) :
    for f in futures :
        f_status = f.cancel()
        logger.debug(f"f_status = {f_status}")


# COMMENTED OUT FOR NOW -- WILL GET BACK TO LATER..
# def check_idle() :
#     # Iterate through task futures list 
#     # If none are running, then coldjig is in idele state

#     busy = False
#     for tf in task_futures :
#         if tf.running() :
#             busy = True
#             break
    
#     if not busy :
#         status = "IDLE"

#     # check every second
#     time.sleep(1)
